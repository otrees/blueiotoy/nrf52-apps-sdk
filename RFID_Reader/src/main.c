#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "boards.h"

#include "app_timer.h"

#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"

#include "nrf_uarte.h"
#include "nrfx_uarte.h"

// LOG
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

// BOARD
#include "board_buttons.h"
#include "board_rgbleds.h"
    // PERIPHERALS
#include "peripheral/adc.h"
#include "peripheral/pwm.h"

// APP
#include "app_rfid.h"

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define MAX_DITY_CNT        (0)
APP_TIMER_DEF(m_app_timer_id);

nrfx_uarte_t app_uart_inst = NRFX_UARTE_INSTANCE(1);

uint8_t rx_buffer[32];
uint8_t rx_cnt = 0;

uint8_t dirty_counter;


//TODO: turn off the power management

// SCAN COMMAND
static int16_t max_value;
static int max_ticks; 
static int ticks;


void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name)
{
  NRF_LOG_ERROR("APP ERROR HANDLER: %s:%d err.code =%d\n", p_file_name, line_num, error_code);
  NRF_LOG_FLUSH();
  nrf_delay_ms(3000);
  NVIC_SystemReset();
}

void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    NRF_LOG_ERROR("Received a fault! id: 0x%08x, pc: 0x%08x, info: 0x%08x", id, pc, info);
    assert_info_t *p_info = (assert_info_t *) info;
    NRF_LOG_ERROR("ASSERTION FAILED at %s:%u", p_info->p_file_name, p_info->line_num);
}

void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


void on_rfid_timer()
{
    rfid_check_and_detect();
}


int16_t get_average(uint16_t *adc_values)
{
    int32_t result = 0;
    uint16_t num_samples = get_adc_samples_num(); 
    for (int i = 0; i < num_samples; i++)
    {
        result += (int16_t) adc_values[i];
    }
    result /= num_samples;
    return (int16_t) result;
}


void on_adc_done_output_average()
{
    adc_stop();

    int16_t average = get_average(get_adc_data());
    NRF_LOG_INFO("ADC average: %d", average); 

    // disable sampling event
    adc_sampling_event_disable();
}

void on_adc_done_output_samples()
{
    // skip dirty data
    if (dirty_counter-- != 0) return;

    // disable sampling event
    adc_stop();

    // output data
    uint16_t *adc_values = get_adc_data();
    for (int i = 0; i < ADC_SAMPLES_DETECTION; i++)
    {
        NRF_LOG_RAW_INFO("%d\n\r", (int16_t) adc_values[i]);
    }
}

void on_adc_done_output_average_and_continue()
{
    adc_stop();

    int16_t average = get_average(get_adc_data());
    NRF_LOG_INFO("AVG (%d): %d", ticks, average); 

    if (max_value < average)
    {
        max_value = average;
        max_ticks = ticks;
    } 

    if (ticks++ < 128)
        adc_setup(ADC_MAX_SAMPLES_IN_BUFFER, ticks, on_adc_done_output_average_and_continue);
    else 
        NRF_LOG_INFO("Maximum reached at %d ticks - %d ", max_ticks, max_value);
}

static uint16_t isns_adc_raw[100];
static uint8_t  isns_current_coutner = 0;
void on_adc_done_output_isns()
{
    adc_stop();

    // save average
    int16_t average = get_average(get_adc_data());
    isns_adc_raw[isns_current_coutner++] = average;

    if (isns_current_coutner == 100)
    {
        for (int i = 0; i < 100; i++)
            NRF_LOG_RAW_INFO("%d\n\r", (int16_t) isns_adc_raw[i]);
    }
    else
    {
        // set pwm
        pwm_set_duty_cycle_channel0(isns_current_coutner);

        // setup adc
        adc_setup(4, 128/2, on_adc_done_output_isns);
    }
}



void cmd_scan()
{
    NRF_LOG_DEBUG("CMD SCAN: delays (0,128) ticks");

    // start
    max_ticks = 0;
    max_value = -1000;
    ticks = 1;

    adc_setup(ADC_MAX_SAMPLES_IN_BUFFER, ticks, on_adc_done_output_average_and_continue);
}

void cmd_read(int num)
{

    NRF_LOG_DEBUG("CMD READ: delay %d ticks", num);

    dirty_counter = MAX_DITY_CNT;
    app_timer_stop(m_app_timer_id);

    adc_setup(ADC_SAMPLES_DETECTION, num, on_adc_done_output_samples);
}

void cmd_average(int num)
{
    if (num <= 0 || num >= 128)
    {
        NRF_LOG_WARNING("Wrong input (0,128)");
        return;
    }
    NRF_LOG_DEBUG("CMD AVERAGE: delay %d ticks", num);

    app_timer_stop(m_app_timer_id);

    adc_setup(ADC_MAX_SAMPLES_IN_BUFFER, num, on_adc_done_output_average);
}

void cmd_listen(int num)
{
    NRF_LOG_DEBUG("CMD LISTEN: delay %d ticks", num);

    app_timer_stop(m_app_timer_id);
    rfid_detect();
}

void cmd_listen_on_timer(int num)
{
    NRF_LOG_DEBUG("CMD LISTEN on timer: delay %d ticks", num);

    // start timer to presample and detect
    app_timer_start(m_app_timer_id, APP_TIMER_TICKS(200), NULL);
}


void cmd_measure_current()
{
    NRF_LOG_DEBUG("CMD measure current");

    // start timer to presample and detect
    app_timer_stop(m_app_timer_id);

    // adc deinit , adc init
    // adc_channel_deinit();
    // adc_init_isns_channel();

    pwm_set_duty_cycle_channel0(0);

    adc_setup(4, 128/2, on_adc_done_output_isns);
}



static bool is_digit(uint8_t ch)
{
    return (int) '0' <= ch && ch <= (int) '9';
}


static int get_ticks_from_argument()
{
    int start_idx = 0;
    int end_idx = 0;
    for (int i = 1; i < rx_cnt; i++)
    {
        // not digit
        if (!is_digit(rx_buffer[i])) continue;

        // is digit
        if (!start_idx) start_idx = i;
        end_idx = i;
    }
    rx_buffer[end_idx+1] = 0;
    int ticks = atoi((const char*) &(rx_buffer[start_idx]));

    if (ticks <= 0 || ticks >= 128)
    {
        NRF_LOG_WARNING("Wrong format or input range (0,128)");
        return - 1;
    }
    return ticks;
}

static void process_rx_cmd()
{
    uint8_t cmd = rx_buffer[0];
    switch (cmd)
    {
        // case 13: cmd_listen(ADC_SAMPLING_OPTIMAL_TICKS); break;
        case 13: cmd_listen_on_timer(ADC_SAMPLING_OPTIMAL_TICKS); break;

        case 's':
            cmd_scan();
            break;

        case 'a':
        {
            int num = get_ticks_from_argument();
            if (num == -1)  break;
            cmd_average(num);

        } break;

        case 'r':
        {
            int num = get_ticks_from_argument();
            if (num == -1)  break;
            cmd_read(num);

        } break;

        case 'l':
        {
            int num = get_ticks_from_argument();
            if (num == -1)  break;
            cmd_listen(num);

        } break;

        case 'L':
        {
            int num = get_ticks_from_argument();
            if (num == -1)  break;
            cmd_listen_on_timer(num);

        } break;

        case 'i':
            cmd_measure_current();
            break;


        default:
            NRF_LOG_WARNING("Command %c not recognized.", cmd);
            break;
    }
}


static void uart_event_handler(nrfx_uarte_event_t *p_event, void* p_context)
{
    if (p_event->type == NRFX_UARTE_EVT_RX_DONE)
    {

        if (rx_buffer[rx_cnt] == '\r')
        {
            NRF_LOG_RAW_INFO("\r\n");
            process_rx_cmd();
            rx_cnt = 0;
        }
        else
        {
            NRF_LOG_RAW_INFO("%c", rx_buffer[rx_cnt]);
            rx_cnt++;
        }

        (void) nrfx_uarte_rx(&app_uart_inst, &(rx_buffer[rx_cnt]), 1);
    }
    else if (p_event->type == NRFX_UARTE_EVT_ERROR)
    {
        // app_uart_evt_t app_uart_event;
        // app_uart_event.evt_type = APP_UART_COMMUNICATION_ERROR;
        // app_uart_event.data.error_communication = p_event->data.error.error_mask;
        // m_event_handler(&app_uart_event);

        //TODO: process error
        rx_cnt = 0;
        (void)nrfx_uarte_rx(&app_uart_inst, rx_buffer, 1);
    }
}


void uart_init(void)
{
    ret_code_t err_code;
    nrfx_uarte_config_t config = NRFX_UARTE_DEFAULT_CONFIG;
    config.baudrate = NRF_UARTE_BAUDRATE_115200;
    config.hwfc = NRF_UARTE_HWFC_DISABLED;
    config.interrupt_priority = APP_IRQ_PRIORITY_MID;
    config.parity = NRF_UARTE_PARITY_EXCLUDED;
    config.pselcts = NRF_UARTE_PSEL_DISCONNECTED;
    config.pselrts = NRF_UARTE_PSEL_DISCONNECTED;  
    config.pselrxd = RX_PIN_NUMBER;
    config.pseltxd = NRF_UARTE_PSEL_DISCONNECTED;

    err_code = nrfx_uarte_init(&app_uart_inst, &config, (nrfx_uarte_event_handler_t) uart_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_uarte_rx(&app_uart_inst, rx_buffer,1);    
    APP_ERROR_CHECK(err_code);

    NRF_LOG_INFO("UART init: %d", err_code);
}


static void idle_state_handle()
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static void log_init()
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INTERNAL_FINAL_FLUSH(); //NOTE: this needs to be here because of bootloader
}

static void board_init() 
{
    // init buttons and rgb leds
    buttons_init();
    rgb_leds_init();
    rgb_leds_write(LED_BLUE);

    adc_init();
    // adc_init_isns_channel();

}


static void power_management_init()
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

void timers_init()
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
    app_timer_create(&m_app_timer_id, APP_TIMER_MODE_REPEATED, on_rfid_timer);
}

int main(void)
{
    log_init();
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_INFO("===============================");
    NRF_LOG_INFO("SunFibre RFID Reader starting!!!");

    board_init();
    power_management_init();

    timers_init();
    uart_init();
    rfid_init();
    pwm_init_channel0();

    NRF_LOG_INFO("===============================");

    for (;;)
    { 
        idle_state_handle();
    }
}

