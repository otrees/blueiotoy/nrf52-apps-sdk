
#ifndef APP_RFID_H__
#define APP_RFID_H__ 

#define CONVERSION_TRIALS       (1)

#define K   (256)
#define L   (16)
#define EM4100_LEN            (64)
#define EM4100_HEADER_LEN     (9)
#define EM4100_DATA_COLS      (5)
#define EM4100_DATA_ROWS      (10)
#define EM4100_ID_LEN          EM4100_DATA_ROWS
#define EM4100_BODY_LEN       (EM4100_DATA_COLS * (EM4100_DATA_ROWS+1)) //55


// #define ADC_SAMPLING_OPTIMAL_TICKS_3US  (14) 
#define ADC_SAMPLING_OPTIMAL_TICKS_5US  (112) 
#define ADC_SAMPLING_OPTIMAL_TICKS      ADC_SAMPLING_OPTIMAL_TICKS_5US 

#define ADC_TRIM_BITS            (4)        // 4 bits 
#define ADC_WINDOW_BITS          (K/32)     // 8bits
#define ADC_CHECK_RANGE          (10)       // 15 LSB
// #define ADC_SAMPLES_DETECTION    (8192) 
#define ADC_SAMPLES_DETECTION    (32 * (2*(EM4100_BODY_LEN + 2*EM4100_HEADER_LEN) + ADC_TRIM_BITS + ADC_WINDOW_BITS))  // 32 * 158 = 5096

#define ADC_SAMPLES_CHECK        (32 * 10)  


typedef enum
{
    EVT_RFID_DETECTED,
    NUM_EVTS_RFID,
} app_rfid_evt_t;


typedef struct 
{
    char rfid_tag[EM4100_ID_LEN + 1];
} app_rfid_evt_context_t;

typedef void (*app_rfid_evt_handler_t)(app_rfid_evt_t app_event, app_rfid_evt_context_t *context);


void rfid_detect();
void rfid_check_and_detect();


void rfid_set_app_evt_handler(app_rfid_evt_handler_t app_rfid_evt_handler);

void rfid_init(void);


#endif /* APP_RFID_H__ */
