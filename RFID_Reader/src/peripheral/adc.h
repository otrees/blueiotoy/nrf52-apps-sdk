#ifndef PERIPHERAL_ADC_H__
#define PERIPHERAL_ADC_H__

#include <stdint.h>
#include "nrfx_saadc.h"


#define RFID_PIN                        (8)                     //P.08 
#define ADC_RFID_CHANNEL                NRF_SAADC_INPUT_AIN2    //P.04
#define ADC_RFID_CHANNEL_IDX			4
// #define ADC_RFID_CHANNEL_GAIN           ? 
// #define ADC_RFID_CHANNEL_SAMTIME        ? 

#define GPIOTE_PIN                         RFID_PIN  
// #define GPIOTE_PIN                      LED_3_RGB_BLUE
// #define GPIOTE_PIN                      MIC_PWR_PIN_NUMBER 
// #define GPIOTE_PIN                      DIM_LED_PWM  

// ADC
#define ADC_NUM_CHANNELS		       1	                //VDD internal, battery, VLED, ISNS

    //sampling
#define ADC_MAX_SAMPLES_IN_BUFFER       (8192)      //Number of SAADC samples in RAM before returning a SAADC event. For low power SAADC set this constant to 1. Otherwise the EasyDMA will be enabled for an extended time which consumes high current.


// sampling frequency
#define ADC_CALIBRATION_INTERVAL        1000                              //Determines how often the SAADC should be calibrated relative to NRF_DRV_SAADC_EVT_DONE event. E.g. value 5 will make the SAADC calibrate every fifth time the NRF_DRV_SAADC_EVT_DONE is received.

#define ADC_CFG_RESOLUTION									NRF_SAADC_RESOLUTION_10BIT
#define ADC_CFG_OVERSAMPLE                  NRF_SAADC_OVERSAMPLE_DISABLED   //Oversampling setting for the SAADC. Setting oversample to 4x This will make the SAADC output a single averaged value when the SAMPLE task is triggered 4 times. Enable BURST mode to make the SAADC sample 4 times when triggering SAMPLE task once.
#define ADC_CFG_BURST_MODE                  0                               //Set to 1 to enable BURST mode, otherwise set to 0.
#define ADC_CFG_IRQ_PRIORITY								APP_IRQ_PRIORITY_LOW_MID


// millivolts recalculation
#define ADC_RESOLUTION                  1024 
#define ADC_REF_VOLTAGE_MILLIVOLTS      600  // reference voltage (in milli volts) used by ADC while doing conversion.
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS  270 

//NOTE: (6 - ADC PRESCALER) matches the real gain multiplier
#define ADC_RESULT_MILLIVOLTS(ADC_VALUE, ADC_PRE_SCALING_COMPENSATION) \
    ((((ADC_VALUE) * ADC_REF_VOLTAGE_MILLIVOLTS * (6 - (ADC_PRE_SCALING_COMPENSATION))) / ADC_RESOLUTION) )


typedef void (*app_adc_evt_handler_t)();
void adc_set_app_evt_handler(app_adc_evt_handler_t app_adc_evt_handler);


void adc_sampling_event_enable();
void adc_gpiote_event_enable();
void adc_gpiote_event_disable();
void adc_sampling_event_disable();

void adc_timer_set_cc_channel2(uint8_t ticks);
void adc_prepare_buffers(uint16_t num_samples);

void adc_calibrate();
void adc_stop();
void adc_start();
void adc_setup(uint16_t num_samples, uint8_t ticks, app_adc_evt_handler_t app_adc_evt_handler);


void adc_init_isns_channel();
void adc_channel_deinit();

void adc_init();


uint16_t *get_adc_data();
uint16_t get_adc_samples_num();




#endif  /* PERIPHERAL_ADC_H__ */