#include "adc.h"

#include <stdint.h>

#include "nrfx_timer.h"
#include "nrfx_ppi.h"
#include "nrfx_saadc.h"
#include "nrfx_gpiote.h"

#include "nrf_log.h"

#include "boards.h"


#define ADC_RFID_TIMER_125KHz_TICKS  (16000000/125000) 
#define ADC_RFID_TIMER_DUTY_TICKS    (ADC_RFID_TIMER_125KHz_TICKS/2)
#define ADC_RFID_TIMER_OPTIMAL_TICKS     112 

static bool                 adc_calibration = false;  
static nrf_saadc_value_t*   adc_buffer_to_read = NULL;
static uint16_t             adc_num_samples = ADC_MAX_SAMPLES_IN_BUFFER;                        //default
static nrf_saadc_value_t    adc_buffers[ADC_MAX_SAMPLES_IN_BUFFER];

// pointer to application adc event handler
static app_adc_evt_handler_t app_evt_handler;


static nrfx_timer_t         m_timer2 = NRFX_TIMER_INSTANCE(2);

static nrf_ppi_channel_t    m_ppi_channel0;
static nrf_ppi_channel_t    m_ppi_channel1;
static nrf_ppi_channel_t    m_ppi_channel2;

void adc_calibrate()
{
    nrfx_saadc_abort();                        // abort all ongoing conversions. Calibration cannot be run if SAADC is busy

    NRF_LOG_DEBUG("ADC calibration starting...");   

    while(nrfx_saadc_calibrate_offset() != NRF_SUCCESS); 
    adc_calibration = false;
}


uint16_t *get_adc_data()
{
    return (uint16_t*) adc_buffer_to_read;
}

uint16_t get_adc_samples_num()
{
    return adc_num_samples;
}


static void on_adc_evt(nrfx_saadc_evt_t const *p_event)
{
    ret_code_t err_code;

    if (p_event->type == NRFX_SAADC_EVT_DONE)
    {
        adc_stop();
        // mark buffer as to read
        adc_buffer_to_read = p_event->data.done.p_buffer;

        NRF_LOG_WARNING("SAADC event - %d (prio. %d)", p_event->data.done.size,  current_int_priority_get());
        for (int i = 0; i < 6; i++) NRF_LOG_RAW_INFO("%d ", p_event->data.done.p_buffer[i]);
        NRF_LOG_RAW_INFO("\r\n");

        // // set buffer so the SAADC can write to it again
        // err_code = nrfx_saadc_buffer_convert(p_event->data.done.p_buffer, adc_num_samples);             
        // APP_ERROR_CHECK(err_code);

        if (app_evt_handler) app_evt_handler();
    }

    else if (p_event->type == NRFX_SAADC_EVT_CALIBRATEDONE)
    {
        // need to setup both buffers, as they were both removed with the call to nrfx_saadc_abort before calibration.
        adc_prepare_buffers(adc_num_samples);

        NRF_LOG_DEBUG("ADC calibration completed!");
    }
}



void adc_gpiote_event_enable()
{
    ret_code_t err_code;
    err_code = nrfx_ppi_channel_enable(m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_enable(m_ppi_channel1);
    APP_ERROR_CHECK(err_code);
}

void adc_gpiote_event_disable()
{
    ret_code_t err_code;
    err_code = nrfx_ppi_channel_disable(m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_disable(m_ppi_channel1);
    APP_ERROR_CHECK(err_code);
}


void adc_sampling_event_enable()
{
    ret_code_t err_code;

    nrfx_timer_enable(&m_timer2);
    err_code = nrfx_ppi_channel_enable(m_ppi_channel2);
    APP_ERROR_CHECK(err_code);
}

void adc_sampling_event_disable()
{
    ret_code_t err_code;

    nrfx_timer_disable(&m_timer2);
    err_code = nrfx_ppi_channel_disable(m_ppi_channel2);
    APP_ERROR_CHECK(err_code);
}


void adc_prepare_buffers(uint16_t num_samples)
{
    ret_code_t err_code;
    // nrfx_saadc_abort();    
    
    err_code = nrfx_saadc_buffer_convert(adc_buffers, num_samples);    
    APP_ERROR_CHECK(err_code);

    adc_buffer_to_read = NULL;
    adc_num_samples = num_samples;
}

void adc_timer_set_cc_channel2(uint8_t ticks)
{
    ASSERT(ticks >= 0 && ticks <= 128);
    // set CC event on channel2
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, ticks, false);
}

void adc_set_app_evt_handler(app_adc_evt_handler_t app_adc_evt_handler)
{
    app_evt_handler = app_adc_evt_handler;
}

void adc_stop()
{
    adc_sampling_event_disable();
    adc_gpiote_event_disable();
}

void adc_start()
{
    adc_sampling_event_enable();
    adc_gpiote_event_enable();
}

void adc_setup(uint16_t num_samples, uint8_t ticks, app_adc_evt_handler_t app_adc_evt_handler)
{
    // set ticks
    adc_timer_set_cc_channel2(ticks);

    // prepare buffers
    adc_prepare_buffers(num_samples);

    adc_set_app_evt_handler(app_adc_evt_handler);

    // enable ppi events
    adc_start();
}

static void adc_rfid_init_timer_events()
{
    // gpiote event - compare events for toggling the pin
    nrfx_timer_extended_compare(&m_timer2, NRF_TIMER_CC_CHANNEL0, ADC_RFID_TIMER_125KHz_TICKS, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL1, ADC_RFID_TIMER_DUTY_TICKS, false);

    // sampling event - set CC event on channel2
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, ADC_RFID_TIMER_OPTIMAL_TICKS, false);
}

static void on_timer_evt(){}

static void timer_init()
{
    ret_code_t err_code;

    nrfx_timer_config_t timer_cfg;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;           //user defined
    timer_cfg.frequency = NRF_TIMER_FREQ_16MHz;           //user defined
    timer_cfg.interrupt_priority = APP_IRQ_PRIORITY_LOWEST;    //user defined
    timer_cfg.mode = NRF_TIMER_MODE_TIMER;

    err_code = nrfx_timer_init(&m_timer2, &timer_cfg, (nrfx_timer_event_handler_t) on_timer_evt);
    APP_ERROR_CHECK(err_code);    
}

static void gpiote_init()
{
    ret_code_t err_code;

    if (!nrfx_gpiote_is_init())
    {
        err_code = nrfx_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }

    nrf_gpio_pin_write(RFID_PWM_PIN, 1);
    nrfx_gpiote_out_config_t config = NRFX_GPIOTE_CONFIG_OUT_TASK_TOGGLE(1);
    err_code = nrfx_gpiote_out_init(RFID_PWM_PIN, &config);
    APP_ERROR_CHECK(err_code);

    nrfx_gpiote_out_task_enable(RFID_PWM_PIN);
}



static void adc_sampling_event_init()
{
    ret_code_t err_code;

    // TIMER INIT
    timer_init();

    // TIMER CC_CH2 event --> SAADC sample task
    uint32_t timer_compare_event_ch2_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL2);
    uint32_t adc_sample_task_addr = nrfx_saadc_sample_task_get();

    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel2);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_assign(m_ppi_channel2, timer_compare_event_ch2_addr, adc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
}

void adc_rfid_gpiote_event_init()
{
    ret_code_t err_code;

    // GPIOTE INIT
    gpiote_init();

    // INIT PPI
    uint32_t timer_compare_event_ch0_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL0);
    uint32_t timer_compare_event_ch1_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL1);

    uint32_t gpiote_set_task_addr = nrfx_gpiote_set_task_addr_get(RFID_PWM_PIN);
    uint32_t gpiote_clear_task_addr = nrfx_gpiote_clr_task_addr_get(RFID_PWM_PIN);

    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel1);
    APP_ERROR_CHECK(err_code);

    // toggle task
    err_code = nrfx_ppi_channel_assign(m_ppi_channel0, timer_compare_event_ch0_addr, gpiote_set_task_addr);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_assign(m_ppi_channel1, timer_compare_event_ch1_addr, gpiote_clear_task_addr);
    APP_ERROR_CHECK(err_code);
}


static void adc_rfid_init_channels()
{
    ret_code_t err_code;

    nrf_saadc_channel_config_t channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_RFID_CHANNEL);
    channel_config.acq_time = ADC_RFID_CHANNEL_ACQ;
    channel_config.gain = ADC_RFID_CHANNEL_GAIN;

    err_code = nrfx_saadc_channel_init(ADC_RFID_CHANNEL_IDX, &channel_config);                            
    APP_ERROR_CHECK(err_code);
}



// static void adc_isns_sampling_event_init()
// {
//     ret_code_t err_code;
//     // TIMER INIT
//     timer_isns_init();
//     // INIT PPI
//     uint32_t timer_compare_event_ch0_addr = nrfx_timer_compare_event_address_get(&m_timer, NRF_TIMER_CC_CHANNEL0);
//     uint32_t adc_sample_task_addr = nrfx_saadc_sample_task_get();

//     err_code = nrfx_ppi_channel_alloc(&m_ppi_channel0);
//     APP_ERROR_CHECK(err_code);
//     err_code = nrfx_ppi_channel_assign(m_ppi_channel0, timer_compare_event_ch0_addr, adc_sample_task_addr);
//     APP_ERROR_CHECK(err_code);
// }

void adc_init_isns_channel()
{
    ret_code_t err_code;
    // configure SAADC channel
    nrf_saadc_channel_config_t channel_config_mic = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_ISNS_CHANNEL);
    channel_config_mic.acq_time = NRF_SAADC_ACQTIME_5US;
    channel_config_mic.gain = NRF_SAADC_GAIN1;    //max range 0.6 V

    // initialize SAADC channel with the channel configuration
    err_code = nrfx_saadc_channel_init(0, &channel_config_mic);                            
    APP_ERROR_CHECK(err_code);
}


void adc_channel_deinit()
{
    adc_stop();
    nrfx_saadc_abort();                        // abort all ongoing conversions. Calibration cannot be run if SAADC is busy
    nrfx_saadc_channel_uninit(0);
}

static void adc_instance_init()
{
    ret_code_t err_code;
    nrfx_saadc_config_t saadc_config = {
        .low_power_mode = false,
        .resolution = ADC_CFG_RESOLUTION,
        .oversample = ADC_CFG_OVERSAMPLE,
        .interrupt_priority = ADC_CFG_IRQ_PRIORITY 
    };

	// initialize SAADC and SAADC channel 
    err_code = nrfx_saadc_init(&saadc_config, on_adc_evt);
    APP_ERROR_CHECK(err_code);
}


void adc_init()
{
    adc_instance_init();
    adc_rfid_init_channels();
    adc_rfid_init_timer_events();

    adc_sampling_event_init();
    adc_rfid_gpiote_event_init();
    
    // calibrate ADC
    // adc_calibrate();
}

