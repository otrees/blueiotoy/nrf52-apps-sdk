#ifndef APP_CONFIG_H__
#define APP_CONFIG_H__

#include <stdint.h>
#include "boards.h"

#include "nrf_drv_pwm.h"


// BUTTON CONFIG
#define USER_BUTTON_PIN            	            BUTTON_0                                /**< Button that will trigger the notification event with the LED Button Service */
#define BUTTON_DETECTION_DELAY_MS               45                     /**< Delay from a until a button is reported as pushed */
#define BUTTON_SHORT_PRESS_MS          	        1000                   /**< Interval  50-1000 ms when button is reported as SHORT PRESS */
#define BUTTON_LONG_PRESS_MS          	        3500                   /**< Interval  1000-3500 ms when button is reported as LONG PRESS */
#define BUTTON_VERY_LONG_PRESS_MS               8000                  /**< Interval   3500-8000 ms when button is reported as VERY_LONG_PRESS . */
#define BUTTON_VERY_VERY_LONG_PRESS_MS          20000                  /**< Interval  8000-15000 ms when button is reported as VERY_LONG_PRESS . */


// PWM CONFIG
// #define PWM_CH0_OUTPUT_PIN0                  (LED_2_RGB_RED | NRF_DRV_PWM_PIN_INVERTED)
#define PWM_CH0_OUTPUT_PIN0                     DIM_LED_PWM
#define PWM_CH0_OUTPUT_PIN1                     NRF_DRV_PWM_PIN_NOT_USED
#define PWM_CH0_OUTPUT_PIN2                     NRF_DRV_PWM_PIN_NOT_USED
#define PWM_CH0_OUTPUT_PIN3                     NRF_DRV_PWM_PIN_NOT_USED


#endif  /* APP_CONFIG_H__ */