#ifndef APP_RFID_H__
#define APP_RFID_H__ 

#include <stdint.h>
#include <stdbool.h>

#define RFID_TAG_LEN     10


typedef enum
{
    EVT_RFID_DETECTED,
    NUM_EVTS_RFID,
} app_rfid_evt_t;



typedef struct 
{
    char rfid_tag_array[RFID_TAG_LEN + 1];
    uint32_t rfid_tag;
    bool paired;
} app_rfid_evt_context_t;

typedef void (*app_rfid_evt_handler_t)(app_rfid_evt_t app_event, app_rfid_evt_context_t *context);

typedef struct 
{
    app_rfid_evt_handler_t   app_evt_handler;
} app_rfid_init_t;


void app_rfid_disable();
void app_rfid_enable();

void app_rfid_set_paired_tag(uint32_t tag_id);
void app_rfid_set_detected_tag(uint32_t tag_id, uint8_t *tag_id_array, bool update_characteristics);
void app_rfid_set_app_evt_handler(app_rfid_evt_handler_t app_rfid_evt_handler);

void app_rfid_init(app_rfid_init_t *init);

#endif /* APP_RFID_H__ */
