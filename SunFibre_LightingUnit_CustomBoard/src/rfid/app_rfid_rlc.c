#include "app_rfid.h"
#include "app_rfid_rlc.h"
#include "boards.h"

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RLC == 1

#include "nrf_log.h"
#include "app_timer.h"


#include "app_config.h"
#include "utils.h"

#include "peripheral/adc.h"

#define APP_TIMER_TICKS_TO_MS(ticks) (ticks * 1000000 / 16384)

// TIMER 
APP_TIMER_DEF(m_rfid_timer_id); 
APP_TIMER_DEF(m_rfid_detectiondelay_timer_id); 

static bool rfid_can_read;
static bool rfid_processing;
static bool enabled = false; 

static uint8_t rfid_tag_id_array[EM4100_ID_LEN];

static void rfid_detect();
static void rfid_check_and_detect();
static void on_adc_detection_sampling_done();
static void on_adc_check_sampling_done();

static adc_cfg_t adc_cfg_check = 
{
    .mode = ADC_MEASURE_MODE_RFID,
    .adc_evt_handler = on_adc_check_sampling_done,
    .num_samples = ADC_SAMPLES_CHECK,
    .start_immediately = true
};

static adc_cfg_t adc_cfg_detection = 
{
    .mode = ADC_MEASURE_MODE_RFID,
    .adc_evt_handler = on_adc_detection_sampling_done,
    .num_samples = ADC_SAMPLES_DETECTION,
    .start_immediately = true
};


static int demodulate(uint16_t *adc_samples, uint16_t adc_values_num, uint8_t *encoded_bits)
{
    uint32_t movavg_K_cur = 0;
    uint32_t movavg_L_cur = 0;

    uint8_t bitval_prev;
    uint16_t bitidx_prev = K/2;

    uint16_t encoded_bits_cnt = 0;

    // sum first K elements of long windows 
    for (int i = 0; i < K; i++)
        movavg_K_cur += adc_samples[i];

    // sum L elements of small window
    for (int i = K/2 - L/2; i < K/2 + L/2; i++)
        movavg_L_cur += adc_samples[i];

    // calculate first
    bitval_prev = movavg_K_cur * L < movavg_L_cur * K;

    // demodulate 
    for (int i = K/2; i < adc_values_num-K/2; i++)
    {
        // threshold
        uint8_t bitval_cur = movavg_K_cur * L < movavg_L_cur * K;

        // adjust moving average
        movavg_K_cur -= adc_samples[i-K/2];
        movavg_K_cur += adc_samples[i+K/2];
        movavg_L_cur -= adc_samples[i-L/2];
        movavg_L_cur += adc_samples[i+L/2];

        // check changes
        if (bitval_prev != bitval_cur)
        {
            uint16_t diff = i - bitidx_prev;
            if (80 > diff && diff >= 48)
            {
                encoded_bits[encoded_bits_cnt++] = bitval_cur;
                encoded_bits[encoded_bits_cnt++] = bitval_cur;
            }
            else if ( 48 > diff && diff >= 16) 
            {
                encoded_bits[encoded_bits_cnt++] = bitval_cur;
            }
            // else if (encoded_bits_cnt != 0)
            // {
            //     // return -1;
            // }
            bitidx_prev = i;
        }
        bitval_prev = bitval_cur;
    }
    return encoded_bits_cnt;
}

static int _decode(uint8_t *encoded_bits, uint16_t encoded_bits_num, uint8_t *decoded_bits, uint16_t start)
{
    uint16_t decoded_bits_cnt = 0;
    for (int i=start+1; i < encoded_bits_num; i+=2)
    {
        if (encoded_bits[i-1] && !encoded_bits[i]) decoded_bits[decoded_bits_cnt++] = 0;
        else if (!encoded_bits[i-1] && encoded_bits[i]) decoded_bits[decoded_bits_cnt++] = 1;
        else return -1;
    }
    return decoded_bits_cnt;
}

static int decode(uint8_t *encoded_bits, uint16_t encoded_bits_num, uint8_t *decoded_bits)
{
    int decoded_bits_cnt = _decode(encoded_bits, encoded_bits_num, decoded_bits, 0);
    if (decoded_bits_cnt != -1) return decoded_bits_cnt;

    return _decode(encoded_bits, encoded_bits_num, decoded_bits, 1);
}

static int read_tag_id(uint8_t *decoded_bits, uint16_t decoded_bits_num, uint8_t *tag_id)
{
    // find header
    int start_idx = -1;
    int header_cnt = 0;

    for (int i = 0; i < decoded_bits_num; i++)
    {
        if (!decoded_bits[i])
        {
            header_cnt = 0; continue;
        }

        if (++header_cnt == EM4100_HEADER_LEN)
        {
            start_idx = i+1; break;
        }
    }
    if (start_idx == -1) return -1;

    // rearange if needed
    if (start_idx + EM4100_BODY_LEN > decoded_bits_num)
    {

        uint8_t first_part_len = decoded_bits_num - start_idx;
        uint8_t second_part_len = EM4100_BODY_LEN - first_part_len ;
        uint8_t second_part_idx = decoded_bits_num - EM4100_LEN;

        uint8_t tmp[second_part_len];
        // copy second part to temporary register  ... [len(data) - 64 : start_idx - 9]
        memcpy(tmp, &decoded_bits[second_part_idx], second_part_len);
        // copy first part to begginning  ... [start_idx: ]
        memcpy(decoded_bits, &decoded_bits[start_idx], first_part_len);
        memcpy(&decoded_bits[first_part_len], tmp, second_part_len);

        // for (int i = start_idx; i < decoded_bits_num; i++) NRF_LOG_RAW_INFO("%d ", decoded_bits[i]);
        // NRF_LOG_RAW_INFO("\n");

        // for (int i = decoded_bits_num - 64; i < start_idx - 9; i++) NRF_LOG_RAW_INFO("%d ", decoded_bits[i]);
        // NRF_LOG_RAW_INFO("\n");

        // NRF_LOG_INFO("idxs: %d-%d/%d %d-%d/%d", 
        //     start_idx, decoded_bits_num, decoded_bits_num-start_idx,
        //     decoded_bits_num-EM4100_LEN, start_idx-9, EM4100_BODY_LEN - (decoded_bits_num - start_idx)
        //     );

        // for (int i = 0; i < 55; i++) NRF_LOG_RAW_INFO("%d ", decoded_bits[i]);
        // NRF_LOG_RAW_INFO("\n");
        // for (int i = decoded_bits_num-start_idx; i < 55; i++) NRF_LOG_RAW_INFO("%d ", decoded_bits[i]);
        // NRF_LOG_RAW_INFO("\n");

        start_idx = 0;

    }

    // //TODO: DEBUG
    // NRF_LOG_INFO("START IDX: %d", start_idx);

    // //TODO: DEBUG
    // for (int i = start_idx; i < start_idx + 55; i+=5){
    //     for (int j = 0; j < 5; j++){
    //         NRF_LOG_RAW_INFO("%d", decoded_bits[i+j]);
    //     }
    //     NRF_LOG_RAW_INFO("\n");
    // }

    // process data
    uint8_t col_parity[EM4100_DATA_COLS-1];
    memset(&col_parity, 0, sizeof(col_parity));

    int idx = start_idx;
    for (int r = 0; r < EM4100_DATA_ROWS; r++)
    {
        uint8_t symbol = 0;
        uint8_t row_parity = 0;

        for (int c = 0; c < EM4100_DATA_COLS-1; c++, idx++)
        {
            symbol = (symbol << 1) | decoded_bits[idx];
            row_parity += decoded_bits[idx];
            col_parity[c] += decoded_bits[idx];
            // if (c ==0 && decoded_bits[idx]) {NRF_LOG_INFO("ADDED %d", col_parity[c]);}
        }
        // check row parity
        // NRF_LOG_INFO("RP: %d / %d (%d)", row_parity, decoded_bits[idx], idx);
        if ((row_parity & 0x1) != decoded_bits[idx]) return -2;
        idx++;

        // assign symbol
        tag_id[r] = symbol;
        // NRF_LOG_INFO("APPENDED: %d", tag_id[r]);
    }

    // check column parity
    for (int c = 0; c < EM4100_DATA_COLS-1; c++, idx++)
        if ((col_parity[c] & 0x1) != decoded_bits[idx]) return -3;

    // check stopbit
    if (decoded_bits[idx]) return -4;

    return EM4100_ID_LEN;
}

static int check(uint16_t *adc_samples, uint16_t adc_values_num)
{
    // TEST01: range > ADC_CHECK_RANGE
    int min = UINT16_MAX;
    int max = 0;
    for (int i = 0; i < adc_values_num; i++)
    {
        if (adc_samples[i] < min) min = adc_samples[i];
        if (adc_samples[i] > max) max = adc_samples[i];
        if (max-min >= ADC_CHECK_RANGE) return true;
    }
    // NRF_LOG_INFO("Check failed, max/min: %d/%d", max, min);
    return false;

} 


static int rfid_process_detection_samples(bool trim)
{
    int begin, end1, end2, end3;
    uint16_t *adc_samples = get_adc_data();
    uint16_t adc_samples_num = get_adc_samples_num();
    uint8_t encoded_bits[256]; 
    uint8_t decoded_bits[128];

    // remove first samples when rfid detection turned on --> when circuit gets stable
    if (trim)
    {
        adc_samples = &adc_samples[ADC_TRIM_BITS * 32];
        adc_samples_num -= ADC_TRIM_BITS * 32;
    }

    // for (int i = 3000; i < 3000 + 20; i++) NRF_LOG_RAW_INFO("%d ", adc_samples[i]);
    // NRF_LOG_RAW_INFO("\n");

    begin = app_timer_cnt_get();

    // DEMODULATION
    int encoded_bits_cnt = demodulate(adc_samples, adc_samples_num, encoded_bits);
    end1 = app_timer_cnt_get();
    if (encoded_bits_cnt < 128) return -1;

    // DECODING
    int decoded_bits_cnt = decode(encoded_bits, encoded_bits_cnt, decoded_bits);
    end2 = app_timer_cnt_get();
    if (decoded_bits_cnt == -1) {NRF_LOG_ERROR("(RFID): Decoding failed"); return -1;}

    // READING
    int tag_id_ret_code = read_tag_id(decoded_bits, decoded_bits_cnt, rfid_tag_id_array);
    end3 = app_timer_cnt_get();

    NRF_LOG_INFO("result time in ticks = %d %d, %d", 
        APP_TIMER_TICKS_TO_MS(app_timer_cnt_diff_compute(end1,begin)),
        APP_TIMER_TICKS_TO_MS(app_timer_cnt_diff_compute(end2,begin)),
        APP_TIMER_TICKS_TO_MS(app_timer_cnt_diff_compute(end3,begin))
    );


    if (tag_id_ret_code == -1) {NRF_LOG_ERROR("(RFID): Reading HEAD failed"); return -1;}
    if (tag_id_ret_code == -2) {NRF_LOG_ERROR("(RFID): Reading ROW PARITY failed"); return -1;}
    if (tag_id_ret_code == -3) {NRF_LOG_ERROR("(RFID): Reading COL PARITY failed"); return -1;}
    if (tag_id_ret_code == -4) {NRF_LOG_ERROR("(RFID): Reading STOPBIT failed"); return -1;}

    return hex2int(rfid_tag_id_array, EM4100_ID_LEN);
}

static int rfid_process_check_samples(bool trim)
{
    uint16_t *adc_samples = get_adc_data();
    if (adc_samples == NULL) 
    {
        NRF_LOG_WARNING("RCH: Non Valid!");
        return 0;
    }
    uint16_t adc_samples_num = get_adc_samples_num();

    // remove first samples when rfid detection turned on --> when circuit gets stable
    if (trim)
    {
        adc_samples = &adc_samples[ADC_TRIM_BITS * 32];
        adc_samples_num -= ADC_TRIM_BITS * 32;
    }
    return check(adc_samples, adc_samples_num);
}


static void on_adc_detection_sampling_done()
{
    // process data
    uint32_t tag_id = rfid_process_detection_samples(true);
    rfid_processing = false;

    if (tag_id == -1) return;

    // do not read anything 1000 ms to prevent glitches
    rfid_can_read = false;

    app_timer_stop(m_rfid_timer_id);
    app_timer_start(m_rfid_detectiondelay_timer_id, RFID_AFTER_DETECTION_DELAY, NULL);

    app_rfid_set_detected_tag(tag_id, rfid_tag_id_array, true);
}

static void on_adc_check_sampling_done()
{
    bool rfid_present = rfid_process_check_samples(true);
    rfid_processing = false;

    // NRF_LOG_INFO("CB Check %d", rfid_present);
    // no detectable signal
    if (!rfid_present) return;

    // detectable signal
    rfid_detect();
}


static void rfid_detect()
{
    // ret_code_t err_code;
    // NRF_LOG_INFO("RD");
    // skip if processsing is ongoing
    // if (rfid_processing) {NRF_LOG_WARNING("SKIP"); return;}
    adc_set_measure_req(&adc_cfg_detection);
    // rfid_processing = true;
}


static void rfid_check_and_detect()
{
    // ret_code_t err_code;
    // NRF_LOG_INFO("RCH");

    // skip if processsing is ongoing
    // if (rfid_processing) {NRF_LOG_WARNING("(RFID): BUSY"); return;}

    adc_set_measure_req(&adc_cfg_check);
}

static void on_rfid_timer()
{
    rfid_check_and_detect();
}

static void on_rfid_detectiondelay_timer()
{
    rfid_can_read = true;
    if (enabled) app_timer_start(m_rfid_timer_id, RFID_DETECTION_FREQUENCY, NULL);
}


void app_rfid_disable()
{
    NRF_LOG_INFO("RFID disable...");
    if (enabled)
        app_timer_stop(m_rfid_timer_id);
    enabled = false;
}

void app_rfid_enable()
{
    NRF_LOG_INFO("RFID enable...");
    if (!enabled)
        app_timer_start(m_rfid_timer_id, RFID_DETECTION_FREQUENCY, NULL);
    enabled = true;
}

static void timers_create()
{
    app_timer_create(&m_rfid_timer_id, APP_TIMER_MODE_REPEATED, on_rfid_timer);
    app_timer_create(&m_rfid_detectiondelay_timer_id, APP_TIMER_MODE_SINGLE_SHOT, on_rfid_detectiondelay_timer); 
}

void app_rfid_init(app_rfid_init_t *init)
{
    timers_create();

    // rfid_processing = false;
    rfid_can_read = true;

    app_rfid_set_app_evt_handler(init->app_evt_handler);
}

#endif