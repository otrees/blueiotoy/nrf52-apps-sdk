#include "app_rfid.h"

#include <stdlib.h>

#include "app_ble_services.h"
#include "utils.h"

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
	#if defined(RFID_AS_RDM6300) && RFID_AS_RDM6300 == 1
		#include "app_rfid_rdm6300.h"
	#elif defined(RFID_AS_RLC) && RFID_AS_RLC == 1
		#include "app_rfid_rlc.h"
	#endif
#endif

// pointer to application rfid event handler
static app_rfid_evt_handler_t app_evt_handler;

// tag id that is desired to detect
static uint32_t paired_rfid_tag_id;

void app_rfid_set_detected_tag(uint32_t tag_id, uint8_t *tag_id_array, bool update_ble_characteristics)
{
    // output EVT_RFID_DETECTED 
    app_rfid_evt_context_t *context = (app_rfid_evt_context_t *) malloc(sizeof(app_rfid_evt_context_t));

    hex_array_to_str(tag_id_array, (uint8_t*) context->rfid_tag_array, RFID_TAG_LEN);
    context->rfid_tag = tag_id;
    context->paired = tag_id == paired_rfid_tag_id;

    app_evt_handler(EVT_RFID_DETECTED, context);
    free(context);

    // set BLE characteristics
    if (update_ble_characteristics)
        app_ble_services_set_detected_tag(tag_id, true);
}


void app_rfid_set_paired_tag(uint32_t tag_id)
{
    paired_rfid_tag_id = tag_id;
}

void app_rfid_set_app_evt_handler(app_rfid_evt_handler_t app_rfid_evt_handler)
{
    app_evt_handler = app_rfid_evt_handler;
}