#include "app_rfid.h"
#include "app_rfid_rdm6300.h"

#include "boards.h"

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RDM6300 == 1

#include "nrf_log.h"
#include "nrf_uarte.h"
#include "nrfx_uarte.h"
#include "app_timer.h"


#include "app_config.h"
#include "utils.h"


static bool rfid_can_read;
static bool enabled;

static int rfid_word_index;
static uint8_t rfid_word[RFID_BUFF_SIZE];

static uint8_t rx_buffer[1];
static nrfx_uarte_t app_uart_inst = NRFX_UARTE_INSTANCE(1);


APP_TIMER_DEF(m_rfid_detectiondelay_timer_id); 


void print_rfid_word()
{
    NRF_LOG_INFO("RFID TAG: %d", hex2int(&rfid_word[RFID_DATA_POS], RFID_DATA_LEN));
}

static bool check_checksum()
{
    int checksum = 0;
    for (int i = 0; i < RFID_DATA_LEN ; i+=2) {
      checksum ^= hex2int(&rfid_word[RFID_DATA_POS + i], 2);
    }
    NRF_LOG_DEBUG("Checksum calculated: %d \r\n", checksum);

    int checksum2 = hex2int(&rfid_word[RFID_CHECKSUM_POS], 2);
    NRF_LOG_DEBUG("Checksum calculated: %d \r\n", checksum2);
    return checksum == checksum2;
}

static void rfid_read_tagid()
{
    if (check_checksum()){

        // do not read anything 1000 ms to prevent glitches
        rfid_can_read = false;
        app_timer_start(m_rfid_detectiondelay_timer_id, RFID_AFTER_DETECTION_DELAY, NULL);

        // set detected tag 
        uint32_t tag_id = hex2int(&(rfid_word[RFID_DATA_POS]), RFID_TAG_LEN);
        app_rfid_set_detected_tag(tag_id, &(rfid_word[RFID_DATA_POS]), true);
    }
    else {
        //TODO: EVT_RFID_ERROR_CHECKSUM
        NRF_LOG_ERROR("(RFID): Checksum error!\r\n");
    }
}

static void rfid_process_rx_byte()
{
    if (!rfid_can_read) return;

    uint8_t byte = rx_buffer[0];

    // HEAD received ... start reading
    if (byte == HEAD) rfid_word_index = 0;

    // read next one
    if (0 <= rfid_word_index && rfid_word_index < RFID_BUFF_SIZE) rfid_word[rfid_word_index++] = byte;

    // check error conditions
    if (rfid_word_index == -1 || rfid_word_index > RFID_BUFF_SIZE)
    {
        NRF_LOG_ERROR("(RFID): Error while processing");
        rfid_word_index = -1;
    }

    // TAIL received ... quit reading 
    if (byte == TAIL && rfid_word_index == RFID_BUFF_SIZE) {
        rfid_word_index = -1;
        rfid_read_tagid();
    }
}

static void on_rfid_detectiondelay_timer()
{
    rfid_can_read = true;
}

static void on_uart_evt(nrfx_uarte_event_t *p_event, void* p_context)
{

    if (p_event->type == NRFX_UARTE_EVT_RX_DONE)
    {
        rfid_process_rx_byte();
        (void)nrfx_uarte_rx(&app_uart_inst, rx_buffer, 1);
    }
    else if (p_event->type == NRFX_UARTE_EVT_ERROR)
    {
        // app_uart_evt_t app_uart_event;
        // app_uart_event.evt_type = APP_UART_COMMUNICATION_ERROR;
        // app_uart_event.data.error_communication = p_event->data.error.error_mask;
        // m_event_handler(&app_uart_event);

        //TODO: process error
        (void)nrfx_uarte_rx(&app_uart_inst, rx_buffer, 1);
    }
}


void app_rfid_enable()
{
    if (!enabled)
        nrfx_uarte_rx(&app_uart_inst, rx_buffer,1);
    enabled = true;
}

void app_rfid_disable()
{
    if (enabled)
        nrfx_uarte_rx_abort(&app_uart_inst);
    enabled = false;
}


static void timers_create()
{
    app_timer_create(&m_rfid_detectiondelay_timer_id, APP_TIMER_MODE_SINGLE_SHOT, on_rfid_detectiondelay_timer); 
}

static ret_code_t uart_init(void)
{
    ret_code_t err_code;
    nrfx_uarte_config_t config = NRFX_UARTE_DEFAULT_CONFIG;
    config.baudrate = NRF_UARTE_BAUDRATE_9600;
    config.hwfc = NRF_UARTE_HWFC_DISABLED;
    config.interrupt_priority = APP_IRQ_PRIORITY_LOWEST;
    config.parity = NRF_UARTE_PARITY_EXCLUDED;
    config.pselcts = NRF_UARTE_PSEL_DISCONNECTED;
    config.pselrts = NRF_UARTE_PSEL_DISCONNECTED;  
    config.pselrxd = RX_PIN_NUMBER;
    config.pseltxd = NRF_UARTE_PSEL_DISCONNECTED;

    err_code = nrfx_uarte_init(&app_uart_inst, &config, (nrfx_uarte_event_handler_t) on_uart_evt);
    NRF_LOG_INFO("(RFID): UART init: %d", err_code);

    APP_ERROR_CHECK(err_code);

    return nrfx_uarte_rx(&app_uart_inst, rx_buffer,1);    
}


void app_rfid_init(app_rfid_init_t *init)
{
    enabled = true;
    rfid_can_read = true;
    uart_init();
    timers_create();

    rfid_word_index = -1;

    app_rfid_set_app_evt_handler(init->app_evt_handler);
}

#endif