#ifndef APP_RFID_RDM6300_H__
#define APP_RFID_RDM6300_H__ 

#include "app_rfid.h"

#define HEAD 			            0x2
#define TAIL 			            0x3

#define RFID_BUFF_SIZE 		        14

#define RFID_DATA_POS 		        1
#define RFID_DATA_LEN 		        10
#define RFID_CHECKSUM_POS 	        11
#define RFID_CHECKSUM_LEN 	        2


#define RFID_AFTER_DETECTION_DELAY							APP_TIMER_TICKS(1000)

#endif /* APP_RFID_RDM6300_H__ */
