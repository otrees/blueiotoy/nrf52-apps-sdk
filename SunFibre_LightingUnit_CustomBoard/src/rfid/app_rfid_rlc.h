
#ifndef APP_RFID_RLC_H__
#define APP_RFID_RLC_H__ 

#include "app_rfid.h"


#define CONVERSION_TRIALS       (1)

#define K                       (256)
#define L                       (16)
#define EM4100_LEN              (64)
#define EM4100_HEADER_LEN       (9)
#define EM4100_DATA_COLS        (5)
#define EM4100_DATA_ROWS        (10)
#define EM4100_ID_LEN           EM4100_DATA_ROWS
#define EM4100_BODY_LEN         (EM4100_DATA_COLS * (EM4100_DATA_ROWS+1)) //55


#define ADC_TRIM_BITS                           (4)        // 4 bits 
#define ADC_WINDOW_BITS                         (K/32)     // 8bits
#define ADC_CHECK_RANGE                         (10)       // 15 LSB
// #define ADC_SAMPLES_DETECTION                (8192) 
#define ADC_SAMPLES_DETECTION                   (32 * (2*(EM4100_BODY_LEN + 2*EM4100_HEADER_LEN) + ADC_TRIM_BITS + ADC_WINDOW_BITS))  // 32 * 158 = 5096

#define ADC_SAMPLES_CHECK                       (32 * 10)  



// #define RFID_DETECTION_FREQUENCY								APP_TIMER_TICKS(5000)
#define RFID_DETECTION_FREQUENCY								APP_TIMER_TICKS(150)
#define RFID_AFTER_DETECTION_DELAY							APP_TIMER_TICKS(1000)

// void rfid_detect();
// void rfid_check_and_detect();

#endif /* APP_RFID_RLC_H__ */
