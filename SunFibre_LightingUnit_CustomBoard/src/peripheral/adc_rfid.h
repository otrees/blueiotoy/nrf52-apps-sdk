#ifndef PERIPHERAL_ADC_RFID_H__
#define PERIPHERAL_ADC_RFID_H__

#include <stdint.h>
#include <stdbool.h>
#include "adc.h"

#include "boards.h"

// TIMER
#define ADC_RFID_TIMER_125KHz_TICKS  (16000000/125000) 
// #define TIMER_125KHz_TICKS  129
#define ADC_RFID_TIMER_DUTY_TICKS    (ADC_RFID_TIMER_125KHz_TICKS/2)


#if PROTOTYPE_VERSION == 1
	#define ADC_RFID_TIMER_OPTIMAL_TICKS     112 
#elif PROTOTYPE_VERSION == 2
	#define ADC_RFID_TIMER_OPTIMAL_TICKS     122 
#endif

// ADC RFID CHANNEL
#define ADC_RFID_NUM_CHANNELS		       	1


// void adc_rfid_gpiote_event_enable();
// void adc_rfid_gpiote_event_disable();
// void adc_rfid_sampling_event_enable();
// void adc_rfid_sampling_event_disable();

void adc_rfid_stop();
void adc_rfid_start();

void adc_rfid_init();
void adc_rfid_deinit();

#endif  /* PERIPHERAL_ADC_RFID_H__ */