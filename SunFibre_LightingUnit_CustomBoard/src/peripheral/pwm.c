#include <stdint.h>
#include <string.h>


#include "nrf_drv_pwm.h"

#include "app_config.h"

// Set duty cycle between 0 and 100%

static nrfx_pwm_t m_pwm0 = NRFX_PWM_INSTANCE(0);

uint8_t pwm_ch0_output_pins[NRF_PWM_CHANNEL_COUNT] = {
	PWM_CH0_OUTPUT_PIN0,
	PWM_CH0_OUTPUT_PIN1,
	PWM_CH0_OUTPUT_PIN2,
	PWM_CH0_OUTPUT_PIN3,
};

nrf_pwm_values_individual_t seq_values[] = {{0, 0, 0, 0}};
nrf_pwm_sequence_t const seq =
{
    .values.p_individual = seq_values,
    .length          = NRF_PWM_VALUES_LENGTH(seq_values),
    .repeats         = 0,
    .end_delay       = 0
};

void pwm_set_duty_cycle_channel0(uint8_t duty_cycle)
{
    // Declare variables holding PWM sequence values. In this example only one channel is used 
    // Check if value is outside of range. If so, set to 100%
    
    //NOTE: the MSB bit sets the polarity
    if(duty_cycle >= 100)
        seq_values->channel_0 = 100  | 0x8000 ;
    else
        seq_values->channel_0 = duty_cycle | 0x8000;
    
}

void pwm_channel0_enable()
{
    nrfx_pwm_simple_playback(&m_pwm0, &seq, 1, NRFX_PWM_FLAG_LOOP);
}


void pwm_channel0_disable()
{
    nrfx_pwm_stop(&m_pwm0, false);
}



uint8_t pwm_get_duty_cycle_channel0()
{
    return seq_values->channel_0;
}



void pwm_init_channel0()
{
    ret_code_t err_code;

    nrfx_pwm_config_t const config =
    {
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_16MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = 100,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
	//copy 
	memcpy((void *) &config.output_pins, (void const *) &pwm_ch0_output_pins[0], sizeof(pwm_ch0_output_pins));

    // Init PWM without error handler
    err_code = nrfx_pwm_init(&m_pwm0, &config, NULL);
    APP_ERROR_CHECK(err_code);
}

void pwm_uninit_channel0()
{

    // NRFX_ASSERT(p_cb->state != NRFX_DRV_STATE_UNINITIALIZED);
}


