
#ifndef PERIPHERAL_PWM_H__
#define PERIPHERAL_PWM_H__

#include <stdint.h>


void pwm_init_channel0();

void pwm_channel0_enable();
void pwm_channel0_disable();

void pwm_set_duty_cycle_channel0(uint8_t duty_cycle);

uint8_t pwm_get_duty_cycle_channel0(void);

#endif  /* PERIPHERAL_PWM_H__ */