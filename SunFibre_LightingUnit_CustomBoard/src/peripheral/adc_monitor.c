#include <stdint.h>

#include "nrfx_saadc.h"
#include "nrfx_timer.h"
#include "nrf_log.h"

#include "adc.h"
#include "adc_monitor.h"
#include "adc_rfid.h"

#include "app_config.h"
#include "boards.h"


static uint16_t monitor_results[ADC_MONITOR_NUM_SAMPLES]; 



void on_adc_monitor_sampling_done()
{
    uint16_t *adc_samples = get_adc_data();

    // check validity of samples
    if (adc_samples == NULL)  NRF_LOG_ERROR("SKIP")

    memcpy(monitor_results, adc_samples, ADC_MONITOR_NUM_SAMPLES * sizeof(uint16_t));
}


//TODO: average
uint16_t get_adc_monitor_result(uint8_t adc_channel_idx)
{
    NRFX_ASSERT(adc_channel_idx >= 0 && adc_channel_idx < ADC_MONITOR_NUM_CHANNELS);

    int32_t sum = 0;
    for (int i = 0; i < ADC_MONITOR_NUM_SAMPLES; i+=ADC_MONITOR_NUM_CHANNELS)
    {
        sum+= (int16_t) monitor_results[i + adc_channel_idx];
    }
    return (uint16_t) (sum/ADC_MONITOR_SAMPLES_PER_CHANNEL);
}


void adc_monitor_start() {}
void adc_monitor_stop()
{
    adc_monitor_deinit();
}

static void adc_monitor_deinit_timer_events()
{
    nrfx_timer_clear(&m_timer2);
    // nrf_timer_shorts_disable(m_timer2.p_reg, UINT32_MAX);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL0, -1, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, -1, false);
}



static void adc_monitor_init_timer_events()
{
    // gpiote event - compare events for toggling the pin

    // nrfx_timer_extended_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, ADC_MONITOR_TIMER_10KHz_TICKS, NRF_TIMER_SHORT_COMPARE2_CLEAR_MASK, false);
    nrfx_timer_extended_compare(&m_timer2, NRF_TIMER_CC_CHANNEL0, ADC_MONITOR_TIMER_10KHz_TICKS, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, ADC_MONITOR_TIMER_10KHz_TICKS/2, false);
}


static void adc_monitor_deinit_channels()
{
    ret_code_t err_code;
    err_code = nrfx_saadc_channel_uninit(ADC_BATTERY_CHANNEL_IDX);                            
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_saadc_channel_uninit(ADC_VLED_CHANNEL_IDX);                            
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_saadc_channel_uninit(ADC_ISNS_CHANNEL_IDX);                            
    APP_ERROR_CHECK(err_code);

    // nrfx_saadc_uninit();
}


static void adc_monitor_init_channels()
{
    ret_code_t err_code;

    // battery channel 
    nrf_saadc_channel_config_t channel_config0 = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_BATTERY_CHANNEL);
    channel_config0.acq_time = ADC_BATTERY_CHANNEL_ACQ;
    channel_config0.gain = ADC_BATTERY_CHANNEL_GAIN;
    err_code = nrfx_saadc_channel_init(ADC_BATTERY_CHANNEL_IDX, &channel_config0);                            
    APP_ERROR_CHECK(err_code);

    // vled channel 
    nrf_saadc_channel_config_t channel_config1 = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_VLED_CHANNEL);
    channel_config1.acq_time = ADC_VLED_CHANNEL_ACQ;
    channel_config1.gain = ADC_VLED_CHANNEL_GAIN;
    err_code = nrfx_saadc_channel_init(ADC_VLED_CHANNEL_IDX, &channel_config1);                            
    APP_ERROR_CHECK(err_code);

    // isns channel
    nrf_saadc_channel_config_t channel_config2 = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_ISNS_CHANNEL);
    channel_config2.acq_time = ADC_ISNS_CHANNEL_ACQ;
    channel_config2.gain = ADC_ISNS_CHANNEL_GAIN;
    err_code = nrfx_saadc_channel_init(ADC_ISNS_CHANNEL_IDX, &channel_config2);                            
    APP_ERROR_CHECK(err_code);

    // vdd channel
    // nrf_saadc_channel_config_t channel_config3 = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_VDD_INTERNAL_CHANNEL);
    // err_code = nrfx_saadc_channel_init(ADC_VDD_INTERNAL_CHANNEL_IDX, &channel_config3);                            
    // APP_ERROR_CHECK(err_code);
}

void adc_monitor_deinit()
{
    // deinit channels 
    adc_monitor_deinit_channels();
    // deinit timer events
    adc_monitor_deinit_timer_events();
    // deinit ADC
    adc_sampling_event_deinit();
}

void adc_monitor_init()
{
    adc_sampling_event_init();

    adc_monitor_init_channels();
    adc_monitor_init_timer_events();
}