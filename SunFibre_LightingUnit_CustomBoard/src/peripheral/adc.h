
#ifndef PERIPHERAL_ADC_H__
#define PERIPHERAL_ADC_H__

#include <stdint.h>
#include "nrfx_saadc.h"
#include "nrfx_timer.h"

#define ADC_CALIBRATION_INTERVAL        		10                              //Determines how often the SAADC should be calibrated relative to NRF_DRV_SAADC_EVT_DONE event. E.g. value 5 will make the SAADC calibrate every fifth time the NRF_DRV_SAADC_EVT_DONE is received.

#define ADC_CFG_RESOLUTION									NRF_SAADC_RESOLUTION_10BIT
#define ADC_CFG_OVERSAMPLE                  NRF_SAADC_OVERSAMPLE_DISABLED   //Oversampling setting for the SAADC. Setting oversample to 4x This will make the SAADC output a single averaged value when the SAMPLE task is triggered 4 times. Enable BURST mode to make the SAADC sample 4 times when triggering SAMPLE task once.
#define ADC_CFG_BURST_MODE                  0                               //Set to 1 to enable BURST mode, otherwise set to 0.
#define ADC_CFG_IRQ_PRIORITY								APP_IRQ_PRIORITY_LOW						//!!! NOTE: super important to have the same priority as for app_timer --> no interrrupt preemption


// millivolts recalculation
#define ADC_RESOLUTION                  		1024 
#define ADC_REF_VOLTAGE_MILLIVOLTS      		600  // reference voltage (in milli volts) used by ADC while doing conversion.
#define ADC_MAX_SAMPLES_IN_BUFFER						8192
// #define DIODE_FWD_VOLT_DROP_MILLIVOLTS  270 

//NOTE: (6 - ADC PRESCALER) matches the real gain multiplier
#define ADC_RESULT_MILLIVOLTS(ADC_VALUE, ADC_GAIN) \
    ((((ADC_VALUE) * ADC_REF_VOLTAGE_MILLIVOLTS * (6 - (ADC_GAIN))) / ADC_RESOLUTION) )

typedef enum
{
	ADC_MEASURE_MODE_NONE,
	ADC_MEASURE_MODE_MONITOR,
	ADC_MEASURE_MODE_RFID,
} adc_measure_mode_t;


typedef void (*app_adc_evt_handler_t)();


typedef struct
{
	adc_measure_mode_t mode;
	uint16_t num_samples;
	app_adc_evt_handler_t adc_evt_handler;
	bool start_immediately;
} adc_cfg_t;



uint16_t *get_adc_data();
uint16_t get_adc_samples_num();

bool adc_any_measure_req();
void adc_set_measure_req(adc_cfg_t *cfg);
void adc_calibrate();

void adc_start();
void adc_stop();
void adc_measure();

void adc_sampling_event_init();
void adc_sampling_event_deinit();

extern nrfx_timer_t m_timer2;

#endif  /* PERIPHERAL_ADC_H__ */