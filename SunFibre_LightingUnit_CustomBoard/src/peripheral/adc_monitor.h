#ifndef PERIPHERAL_ADC_PID_H__
#define PERIPHERAL_ADC_PID_H__

#include <stdint.h>
#include "adc.h"

// ADC
#define ADC_MONITOR_NUM_CHANNELS		        				3	                //battery, VLED, ISNS
#define ADC_MONITOR_SAMPLES_PER_CHANNEL							5
#define ADC_MONITOR_NUM_SAMPLES										 	(ADC_MONITOR_NUM_CHANNELS * ADC_MONITOR_SAMPLES_PER_CHANNEL)	

#define ADC_MONITOR_TIMER_10KHz_TICKS  							(16000000/10000)  //10 kHz

#define ADC_MONITOR_FREQUENCY												APP_TIMER_TICKS(5000)	//5s


void on_adc_monitor_sampling_done();

uint16_t get_adc_monitor_result(uint8_t adc_channel_idx);

void adc_monitor_stop();
void adc_monitor_start();
void adc_monitor_deinit();
void adc_monitor_init();


#endif  /* PERIPHERAL_ADC_PID_H__ */