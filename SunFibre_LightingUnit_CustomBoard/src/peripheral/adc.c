#include <stdint.h>

#include "nrfx_saadc.h"
#include "nrfx_timer.h"
#include "nrfx_ppi.h"
#include "nrf_log.h"


#include "adc.h"
#include "adc_monitor.h"
#include "adc_rfid.h"

#include "app_config.h"

bool ADC_DEBUG = false;


static bool adc_calibration = false;  
static bool adc_measure_req = false;

static adc_cfg_t current_cfg = {0};
static adc_cfg_t new_cfg = {0};

static nrf_saadc_value_t* adc_buffer_to_read = NULL;
static nrf_saadc_value_t adc_buffer[ADC_MAX_SAMPLES_IN_BUFFER];

nrfx_timer_t m_timer2 = NRFX_TIMER_INSTANCE(2);

static nrf_ppi_channel_t m_ppi_channel_sampling;
// static nrf_ppi_channel_t m_ppi_channel_stop;


// CONFIGS
static nrfx_saadc_config_t saadc_config = 
{
    .low_power_mode = false,
    .resolution = ADC_CFG_RESOLUTION,
    .oversample = ADC_CFG_OVERSAMPLE,
    .interrupt_priority = ADC_CFG_IRQ_PRIORITY 
};

static nrfx_timer_config_t timer_cfg =
{
    .bit_width = NRF_TIMER_BIT_WIDTH_32,
    .frequency = NRF_TIMER_FREQ_16MHz,
    .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,
    .mode = NRF_TIMER_MODE_TIMER

}; 



//NOTE: prevents overridng non-used configuration
static bool adc_current_cfg_up_to_date()
{
    return current_cfg.mode == new_cfg.mode && current_cfg.num_samples == new_cfg.num_samples;
}


bool adc_any_measure_req()
{
    return adc_measure_req;
}


void adc_set_measure_req(adc_cfg_t *cfg)
{
    if (adc_current_cfg_up_to_date())
    {
        memcpy(&new_cfg, cfg, sizeof(adc_cfg_t));
        adc_measure_req = true;
        if (ADC_DEBUG) NRF_LOG_INFO("(ADC): measure request set");
    }
    else
        NRF_LOG_WARNING("(ADC): config set SKIP");
}



static void adc_set_current_cfg(adc_cfg_t *cfg)
{
    memcpy(&current_cfg, cfg, sizeof(adc_cfg_t));
}



uint16_t *get_adc_data()
{
    return (uint16_t*) adc_buffer_to_read;
}


uint16_t get_adc_samples_num()
{
    return current_cfg.num_samples;
}

void adc_calibrate()
{
    nrfx_saadc_abort();                        // abort all ongoing conversions. Calibration cannot be run if SAADC is busy
    while(nrfx_saadc_calibrate_offset() != NRF_SUCCESS); 
    adc_calibration = false;

    if (ADC_DEBUG) NRF_LOG_DEBUG("(ADC): calibration completed!");
}

static void adc_prepare_buffers()
{
    ret_code_t err_code = nrfx_saadc_buffer_convert(adc_buffer, current_cfg.num_samples);
    APP_ERROR_CHECK(err_code);

    adc_buffer_to_read = NULL;

    if (ADC_DEBUG) NRF_LOG_INFO("(ADC): buffers prepared: %d", current_cfg.num_samples);
}

static void on_adc_evt(nrfx_saadc_evt_t const *p_event)
{

    if (p_event->type == NRFX_SAADC_EVT_DONE)
    {
        //!!! STOP ADC AT FIRST ALWAYS (no NRF_LOG) 
        adc_stop();

        adc_buffer_to_read = p_event->data.done.p_buffer;

        // NRF_LOG_WARNING("SAADC event - %d (prio. %d)", p_event->data.done.size,  current_int_priority_get());
        // for (int i = 0; i < 6; i++) NRF_LOG_RAW_INFO("%d ", p_event->data.done.p_buffer[i]);
        // NRF_LOG_RAW_INFO("\r\n");

        if (current_cfg.adc_evt_handler) current_cfg.adc_evt_handler();

    }

    else if (p_event->type == NRFX_SAADC_EVT_CALIBRATEDONE)
        adc_prepare_buffers();
}



static void adc_sampling_event_enable()
{
    ret_code_t err_code;

    nrfx_timer_enable(&m_timer2);
    err_code = nrfx_ppi_channel_enable(m_ppi_channel_sampling);
    APP_ERROR_CHECK(err_code);
}

static void adc_sampling_event_disable()
{
    ret_code_t err_code;

    nrfx_timer_disable(&m_timer2);
    err_code = nrfx_ppi_channel_disable(m_ppi_channel_sampling);
    APP_ERROR_CHECK(err_code);
}


void adc_start()
{
    //!!! must be started before 
    if (ADC_DEBUG) NRF_LOG_INFO("(ADC): START %d", current_cfg.mode);

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RLC == 1
    if (current_cfg.mode == ADC_MEASURE_MODE_RFID)
        adc_rfid_start();
#endif

    if (current_cfg.mode == ADC_MEASURE_MODE_MONITOR)
        adc_monitor_start();

    adc_sampling_event_enable();
}


void adc_stop()
{
    adc_sampling_event_disable();

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RLC == 1
    if (current_cfg.mode == ADC_MEASURE_MODE_RFID)
        adc_rfid_stop();
#endif

    if (current_cfg.mode == ADC_MEASURE_MODE_MONITOR)
        adc_monitor_stop();

    if (ADC_DEBUG) NRF_LOG_INFO("(ADC): STOP %d", current_cfg.mode);
}



void adc_measure()
{

    // check if busy
    if (nrfx_saadc_is_busy()) return;

    // setup
    if (new_cfg.mode == ADC_MEASURE_MODE_MONITOR)
        adc_monitor_init();

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RLC == 1
    if (new_cfg.mode == ADC_MEASURE_MODE_RFID)
        adc_rfid_init();
#endif
    
    // set new configuration as current
    CRITICAL_REGION_ENTER(); 
    adc_set_current_cfg(&new_cfg);
    adc_measure_req = false;
    CRITICAL_REGION_EXIT();
        
    // prepare buffers
    adc_prepare_buffers();

    // start
    if(current_cfg.start_immediately) adc_start();
}

static void on_timer_evt(nrf_timer_event_t event_type, void* p_context)
{
    NRFX_ASSERT(false);

}


void adc_sampling_event_deinit()
{
    ret_code_t err_code;

    // TIMER DEINIT
    nrfx_timer_uninit(&m_timer2);

    // PPI DEINIT
    err_code = nrfx_ppi_channel_free(m_ppi_channel_sampling);
    APP_ERROR_CHECK(err_code);

    // ADC DEINIT
    // ASSERT(!nrfx_saadc_is_busy());
    nrfx_saadc_uninit();
}


void adc_sampling_event_init()
{
    ret_code_t err_code;

    // ADC INIT
    err_code = nrfx_saadc_init(&saadc_config, on_adc_evt);
    APP_ERROR_CHECK(err_code);

    // TIMER INIT
    err_code = nrfx_timer_init(&m_timer2, &timer_cfg, (nrfx_timer_event_handler_t) on_timer_evt);
    APP_ERROR_CHECK(err_code);    

    // PPI INIT
        // TIMER CC_CH2 event --> SAADC sample task
    uint32_t timer_compare_event_ch2_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL2);
    uint32_t adc_sample_task_addr = nrfx_saadc_sample_task_get();

    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel_sampling);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_assign(m_ppi_channel_sampling, timer_compare_event_ch2_addr, adc_sample_task_addr);
    APP_ERROR_CHECK(err_code);

    //   // SAADC END event --> TIMER STOP task
    // uint32_t adc_end_event_addr = nrf_saadc_event_address_get(NRF_SAADC_EVENT_END);
    // uint32_t adc_stop_task_addr = nrf_saadc_task_address_get(NRF_SAADC_TASK_STOP);
    // // uint32_t timer_stop_task_addr = nrfx_timer_task_address_get(&m_timer2, NRF_TIMER_TASK_STOP);

    // err_code = nrfx_ppi_channel_alloc(&m_ppi_channel_stop);
    // APP_ERROR_CHECK(err_code);
    // // err_code = nrfx_ppi_channel_assign(m_ppi_channel_stop, adc_end_event_addr, timer_stop_task_addr);
    // err_code = nrfx_ppi_channel_assign(m_ppi_channel_stop, adc_end_event_addr, adc_stop_task_addr);
    // APP_ERROR_CHECK(err_code);
    // err_code = nrfx_ppi_channel_enable(m_ppi_channel_stop);
    // APP_ERROR_CHECK(err_code); 
}

