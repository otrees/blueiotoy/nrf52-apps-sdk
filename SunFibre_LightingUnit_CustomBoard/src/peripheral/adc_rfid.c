#include "adc.h"
#include "adc_rfid.h"
#include "boards.h"

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1 && RFID_AS_RLC

#include <stdint.h>

#include "nrfx_timer.h"
#include "nrfx_ppi.h"
#include "nrfx_saadc.h"
#include "nrfx_gpiote.h"

#include "nrf_log.h"


static nrf_ppi_channel_t    m_ppi_channel0;
static nrf_ppi_channel_t    m_ppi_channel1;


static void adc_rfid_gpiote_event_enable()
{
    ret_code_t err_code;
    err_code = nrfx_ppi_channel_enable(m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_enable(m_ppi_channel1);
    APP_ERROR_CHECK(err_code);
}

static void adc_rfid_gpiote_event_disable()
{
    ret_code_t err_code;
    err_code = nrfx_ppi_channel_disable(m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_disable(m_ppi_channel1);
    APP_ERROR_CHECK(err_code);
}


void adc_rfid_stop()
{
    adc_rfid_gpiote_event_disable();
    adc_rfid_deinit();
}

void adc_rfid_start()
{
    adc_rfid_gpiote_event_enable();
}


static void gpiote_init()
{
    ret_code_t err_code;

    if (!nrfx_gpiote_is_init())
    {
        err_code = nrfx_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }

    nrf_gpio_pin_write(RFID_PWM_PIN, 1);
    nrfx_gpiote_out_config_t config = NRFX_GPIOTE_CONFIG_OUT_TASK_TOGGLE(1);
    err_code = nrfx_gpiote_out_init(RFID_PWM_PIN, &config);
    APP_ERROR_CHECK(err_code);

    nrfx_gpiote_out_task_enable(RFID_PWM_PIN);
}



static void adc_rfid_gpiote_event_deinit()
{
    ret_code_t err_code;

    // GPIOTE DEINIT
    nrfx_gpiote_out_uninit(RFID_PWM_PIN);

    // PPI DEINIT
    err_code = nrfx_ppi_channel_free(m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_free(m_ppi_channel1);
    APP_ERROR_CHECK(err_code);
}

static void adc_rfid_gpiote_event_init()
{
    ret_code_t err_code;

    // GPIOTE INIT
    gpiote_init();

    // INIT PPI
    uint32_t timer_compare_event_ch0_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL0);
    uint32_t timer_compare_event_ch1_addr = nrfx_timer_compare_event_address_get(&m_timer2, NRF_TIMER_CC_CHANNEL1);

    uint32_t gpiote_set_task_addr = nrfx_gpiote_set_task_addr_get(RFID_PWM_PIN);
    uint32_t gpiote_clear_task_addr = nrfx_gpiote_clr_task_addr_get(RFID_PWM_PIN);

    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel0);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel1);
    APP_ERROR_CHECK(err_code);

    // toggle task
    err_code = nrfx_ppi_channel_assign(m_ppi_channel0, timer_compare_event_ch0_addr, gpiote_set_task_addr);
    APP_ERROR_CHECK(err_code);
    err_code = nrfx_ppi_channel_assign(m_ppi_channel1, timer_compare_event_ch1_addr, gpiote_clear_task_addr);
    APP_ERROR_CHECK(err_code);
}

static void adc_rfid_init_timer_events()
{
    // gpiote event - compare events for toggling the pin
    nrfx_timer_extended_compare(&m_timer2, NRF_TIMER_CC_CHANNEL0, ADC_RFID_TIMER_125KHz_TICKS, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL1, ADC_RFID_TIMER_DUTY_TICKS, false);

    // sampling event - set CC event on channel2
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, ADC_RFID_TIMER_OPTIMAL_TICKS, false);
}

static void adc_rfid_deinit_timer_events()
{
    nrfx_timer_clear(&m_timer2);
    // nrf_timer_shorts_disable(m_timer2.p_reg, UINT32_MAX);

    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL0, -1, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL1, -1, false);
    nrfx_timer_compare(&m_timer2, NRF_TIMER_CC_CHANNEL2, -1, false);
}



static void adc_rfid_deinit_channels()
{
    ret_code_t err_code = nrfx_saadc_channel_uninit(ADC_RFID_CHANNEL_IDX);                            
    APP_ERROR_CHECK(err_code);

    // nrfx_saadc_uninit();
}


static void adc_rfid_init_channels()
{
    ret_code_t err_code;

    nrf_saadc_channel_config_t channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_RFID_CHANNEL);
    channel_config.acq_time = ADC_RFID_CHANNEL_ACQ;
    channel_config.gain = ADC_RFID_CHANNEL_GAIN;

    err_code = nrfx_saadc_channel_init(ADC_RFID_CHANNEL_IDX, &channel_config);                            
    APP_ERROR_CHECK(err_code);
}


void adc_rfid_deinit()
{
    adc_rfid_deinit_channels();
    adc_rfid_deinit_timer_events();

    adc_rfid_gpiote_event_deinit();
    adc_sampling_event_deinit();
}

void adc_rfid_init()
{
    adc_sampling_event_init();
    adc_rfid_gpiote_event_init();

    adc_rfid_init_channels();
    adc_rfid_init_timer_events();
}
#endif