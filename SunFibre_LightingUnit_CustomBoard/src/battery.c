#include "battery.h"

#include "nrf_drv_gpiote.h"
#include "nrf_log.h"

#include "app_timer.h"

#include "boards.h"
#include "app_config.h"
#include "peripheral/adc_monitor.h"

static bool initially_charging;
static battery_data_t battery_data = {0};

APP_TIMER_DEF(m_battery_detectiondelay_timer_id); 
APP_TIMER_DEF(m_monitor_timer_id); 


// pointer to application battery event handler
static app_battery_evt_handler_t app_evt_handler;

static adc_cfg_t adc_cfg_monitor = 
{                                                        
    .mode               = ADC_MEASURE_MODE_MONITOR,      
    .adc_evt_handler 	= on_adc_monitor_sampling_done,  
    .num_samples 	    = ADC_MONITOR_NUM_SAMPLES,      
    .start_immediately 	= true,                         
};

static void estimate_battery_level()
{
    uint16_t BATTERY_THRESHOLDS(battery_thresholds);
    
    if (battery_data.battery_mV >= battery_thresholds[3])
        battery_data.battery_level = MV_4000_TO_4200;
    else if (battery_data.battery_mV >= battery_thresholds[2])
        battery_data.battery_level = MV_3800_TO_4000;
    else if (battery_data.battery_mV >= battery_thresholds[1])
        battery_data.battery_level = MV_3600_TO_3800;
    else if (battery_data.battery_mV >= battery_thresholds[0])
        battery_data.battery_level = MV_3400_TO_3600;
    else 
        battery_data.battery_level = MV_BELOW_3400;
}

int32_t measure_temperature()
{
    int32_t temperature;
    ret_code_t err_code = sd_temp_get(&temperature);
    // temperature is read in 0.25 degrees of celsia
    APP_ERROR_CHECK(err_code);
    temperature /= 4;
    return temperature;
}

void measure_battery()
{
    // get ADC result
    uint16_t adc_value;
    adc_value = get_adc_monitor_result(ADC_BATTERY_CHANNEL_IDX);
    battery_data.battery_adc_raw = adc_value;
    battery_data.battery_mV = BATTERY_CONVERT_MV(ADC_RESULT_MILLIVOLTS(adc_value, ADC_BATTERY_CHANNEL_GAIN));
    battery_data.battery_level_percent = BATTERY_LEVEL_PERCENTAGE(battery_data.battery_mV);

    // estimate battery level
    estimate_battery_level();

    // run app event handler
    app_evt_handler(EVT_BATTERY_MEASURED);
}

battery_data_t get_battery_data(bool measure)
{
    if (measure) measure_battery();
 
    return battery_data;
}


static void cb_input_charging_change(nrfx_gpiote_pin_t pin_no, nrf_gpiote_polarity_t input_action)
{
    // ret_code_t  err_code;

    // assert
    NRFX_ASSERT(pin_no == STAT_PIN_NUMBER);
    NRFX_ASSERT(input_action == NRF_GPIOTE_POLARITY_TOGGLE);

    battery_data.charging = !battery_data.charging;

    //output event after 100 ms to prevent glitches
    // starting app timer again does not do anything
    app_timer_start(m_battery_detectiondelay_timer_id, APP_TIMER_TICKS(100), NULL);
}

// output battery connected 
static void on_battery_detectiondelay_timer()
{
    app_evt_handler(battery_data.charging? EVT_BATTERY_CHARGER_CONNECTED : EVT_BATTERY_CHARGER_DISCONNECTED);
}

static void on_adc_monitor_timer()
{
    // NRF_LOG_INFO("M");
    adc_set_measure_req(&adc_cfg_monitor);
}

static void timers_create()
{
    app_timer_create(&m_battery_detectiondelay_timer_id, APP_TIMER_MODE_SINGLE_SHOT, on_battery_detectiondelay_timer); 

    app_timer_create(&m_monitor_timer_id, APP_TIMER_MODE_REPEATED, on_adc_monitor_timer); 
    app_timer_start(m_monitor_timer_id, ADC_MONITOR_FREQUENCY, NULL);
}

void battery_set_app_evt_handler(app_battery_evt_handler_t app_battery_evt_handler)
{
    app_evt_handler = app_battery_evt_handler;
}


void battery_init()
{
    ret_code_t err_code;

    //init board
    nrf_gpio_cfg_input(STAT_PIN_NUMBER, BATTERY_PULL);

    nrfx_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    config.pull = BATTERY_PULL;
    err_code = nrfx_gpiote_in_init(STAT_PIN_NUMBER, &config, cb_input_charging_change);
    APP_ERROR_CHECK(err_code);

    // enable button 
    nrfx_gpiote_in_event_enable(STAT_PIN_NUMBER, true);

    // read STAT pin at the initialization
    battery_data.charging = !(nrf_gpio_pin_read(STAT_PIN_NUMBER) ^ BUTTONS_ACTIVE_STATE);
    initially_charging = battery_data.charging;

    NRF_LOG_WARNING("(BATTERY): charging: %d", battery_data.charging);

    timers_create();
}