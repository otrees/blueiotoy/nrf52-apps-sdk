#include "lightmode.h"

#include "nrf_log.h"
#include "app_timer.h"

#include "peripheral/pwm.h"

#include "app_config.h"
#include "app_ble_services.h"

APP_TIMER_DEF(m_led_dim_timer_id); 


static app_light_mode_t app_light_mode;
static bool dim_led_low = false;


static uint8_t isns_to_pwm_duty_cycle(uint8_t desired_current)
{

    // NRF_LOG_INFO("Desired current: %d / %d", desired_current, ((uint16_t) desired_current) * 100 / MAX_LIGHNING_MA );
    return ((uint16_t) desired_current) * 100 / MAX_LIGHNING_MA;
}


static void toggle_dim_led()
{
    dim_led_low = !dim_led_low;
    if (dim_led_low)
    {
        pwm_channel0_disable();
        nrf_gpio_pin_write(DIM_LED_PWM, 0);
    }
    else
    {
        pwm_set_duty_cycle_channel0(isns_to_pwm_duty_cycle(STRONG_LIGHTNING_MA));
        pwm_channel0_enable();
    }
}


static void on_dim_led_toggle_timer_evt(void *p_context)
{
    toggle_dim_led();
}


static void set_dim_led()
{
    app_timer_stop(m_led_dim_timer_id);

    switch (app_light_mode)
    {
        case NO_LIGHTING:
            pwm_channel0_disable();
            nrf_gpio_pin_write(DIM_LED_PWM, 0);
        break;
        case STRONG_LIGHTING:   
            pwm_set_duty_cycle_channel0(isns_to_pwm_duty_cycle(STRONG_LIGHTNING_MA));
            pwm_channel0_enable();
        break;
        case MEDIUM_LIGHTING:
            pwm_set_duty_cycle_channel0(isns_to_pwm_duty_cycle(MEDIUM_LIGHTNING_MA));
            pwm_channel0_enable();
        break;
        case SLOW_BLINKING:
            dim_led_low = false;
            pwm_set_duty_cycle_channel0(isns_to_pwm_duty_cycle(STRONG_LIGHTNING_MA));     
            pwm_channel0_enable();
            app_timer_start(m_led_dim_timer_id, DIM_LED_SLOW_BLINK_INTERVAL, NULL); 
        break;
        case FAST_BLINKING:
            dim_led_low = false;
            pwm_set_duty_cycle_channel0(isns_to_pwm_duty_cycle(STRONG_LIGHTNING_MA));     
            pwm_channel0_enable();
            app_timer_start(m_led_dim_timer_id, DIM_LED_FAST_BLINK_INTERVAL, NULL); 
        break;
        default: break;
    }
}

void increment_app_light_mode(bool update_ble_characteristics)
{
    app_light_mode_t new_light_mode = ((app_light_mode + 1) == NUM_LIGHT_MODES)? 0 : (app_light_mode + 1);
    set_app_light_mode(new_light_mode, update_ble_characteristics);
}

void set_app_light_mode(app_light_mode_t new_light_mode, bool update_ble_characteristics)
{
    NRF_LOG_DEBUG("(APP): light mode changed to %d", new_light_mode);

    app_light_mode = new_light_mode;

    // set LED (pwm)
    set_dim_led();

    // set BLE char.
    if (update_ble_characteristics)
        app_ble_services_set_dim_led((uint8_t)new_light_mode, true);
}

static void timers_create()
{
  app_timer_create(&m_led_dim_timer_id, APP_TIMER_MODE_REPEATED, on_dim_led_toggle_timer_evt);
}

void light_mode_init()
{
	timers_create();
    set_app_light_mode(NO_LIGHTING, true);
}