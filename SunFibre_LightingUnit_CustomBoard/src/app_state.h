#ifndef APP_LOGIC_H__
#define APP_LOGIC_H__

#include "lightmode.h"

typedef enum
{
    IDLE,
    NOT_CONNECTED,
    NOT_CONNECTED_ADVERTISING,
    CONNECTED,
    CONNECTED_ADVERTISING,
    BONDING_ADVERTISING,
    BONDING_CONNECTED,
    NUM_APP_STATES
} app_state_t;

typedef enum
{
    BONDING_MISS_KEY,     // AZURE 0x?? (4102) ... error occures when client connects and starts security with unknown key
    BONDING_NO_SEC_REQ,   // PURPLE---         ... error occures when client connects but does not send a request to start bonding
    CONNECTION_REPAIR,    // WHITE 0x85 (133)  ... error occures when already bonded client tries to bond again
    UNIDENTIFIED_ERROR,
    NUM_APP_ERRORS
} app_error_t;


// app utils
const char *app_state_to_str(app_state_t state);
const char *app_error_to_str(app_error_t error);

#endif /* APP_LOGIC_H__ */