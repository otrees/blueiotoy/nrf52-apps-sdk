#ifndef APP_BLE_SERVICES_H__
#define APP_BLE_SERVICES_H__

#include <stdbool.h> 

#include "ble_advertising.h"


#define BLE_LCS_OBSERVER_PRIO			2
#define BLE_RS_OBSERVER_PRIO			2

// typedef enum
// {
//     EVT_BLE_SERVICE_LCS_DIM_LED_WRITE,
//     EVT_BLE_SERVICE_RS_ENABLED_WRITE,
//     EVT_BLE_SERVICE_RS_PAIREDTAG_WRITE,
//     NUM_EVTS_BLE_SERVICE
// } app_ble_service_evt_t;

typedef struct
{
	ble_advertising_t* p_advertising;

} app_ble_services_init_t;


// LIGHT CONTROL SERVICE
void app_ble_services_set_dim_led(uint8_t light_mode, bool notify);

// MONITOR SERVICE
void app_ble_services_set_battery_charge(uint8_t charging, bool notify);

// RFID SERVICE
void app_ble_services_set_detected_tag(uint32_t tag_id, bool notify);

void app_ble_services_get_adv_data(ble_uuid_t *uuid_to_advertise);

void app_ble_services_init(app_ble_services_init_t *init);

#endif /* APP_BLE_SERVICES_H__ */