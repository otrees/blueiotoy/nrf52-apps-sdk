#ifndef BATTERY_H__
#define BATTERY_H__

#include <stdint.h>
#include <stdbool.h>

#define BATTERY_ACTIVE_STATE    BUTTONS_ACTIVE_STATE
#define BATTERY_PULL            BUTTON_PULL 

#define BATTERY_LEVEL_PERCENTAGE(ADC_VALUE_MILLIVOLTS) \
    ((ADC_VALUE_MILLIVOLTS) * 100 / 3300)

//FROM LIST
// green  3.95 - 4.20
// orange 3.75 - 3.95
// red    X - 3.75
// deep   bellow X V
//2.6 V dangerous !!!

typedef enum 
{
    MV_BELOW_3400,   //0
    MV_3400_TO_3600, //1
    MV_3600_TO_3800, //2
    MV_3800_TO_4000, //3
    MV_4000_TO_4200, //4
} battery_level_t;

#define BATTERY_THRESHOLDS(name) name[4] = { \
                                            3400, \
                                            3600, \
                                            3800, \
                                            4000, \
                                                } \


typedef enum
{
    EVT_BATTERY_CHARGER_CONNECTED,
    EVT_BATTERY_CHARGER_DISCONNECTED,
    EVT_BATTERY_MEASURED,
    NUM_EVTS_BATTERY,
} app_battery_evt_t;



typedef struct
{
    bool                charging;
    battery_level_t     battery_level;
    int16_t            mcu_adc_raw;
    int16_t            mcu_mV;
    int16_t            battery_adc_raw;
    int16_t            battery_mV;
    uint8_t             battery_level_percent;
} battery_data_t;


typedef void (*app_battery_evt_handler_t)(app_battery_evt_t);

int32_t measure_temperature();

void measure_battery();

battery_data_t get_battery_data(bool measure);

void battery_set_app_evt_handler(app_battery_evt_handler_t app_battery_evt_handler);

void battery_init();

#endif  /* BATTERY_H__ */