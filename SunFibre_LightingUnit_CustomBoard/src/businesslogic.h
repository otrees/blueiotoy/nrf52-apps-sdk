#ifndef BUSINESSLOGIC_H__
#define BUSINESSLOGIC_H__

#include "boards.h"

#include "app_ble.h"
#include "app_button.h"
#include "battery.h"

// #include "app_ble_services.h"

#include "app_rfid.h"
typedef enum
{
  EVT_STATE_TIMER_ELAPSED,
	NUM_EVTS_STATE_TIMER

} app_statetimer_evt_t;


void app_on_battery_evt(app_battery_evt_t event);
void app_on_state_timer_evt(app_statetimer_evt_t event);

void app_on_btn_evt(app_btn_evt_t event, void *context);
void app_on_ble_evt(app_ble_evt_t event, void *context);
// void app_on_ble_service_evt(app_ble_service_evt_t event, void *context);
void app_on_rfid_evt(app_rfid_evt_t event, void *context);



void businesslogic_init();

#endif /* BUSINESSLOGIC_H__ */
