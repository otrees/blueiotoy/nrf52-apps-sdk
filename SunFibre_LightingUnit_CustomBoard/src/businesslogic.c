#include "businesslogic.h"
#include <stdint.h>

#include "nrf_log.h"
#include "nrf_pwr_mgmt.h"

#include "ble_conn_state.h"
#include "app_timer.h"

#include "board_buttons.h"
#include "board_rgbleds.h"

#include "app_config.h"
#include "app_state.h"
#include "app_ble_services.h"

#include "utils.h"


//TIMERS
#define START_APP_TIMER(duration)   app_timer_start(m_app_timer_id, duration, NULL);
#define STOP_APP_TIMER()            app_timer_stop(m_app_timer_id);

#define SET_LEDS()                  set_indication_led(); set_battery_led();

APP_TIMER_DEF(m_app_timer_id);


// APPLICATION
static app_state_t app_state;

static conn_handle_t m_incoming_connection = BLE_CONN_HANDLE_INVALID;


static void deep_sleep_enter()
{
    NRF_LOG_WARNING("(APP): Entering DEEP SLEEP mode");

    // turn off all RGB LEDS
    rgb_leds_off();
    // change light mode
    set_app_light_mode(NO_LIGHTING, false);

    // deinit app buttons
    app_buttons_deinit();

    // set wakeup button
    button_wakeup_enable(USER_BUTTON_PIN);

    // shutdown
    nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
}

static void timers_create()
{
    app_timer_create(&m_app_timer_id, APP_TIMER_MODE_SINGLE_SHOT, (void (*)(void *))app_on_state_timer_evt);
}

/* #region LED SETTUPS */

static void set_indication_led()
{
    uint8_t INDICATION_LED_COLORS(led_colors);

    uint8_t led_levels = led_colors[app_state];

    rgb_led_blinking_stop(INDICATION_RGB_LED_IDX);
    rgb_led_write(INDICATION_RGB_LED_IDX, led_levels);

    switch (app_state)
    {
        case BONDING_ADVERTISING:
        case NOT_CONNECTED_ADVERTISING:
        case CONNECTED_ADVERTISING:
            rgb_led_blinking_start(INDICATION_RGB_LED_IDX, led_levels, INDICATION_LED_BLINK_INTERVAL, BLINKING_REPEATED);
        break;
        default: break;
    }
}

static void set_battery_led()
{
    uint8_t BATTERY_LED_COLORS(led_colors);

    battery_data_t battery_data = get_battery_data(false);

    //stop blinking 
    rgb_led_blinking_stop(BATTERY_RGB_LED_IDX);

    // turn off led when IDLE (if not charging)
    if (app_state != IDLE || battery_data.charging)
    {
        uint8_t led_levels = led_colors[battery_data.battery_level];
        rgb_led_write(BATTERY_RGB_LED_IDX, led_levels);

        if (battery_data.charging) rgb_led_blinking_start(BATTERY_RGB_LED_IDX, led_levels, BATTERY_LED_BLINK_INTERVAL, BLINKING_REPEATED);
    }
    else
        rgb_led_write(BATTERY_RGB_LED_IDX, LED_OFF);
}
/* #endregion LED SETTUPS */

static void on_app_error(app_error_t error_code)
{
    NRF_LOG_ERROR("(APP): ERROR: %s!!!", app_error_to_str(error_code));

    //stop previous timer here (the next part is blocking)
    STOP_APP_TIMER();

    //blink for 5 seconds to signalize error
    switch (error_code)
    {
        case BONDING_MISS_KEY:
            rgb_led_blink_with_delay(INDICATION_RGB_LED_IDX, INDICATION_LED_ERROR_BONDING_MISS_KEY, 10);
        break;

        case BONDING_NO_SEC_REQ:
            rgb_led_blink_with_delay(INDICATION_RGB_LED_IDX, INDICATION_LED_ERROR_BONDING_SEC_REQ, 10);
        break;

        case CONNECTION_REPAIR:
            rgb_led_blink_with_delay(INDICATION_RGB_LED_IDX, INDICATION_LED_ERROR_BONDING_REPAIR, 10);
        break;
        default: break;
    }
}


static void extend_app_timer(int extended_duration)
{
    switch (app_state)
    {
        case NOT_CONNECTED:
        case BONDING_ADVERTISING:
        case NOT_CONNECTED_ADVERTISING:
        case CONNECTED_ADVERTISING:
            app_timer_stop(m_app_timer_id);
            app_timer_start(m_app_timer_id, extended_duration, NULL);
            NRF_LOG_DEBUG("(APP): State timer extended.");
        break;
        default: break;
    }
}

static void change_app_state(app_state_t new_state)
{

    NRF_LOG_DEBUG("(APP): State tranzition (%s - %s)", app_state_to_str(app_state), app_state_to_str(new_state));
    NRF_LOG_RAW_INFO("\n");

    // stop state timer
    app_timer_stop(m_app_timer_id);

    // stop advertising if still running
    ble_stop_advertising();

    // change global state
    app_state = new_state;

    switch (app_state)
    {
        case IDLE:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;
            //TODO: start deepsleep timer
        break;

        case NOT_CONNECTED:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;

            START_APP_TIMER(NOT_CONNECTED_MAX_DURATION);
        break;

        case CONNECTED:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;
            //TODO: any timer here???
        break;

        case BONDING_CONNECTED:
            // m_incoming_connection @defined;

            START_APP_TIMER(BONDING_CONNECTED_MAX_DURATION);
        break;

        case NOT_CONNECTED_ADVERTISING:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;

            START_APP_TIMER(NOT_CONNECTED_MAX_DURATION);
            ble_start_advertising_with_whitelist();
        break;

        case CONNECTED_ADVERTISING:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;

            START_APP_TIMER(CONNECTED_ADVER_MAX_DURATION);
            ble_start_advertising_with_whitelist();
        break;

        case BONDING_ADVERTISING:
            m_incoming_connection = BLE_CONN_HANDLE_INVALID;

            START_APP_TIMER(BONDING_ADVER_MAX_DURATION);
            ble_start_advertising_without_whitelist();
        break;
        default: break;
    }

    SET_LEDS();
}

static void change_app_state_when_unknown()
{
    uint8_t active_connections = ble_conn_state_peripheral_conn_count();
    uint8_t peers_count = ble_get_peers_count(); 
    NRF_LOG_WARNING("STATE after disconnection: %d active connections, %d peers cnt", active_connections, peers_count);

    if (active_connections) change_app_state(CONNECTED);
    else if (peers_count)   change_app_state(NOT_CONNECTED_ADVERTISING);
    else                    change_app_state(NOT_CONNECTED);
}


/* #region APPLICATION HANDLERS */
void app_on_battery_evt(app_battery_evt_t event)
{
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_DEBUG("(APP) battery event: %s", app_battery_evt_to_str(event));

    switch (event)
    {
        // so far no specific behaviour
        case EVT_BATTERY_CHARGER_CONNECTED:
            app_ble_services_set_battery_charge(1, true);
            // monitor_service_battery_charge_char_update(&m_ms, true, 1, m_active_connection);
        break;

        case EVT_BATTERY_CHARGER_DISCONNECTED:
            app_ble_services_set_battery_charge(0, true);
            // monitor_service_battery_charge_char_update(&m_ms, true, 0, m_active_connection);
        break;

        case EVT_BATTERY_MEASURED: 
        {
#if defined(BATTERY_DRAINED_SYS_OFF) && BATTERY_DRAINED_SYS_OFF == 1
            battery_data_t battery_data = get_battery_data(false);
            // NOTE: Battery Drained Out
            if (battery_data.battery_level == MV_BELOW_3400)
            {
                NRF_LOG_WARNING("(APP): measured battery voltage: %d mV", battery_data.battery_mV);
                deep_sleep_enter();
            }
#endif
        } break;
    
        default: break;
    }
    set_battery_led();
}


#define DISCONNECT_SIGNALIZE()                         {\
    ble_disconnect(m_incoming_connection);              \
    on_app_error(BONDING_NO_SEC_REQ);                   \
} 

void app_on_state_timer_evt(app_statetimer_evt_t event)
{
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_DEBUG("(APP): Timer event: %s", "EVT_STATE_TIMER_ELAPSED");

    switch (app_state)
    {
        case NOT_CONNECTED:
            NRF_LOG_INFO("(APP): add NEW PEER - no action");
            change_app_state(IDLE);
        break;

        case NOT_CONNECTED_ADVERTISING:
        {
            // !!!ERROR CASE !!!
            // CASE: NOT_CONNECTED_CONNECTED_MAX_DURATION fired, whitelisted device connected but did not start security --> disconnect and IDLE
            if (m_incoming_connection != BLE_CONN_HANDLE_INVALID)
            {
                NRF_LOG_WARNING("(APP): connect FIRST PEER - connected device (0x%x), security not started", m_incoming_connection);
                DISCONNECT_SIGNALIZE();
            }
            // CASE: NOT_CONNECTED_ADVER_MAX_DURATION fired, no action --> IDLE
            else
                NRF_LOG_INFO("(APP): connect FIRST PEER - no connection");

            change_app_state(IDLE);
        }
        break;

        case BONDING_ADVERTISING:
            NRF_LOG_WARNING("(APP): add NEW PEER - no connection");
            change_app_state_when_unknown();
        break;

        case BONDING_CONNECTED:
            // !!!ERROR CASE !!!
            // CASE: new device connected but did not start security --> disconnect and decide state according to connections 
            NRF_LOG_WARNING("(APP): add NEW PEER - security not started");
            DISCONNECT_SIGNALIZE();

            change_app_state_when_unknown();
        break;

        case CONNECTED_ADVERTISING: 
        {
            // !!!ERROR CASE !!!
            // CASE: CONNECTED_CONNECTED_MAX_DURATION fired, whitelisted device connected but did not start security --> disconnect and decide state according to connections 
            if (m_incoming_connection != BLE_CONN_HANDLE_INVALID)
            {
                NRF_LOG_WARNING("(APP): connect ANOTHER PEER - connected device (0x%x), security not started", m_incoming_connection);
                DISCONNECT_SIGNALIZE();
            }
            // CASE: CONNECTED_ADVER_MAX_DURATION fired, no action --> decide state according to connections
            else
                NRF_LOG_INFO("(APP): connect ANOTHER PEER - no connection");

            change_app_state_when_unknown();
        } break;
        default: break;
    }
}

void app_on_ble_evt(app_ble_evt_t event, void *ble_context)
{
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_DEBUG("(APP): BLE event: %s", app_ble_evt_to_str(event));

    app_ble_evt_context_t *context = (app_ble_evt_context_t*) ble_context;
    conn_handle_t conn_handle = context->conn_handle;

    switch (event)
    {
        case EVT_BLE_CONNECTED:
        {
            // do not accept any further connection
            m_incoming_connection = conn_handle;
            ble_stop_advertising();

            switch (app_state)
            {
                //TODO: maybe a new state for this??
                case NOT_CONNECTED_ADVERTISING:
                    extend_app_timer(NOT_CONNECTED_CONNECTED_MAX_DURATION);
                    NRF_LOG_INFO("(APP): connect FIRST PEER - connection (0x%x)", conn_handle);
                break;

                case CONNECTED_ADVERTISING:
                    extend_app_timer(CONNECTED_CONNECTED_MAX_DURATION);
                    NRF_LOG_INFO("(APP): connect ANOTHER PEER - connection (0x%x)", conn_handle);
                break;

                case BONDING_ADVERTISING:
                    NRF_LOG_INFO("(APP): add NEW PEER - connection (0x%x)", conn_handle);
                    change_app_state(BONDING_CONNECTED);
                break;

                default: NRF_LOG_ERROR("NO CONNECTION SHOULD HAPPED IN OTHER STATES (%s)", app_state_to_str(app_state));
            }

        } break;

        case EVT_BLE_CONNECTED_AND_BONDED:
        {
            switch(app_state)
            {
                case NOT_CONNECTED_ADVERTISING:
                    NRF_LOG_INFO("(APP): connect FIRST PEER - security ok (0x%x)", conn_handle);
                    change_app_state(CONNECTED); 
                break;

                case BONDING_CONNECTED:
                    NRF_LOG_INFO("(APP): add NEW PEER - security ok (0x%x)", conn_handle);
                    change_app_state(CONNECTED); 
                break;

                case CONNECTED_ADVERTISING:
                    NRF_LOG_INFO("(APP): connect ANOTHER PEER - security ok (0x%x), peers_count: %d", conn_handle, ble_conn_state_peripheral_conn_count());
                    change_app_state(CONNECTED); 
                break;
                default:
                    NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)app_state);
            }

        } break;

        case EVT_BLE_DISCONNECTED:
        {
            NRF_LOG_INFO("(APP): disconnect (0x%x)", conn_handle);

            switch(app_state)
            {
                case CONNECTED: 
                    NRF_LOG_INFO("(APP): disconnected connection (0x%x)", conn_handle);
                    change_app_state_when_unknown();
                break;

                // !!! Error case !!!
                // CASE: disconnection after error: BONDING_MAX_DURATION x CONNECTION_REPAIR --> start advertising again
                case NOT_CONNECTED_ADVERTISING: 
                    ASSERT(conn_handle == m_incoming_connection);
                    NRF_LOG_INFO("(APP): connect FIRST PEER - disconnected (0x%X)", conn_handle);
                    change_app_state(NOT_CONNECTED_ADVERTISING);
                break;

                case CONNECTED_ADVERTISING:
                {
                    // !!! Error case !!!
                    // CASE: disconnection after BONDING_MAX_DURATION x CONNECTION_REPAIR
                    if (m_incoming_connection == conn_handle)
                    {
                        NRF_LOG_INFO("(APP): connect ANOTHER PEER - disconnected incomming connection (0x%x)", conn_handle);
                        change_app_state_when_unknown();
                    }
                    // NOTE: stay in same state
                    else NRF_LOG_INFO("(APP): disconnected connection (0x%x)", conn_handle);
                } break;


                case BONDING_ADVERTISING:
                    // NOTE: stay in same state
                    NRF_LOG_INFO("(APP): disconnected connection (0x%x)", conn_handle);
                break;


                case BONDING_CONNECTED:
                {
                    // !!! Error case !!!
                    // NOTE: disconnection after BONDING_MAX_DURATION x BONDING_MISS_KEY x BONDING CONN_REPAIR
                    if (m_incoming_connection == conn_handle)
                    {
                        NRF_LOG_INFO("(APP): add NEW PEER - disconnected incomming connection (0x%x)", conn_handle);
                        change_app_state_when_unknown();
                    }
                    // NOTE: stay in same state
                    else  NRF_LOG_INFO("(APP): disconnected connection (0x%x)", conn_handle);
                } break;


                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)app_state);
            } 

        } break;

        case EVT_BLE_SEC_ERROR:
        {
            uint16_t security_error = context->error_code;
            NRF_LOG_DEBUG("(APP): Security error: %d / %s", security_error, app_state_to_str(app_state));
            if (security_error == UNIDENTIFIED_ERROR) NRF_LOG_ERROR("Unidentified ERROR");

            switch(app_state)
            {
                // SECURITY ERROR CONNECTION REPAIR: after connection when client starts security 
                // NOTE: disconnected automatically --> state is changed after disconnection
                case CONNECTED_ADVERTISING:
                case NOT_CONNECTED_ADVERTISING:
                    ASSERT(security_error == CONNECTION_REPAIR);
                    on_app_error(CONNECTION_REPAIR);
                break;

                // SECURITY ERROR: after connection when client start bonding 
                case BONDING_CONNECTED:
                {
                    // NOTE: not disconnected automatically --> state is changed after disconnection
                    if (security_error == BONDING_MISS_KEY)
                    {
                        ble_disconnect(m_incoming_connection);
                        on_app_error(BONDING_MISS_KEY);
                    }

                    // NOTE: disconnected automatically --> state is changed after disconnection
                    if (security_error == CONNECTION_REPAIR)
                        on_app_error(security_error);
                } break;

                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)app_state);
            } 

        } break;

        case EVT_BLE_BONDING_REMOVED:
        {
            switch(app_state)
            {
                case BONDING_ADVERTISING: 
                case BONDING_CONNECTED: 
                    NRF_LOG_INFO("(APP): All bonds has been removed!");
                    change_app_state(NOT_CONNECTED);
                break;

                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)app_state);
            } 
        } break;

        default: break;
    }
}

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
void app_on_rfid_evt(app_rfid_evt_t event, void *rfid_context)
{
    NRF_LOG_DEBUG("(APP) RFID event");
    app_rfid_evt_context_t *context = (app_rfid_evt_context_t*) rfid_context;

    switch (event)
    {
        case EVT_RFID_DETECTED:
            NRF_LOG_INFO("(APP): RFID detected, tag: %d (%s), paired: %d", context->rfid_tag, context->rfid_tag_array, context->paired);
            if (context->paired) increment_app_light_mode(true);
        break;
        default: break;
    }
}
#endif

void app_on_btn_evt(app_btn_evt_t event, void *btn_context)
{
    NRF_LOG_DEBUG("(APP) btn event: %s", app_btn_evt_to_str(event));
    
    switch (event)
    {
        // NOTE: extend state timer if button is pressed (this prevents unwanted state changes)
        case EVT_BTN_PRESS_START:
        {
            switch (app_state)
            {
                case NOT_CONNECTED:
                    extend_app_timer(NOT_CONNECTED_MAX_DURATION);
                break;

                case BONDING_ADVERTISING:
                    extend_app_timer(BONDING_ADVER_MAX_DURATION);
                break;

                case NOT_CONNECTED_ADVERTISING:
                {
                    if (m_incoming_connection == BLE_CONN_HANDLE_INVALID)
                        extend_app_timer(NOT_CONNECTED_ADVER_MAX_DURATION);
                } break;


                case CONNECTED_ADVERTISING:
                    if (m_incoming_connection == BLE_CONN_HANDLE_INVALID)
                        extend_app_timer(CONNECTED_ADVER_MAX_DURATION);
                break;
                
                default: break;
            }

        } break;

        // NOTE: inform user to release button if desired
        case EVT_BTN_PRESSINTERVAL_PASSED:
            rgb_led_blinking_start(DEBUG_RGB_LED_IDX, LED_WHITE, DEBUG_LED_BTN_PRESSED_BLINK_INTERVAL, 2);
        break;

        case EVT_BTN_SHORT_PRESS:
        {
            switch (app_state)
            {
                case IDLE:
                {
                    uint32_t num_peers = ble_get_peers_count();
                    NRF_LOG_DEBUG("(APP): num of peers %d", num_peers);
                    change_app_state(num_peers? NOT_CONNECTED_ADVERTISING : NOT_CONNECTED);
                } break;

                case CONNECTED:
                    change_app_state(CONNECTED_ADVERTISING);
                break;          
                
                default: break;
            }
        } break;

        case EVT_BTN_LONG_PRESS: 
        {
            switch (app_state)
            {
                case NOT_CONNECTED:
                case NOT_CONNECTED_ADVERTISING:
                case BONDING_ADVERTISING:
                case BONDING_CONNECTED:
                case CONNECTED:
                case CONNECTED_ADVERTISING:
                    increment_app_light_mode(true);
                break;
                default: break;
            } 
        } break;

        case EVT_BTN_VERY_LONG_PRESS:
        {
            switch (app_state)
            {
                // BONDING PROCEDURE
                case NOT_CONNECTED:
                case CONNECTED:
                    change_app_state(BONDING_ADVERTISING);
                break;

                case NOT_CONNECTED_ADVERTISING:
                // case CONNECTED_ADVERTISING:
                {
                    // start bonding only if no incomming connection
                    if (m_incoming_connection == BLE_CONN_HANDLE_INVALID)
                        change_app_state(BONDING_ADVERTISING);
                } break;

                // DELETING BONDS PROCEDURE
                case BONDING_CONNECTED:
                case BONDING_ADVERTISING:
                    // stop advertising
                    ble_stop_advertising();
                    // disconnect all
                    ble_disconnect_all();
                    // delete bonds
                    ble_delete_bonds();
                break;
                default: break;
            } 
        } break;

        case EVT_BTN_VERY_VERY_LONG_PRESS:
            rgb_led_blink_with_delay(INDICATION_RGB_LED_IDX, LED_RED, 10);
            NVIC_SystemReset();
        break;

        default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)app_state);
    }
}


void businesslogic_init()
{

    timers_create();
    // ble_conn_state_init();

    // init buttons
    app_btn_init_t button_init;
    button_init.button_pin = USER_BUTTON_PIN;
    button_init.app_evt_handler = (app_btn_evt_handler_t) &app_on_btn_evt;
    app_button_init(&button_init);
    NRF_LOG_INFO("(APP): Button initialized");

    // init RFID
#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
    app_rfid_init_t rfid_init;
    rfid_init.app_evt_handler = (app_rfid_evt_handler_t) &app_on_rfid_evt;
    app_rfid_init(&rfid_init);
    // app_rfid_enable();
#endif

    // init BLE
    app_ble_init_t init;
    init.app_evt_handler = (app_ble_evt_handler_t) &app_on_ble_evt; 
    ble_init(&init);

    // init battery
    battery_init();
    battery_set_app_evt_handler((app_battery_evt_handler_t) &app_on_battery_evt);

    // set app state
    change_app_state(IDLE);
    // set light mode
    light_mode_init();
}