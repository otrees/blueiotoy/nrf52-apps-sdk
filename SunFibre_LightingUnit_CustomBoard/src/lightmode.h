
#ifndef LIGHTMODE_H__
#define LIGHTMODE_H__

#include <stdbool.h>
#include "boards.h" 
#include "peripheral/adc.h" 


#define ISNS_MA_TO_ADC_RAW(I, ADC_GAIN) (I * ADC_RESOLUTION /((6 - ADC_GAIN) * ADC_REF_VOLTAGE_MILLIVOLTS))

#define STRONG_LIGHTNING_MA         (130)       
#define MEDIUM_LIGHTNING_MA         (65)       
#define STRONG_LIGHTNING_ADC_RAW    ISNS_MA_TO_ADC_RAW(STRONG_LIGHTNING_MA, ADC_ISNS_CHANNEL_GAIN)       
#define MEDIUM_LIGHTNING_ADC_RAW    ISNS_MA_TO_ADC_RAW(MEDIUM_LIGHTNING_MA, ADC_ISNS_CHANNEL_GAIN)       


#define MAX_LIGHNING_MA             (130)


typedef enum
{
    NO_LIGHTING,
    STRONG_LIGHTING,
    MEDIUM_LIGHTING,
    SLOW_BLINKING,
    FAST_BLINKING,
    NUM_LIGHT_MODES
} app_light_mode_t;




void set_app_light_mode(app_light_mode_t new_light_mode, bool update_ble_characteristics);
void increment_app_light_mode(bool update_ble_characteristics);

void light_mode_init();

#endif /* LIGHTMODE_H__ */
