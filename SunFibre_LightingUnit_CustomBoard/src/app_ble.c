#include "app_ble.h"

#include <string.h>
#include <stdlib.h>

#include "nrf_log.h"

#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"

#include "ble_conn_params.h"
#include "ble_conn_state.h"

#include "peer_manager.h"
#include "peer_manager_handler.h"

#include "app_config.h"
#include "app_ble_config.h"
#include "app_ble_services.h"
#include "app_ble_advertising.h"
#include "utils.h"

// BLE_ADVERTISING_DEF(m_advertising);                                 
NRF_BLE_GATT_DEF(m_gatt);                                          
NRF_BLE_QWRS_DEF(m_qwr, NRF_SDH_BLE_PERIPHERAL_LINK_COUNT);   

// pointer to application BLE event handler
static app_ble_evt_handler_t app_evt_handler;

// advertising
static pm_peer_id_t      m_peer_id;                                                 /**< Device reference handle to the current bonded central. */

/* #region QWR */
static void qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

// initialize Queued Write Module.
static void qwr_init()
{
    nrf_ble_qwr_init_t qwr_init = {0};
    qwr_init.error_handler = qwr_error_handler;

    for (int i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++)
    {
        ret_code_t err_code = nrf_ble_qwr_init(&m_qwr[i], &qwr_init);
        APP_ERROR_CHECK(err_code);
    }
}
/* #endregion QWR */


/* #region GATT */
/**@brief GATT module event handler.
 */
static void on_gatt_evt(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    switch (p_evt->evt_id)
    {
        case NRF_BLE_GATT_EVT_ATT_MTU_UPDATED:
            NRF_LOG_INFO("(BLE): GATT ATT MTU on connection 0x%x changed to %d", p_evt->conn_handle, p_evt->params.att_mtu_effective);
        break;

        case NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED:
            NRF_LOG_INFO("(BLE): GATT Data length on connection 0x%x changed to %d", p_evt->conn_handle, p_evt->params.data_length);
        break;
        default: break;
    }
}

/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, on_gatt_evt);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */

/* #region  CONNECTION PARAMETERS */

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module that
 *          are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply
 *       setting the disconnect_on_fail config parameter, but instead we use the event
 *       handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(p_evt->conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        NRF_LOG_ERROR("(BLE-CP): BLE_CONN_PARAMS_EVT_FAILED, error code: %d", err_code);
        APP_ERROR_CHECK(err_code);
    }
    else if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_SUCCEEDED)
    {
        NRF_LOG_WARNING("(BLE-CP): Connection parameters changed successfully");
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL; //from GAP params
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */


/* #region  GAP  */
/**@brief Function for handling the Connected event.
 *
 * @param[in] p_gap_evt GAP event received from the BLE stack.
 */

static void _on_ble_connected_evt(ble_gap_evt_t const *p_gap_evt)
{
    ret_code_t  err_code;
    uint32_t    peripheral_link_cnt = ble_conn_state_peripheral_conn_count();

    // assign connection handle to available instance of QWR module.
    for (int index = 0; index < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT ; index++)
    {
        if (m_qwr[index].conn_handle == BLE_CONN_HANDLE_INVALID)
        {
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr[index], p_gap_evt->conn_handle);
            APP_ERROR_CHECK(err_code);
            break;
        }
    }

    // NRF_LOG_WARNING("Connected (prio. %d)", current_int_priority_get());
    NRF_LOG_INFO("(BLE): Connected - link: 0x%x, total: %d", p_gap_evt->conn_handle, peripheral_link_cnt);

    // call app event handler
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_gap_evt->conn_handle;

    app_evt_handler(EVT_BLE_CONNECTED, (void*)app_ble_evt_context);
    free(app_ble_evt_context);
}

/**@brief Function for handling the Disconnected event.
 *
 * @param[in] p_gap_evt GAP event received from the BLE stack.
 */
static void _on_ble_disconnected_evt(ble_gap_evt_t const *p_gap_evt)
{
    uint32_t periph_link_cnt = ble_conn_state_peripheral_conn_count();
    uint16_t disconnect_reason = p_gap_evt->params.disconnected.reason;

    NRF_LOG_INFO("(BLE): Disconnected - link 0x%x, total: %d, reason: %d", p_gap_evt->conn_handle, periph_link_cnt, disconnect_reason);

    // call app event handler
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_gap_evt->conn_handle;
    app_ble_evt_context->error_code = disconnect_reason;

    app_evt_handler(EVT_BLE_DISCONNECTED, (void*) app_ble_evt_context);
    free(app_ble_evt_context);
}


static void on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context)
{
    ret_code_t err_code;

    ble_gap_evt_t const *p_gap_evt = &p_ble_evt->evt.gap_evt;

    // NRF_LOG_INFO("(BLE): on evt %s (%d)", gap_evt_id_to_str(p_ble_evt->header.evt_id), p_ble_evt->header.evt_id);

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("(BLE): CONNECTED");
            _on_ble_connected_evt(p_gap_evt);
        break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("(BLE): DISCONNECTED, reason 0x%x.", p_gap_evt->params.disconnected.reason);
            _on_ble_disconnected_evt(p_gap_evt);
        break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            //err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            // APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

         case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            break;

        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
             NRF_LOG_INFO("BLE_GAP_EVT_AUTH_STATUS: status=0x%x bond=0x%x lv4: %d kdist_own:0x%x kdist_peer:0x%x",
                          p_ble_evt->evt.gap_evt.params.auth_status.auth_status,
                          p_ble_evt->evt.gap_evt.params.auth_status.bonded,
                          p_ble_evt->evt.gap_evt.params.auth_status.sm1_levels.lv4,
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_own),
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_peer));
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            NRF_LOG_DEBUG("BLE_GATTS_EVT_SYS_ATTR_MISSING");
            err_code = sd_ble_gatts_sys_attr_set(p_ble_evt->evt.gap_evt.conn_handle, NULL, 0, 0);
            //APP_ERROR_CHECK(err_code);
            if (err_code != NRF_SUCCESS)
                NRF_LOG_ERROR("(BLE): attribute missing (code: %d)", err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_ERROR("(BLE): GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_ERROR("(BLE): GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    // BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = CONN_MIN_INTERVAL;
    gap_conn_params.max_conn_interval = CONN_MAX_INTERVAL;
    gap_conn_params.slave_latency     = CONN_SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */


/* #region  PEER MANAGER */
static void _on_pm_connsecsucceded_evt(pm_evt_t const *p_evt)
{
    // call app event handler
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_evt->conn_handle;

    app_evt_handler(EVT_BLE_CONNECTED_AND_BONDED, (void*)app_ble_evt_context);
    free(app_ble_evt_context);
}

static void _on_pm_securityfailed_evt(pm_evt_t const *p_evt)
{

    uint16_t error_code;
    uint16_t security_error = p_evt->params.conn_sec_failed.error;

    switch (security_error)
    {
        case PM_CONN_SEC_ERROR_PIN_OR_KEY_MISSING:
            error_code = BONDING_MISS_KEY;
        break;
        case BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP:
            error_code = CONNECTION_REPAIR;
        break;
        default:
            error_code = UNIDENTIFIED_ERROR;
    }
    NRF_LOG_INFO("(BLE-PM): Security Failed - link: 0x%x, reason: %d (0x%x)/%d", p_evt->conn_handle, security_error, security_error, error_code);

    // call app event handler
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_evt->conn_handle;
    app_ble_evt_context->error_code = error_code;

    app_evt_handler(EVT_BLE_SEC_ERROR, (void*)app_ble_evt_context);
    free(app_ble_evt_context);
}


static void _on_pm_bondsdeleted_evt(pm_evt_t const *p_evt)
{
    app_evt_handler(EVT_BLE_BONDING_REMOVED, NULL);
}

static void _on_pm_securityconfigreq_evt(pm_evt_t const *p_evt)
{
    pm_conn_sec_config_t conn_sec_config = {.allow_repairing = APP_ALLOW_REPAIRING};
    pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
}

static void on_pm_evt(pm_evt_t const * p_evt)
{
    // standard function for maintaining room in flash based on Peer Manager events.
    pm_handler_flash_clean(p_evt);

    NRF_LOG_INFO("(BLE-PM): on PM evt %s (%d)", pm_evt_id_to_str(p_evt->evt_id), p_evt->evt_id);

    switch (p_evt->evt_id)
    {

        case PM_EVT_BONDED_PEER_CONNECTED:
            NRF_LOG_INFO("(BLE-PM): ALREADY PAIRED");
            // _on_pm_bondedpeerconnected_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_START:
            NRF_LOG_INFO("(BLE-PM): SECURITY STARTED");
        break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
            NRF_LOG_INFO("(BLE-PM): BONDED");
            //TODO: trial
            m_peer_id = p_evt->peer_id;
            _on_pm_connsecsucceded_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_FAILED:
            NRF_LOG_INFO("(BLE-PM): SECURITY FAILED");
            _on_pm_securityfailed_evt(p_evt);
        break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            NRF_LOG_INFO("(BLE-PM): PEERS DELETED.");
            _on_pm_bondsdeleted_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
            NRF_LOG_INFO("(BLE-PM): SECURITY CONFIG REQ..");
            _on_pm_securityconfigreq_evt(p_evt);
        break;

        // @NOT HANDLED
        case PM_EVT_PEER_DATA_UPDATE_FAILED:
            NRF_LOG_ERROR("(BLE): PEER DATA UPDATE (code: %d)",  p_evt->params.peer_data_update_failed.error);
        break;

        // @NOT HANDLED
        case PM_EVT_PEERS_DELETE_FAILED:
            NRF_LOG_ERROR("(BLE): PEERS DELETE (err. code: %d)", p_evt->params.peers_delete_failed_evt.error);
        break;

        // @NOT HANDLED
        case PM_EVT_ERROR_UNEXPECTED:
            NRF_LOG_ERROR("(BLE-PM): UNEXPECTED ERROR (err. code: %d)", p_evt->params.error_unexpected.error);
        break;

        default: break;
    }
}

static void peer_manager_init()
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(on_pm_evt);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */


static void ble_stack_init()
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, on_ble_evt, NULL);
}

/* #region  PUBLIC */
void ble_disconnect(conn_handle_t conn_handle)
{
    ble_conn_state_status_t conn_status = ble_conn_state_status(conn_handle);
    if (conn_status == BLE_CONN_STATUS_CONNECTED)
    {
        ret_code_t err_code = sd_ble_gap_disconnect(conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        if (err_code != NRF_SUCCESS)
            NRF_LOG_ERROR("(BLE): CMD - disconnect (err. code: %d)", err_code);
    }
    else
        NRF_LOG_WARNING("(BLE): Connection handle %d has already been disconnected!", conn_handle);
}

static void _disconnect(conn_handle_t conn_handle, void *p_context)
{
    ble_disconnect(conn_handle);
}

void ble_disconnect_all()
{
    uint32_t conn_count = ble_conn_state_for_each_connected(_disconnect, NULL);
    NRF_LOG_INFO("(BLE-S): Disconnected %d links.", conn_count);
}

void ble_start_advertising_with_whitelist()
{
    advertising_start(true, false);
}


void ble_start_advertising_without_whitelist()
{
    advertising_start(false, false);
}

void ble_stop_advertising()
{
    advertising_stop();
}

void ble_delete_bonds()
{
    ret_code_t err_code = pm_peers_delete();
    if (err_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(BLE): CMD - delete bonds (err. code: %d)", err_code);
    APP_ERROR_CHECK(err_code);
}

uint32_t ble_get_peers_count()
{
    return pm_peer_count();
}

void ble_init(app_ble_init_t *init)
{
    app_evt_handler = init->app_evt_handler;

    // init BLE stack
    ble_stack_init();

    // init GAP parameters
    gap_params_init();

    // init peer manager
    peer_manager_init();


    // init GATT
    gatt_init();

    // init connection parameters
    conn_params_init();

    // init GWR
    qwr_init();

    // init BLE services
    app_ble_services_init(NULL);

    //init advertising
    ble_uuid_t   adv_uuids[ADVER_MAX_NUM_UUIDS];                                
    app_ble_services_get_adv_data(&(adv_uuids[0]));

    advertising_init(adv_uuids, ADVER_NUM_UUIDS);

    ble_gap_addr_t ble_addr;
    sd_ble_gap_addr_get(&ble_addr);
    NRF_LOG_WARNING("Device address: 0x%6x", ble_addr.addr)

}
/* #endregion  PUBLIC */