#include "app_state.h"


static const char *app_state_str[] = {
    "IDLE",
    "NOT_CONNECTED",
    "NOT_CONNECTED_ADVERTISING",
    "CONNECTED",
    "CONNECTED_ADVERTISING",
    "BONDING_ADVERTISING",
    "BONDING_CONNECTED",
};

static const char *app_error_str[] = {
    "BONDING_MISS_KEY",
    "BONDING_NO_SEC_REQ",     
    "CONNECTION_REPAIR",     
};


const char *app_state_to_str(app_state_t state){
    return app_state_str[state];
}

const char *app_error_to_str(app_error_t error){
    return app_error_str[error];
}