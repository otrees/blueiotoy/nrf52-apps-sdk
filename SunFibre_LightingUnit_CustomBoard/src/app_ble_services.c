#include "app_ble_services.h"

#include <stdint.h>

#include "ble_conn_state.h"
#include "ble_dis.h"
#include "ble_dfu.h"

#include "nrf_log.h"
#include "app_timer.h"

#include "peripheral/adc_monitor.h"
#include "ble_monitor_service.h"
#include "ble_led_control_service.h"
#include "ble_rfid_service.h"


#include "app_config.h"
#include "app_ble_config.h"

#include "lightmode.h"
#include "app_rfid.h"
#include "battery.h"
#include "app_ble.h" // TODO: CIRCULAR DEPENDENCY HERE


#include "ble_service_utils.h"
#include "utils.h"

// LED Control Service client structure
BLE_LCS_DEF(m_lcs); 
BLE_MS_DEF(m_ms); 

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
BLE_RS_DEF(m_rs); 
#endif



// MS timers
APP_TIMER_DEF(m_monitor_service_char_timer_id);

APP_TIMER_DEF(m_backup_timer_id);


/* #region DFU */
    // CLIENT CHANGE
/** Function for handling dfu events from the Buttonless Secure DFU service */
static void on_ble_dfu_evt(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
        {
            NRF_LOG_INFO("(BLE-S): Device is preparing to enter bootloader mode.");
            ble_disconnect_all();
            break;
        }

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            NRF_LOG_INFO("(BLE-S): Device will enter bootloader mode.");
            APP_ERROR_CHECK(false);
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("(BLE-S): Request to enter bootloader mode failed asynchroneously.");
            APP_ERROR_CHECK(false);
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("(BLE-S): Request to send a response to client failed.");
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("(BLE-S): Unknown event from ble_dfu_buttonless.");
    }
}
/* #endregion DFU */

/* #region LCS */
    // CLIENT CHANGE
static void on_ble_led_control_debug_led_write_evt(ble_lcs_t *p_led_control, uint8_t *levels)
{
    uint8_t led_levels = (uint8_t) binArray2int(levels, 3);
    rgb_led_write(DEBUG_RGB_LED_IDX, led_levels);
}

static void on_ble_led_control_dim_led_write_evt(ble_lcs_t *p_led_control, uint8_t mode)
{
    // valid light mode
    if (mode >= 0 && mode <= NUM_LIGHT_MODES)
        set_app_light_mode((app_light_mode_t) mode, false);
    // increment light mode
    else if (mode == 0xFF)
        increment_app_light_mode(true);
    else
        NRF_LOG_ERROR("(BLE-S): Invalid light mode (%d)", mode);
}
    // SERVER CHANGE
void app_ble_services_set_dim_led(uint8_t light_mode, bool notify)
{
    led_control_service_dim_led_char_update(&m_lcs, light_mode);
    if (notify) 
        led_control_service_dim_led_char_notify(&m_lcs, light_mode, BLE_CONN_HANDLE_ALL);
}

/* #endregion LCS */

/* #region RFID */

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
    // CLIENT CHANGE
static void on_ble_rfid_paired_tag_write_evt(ble_rs_t *p_rfid, uint32_t tag_id)
{
    NRF_LOG_INFO("(BLE-S): RFID paired tag changed to: %d", tag_id);
    app_rfid_set_paired_tag(tag_id);
}

static void on_ble_rfid_enabled_write_evt(ble_rs_t *p_rfid, uint8_t enabled)
{
    NRF_LOG_INFO("(BLE-S): RFID enabled changed to: %d", enabled);
    if (enabled)
        app_rfid_enable();
    else
        app_rfid_disable();
}

    // SERVER CHANGE
void app_ble_services_set_detected_tag(uint32_t tag_id, bool notify)
{
    rfid_service_detected_tag_char_update(&m_rs, tag_id);
    if (notify) 
        rfid_service_detected_tag_char_notify(&m_rs, tag_id, BLE_CONN_HANDLE_ALL);
}
#endif
/* #endregion RFID */



/* #region MONITOR */
    // SERVER CHANGE
void app_ble_services_set_battery_charge(uint8_t battery_charge, bool notify)
{
    monitor_service_battery_charge_char_update(&m_ms, battery_charge);
    if (notify)
        monitor_service_battery_charge_char_notify(&m_ms, battery_charge, BLE_CONN_HANDLE_ALL);
}

static void on_timer_ms_evt(void *p_context)
{   
    int16_t adc_value, vled_mV, isns_mA;
    NRF_LOG_RAW_INFO("\n--- Maintenance service callback ---\n");

    // measure
    int32_t temperature = measure_temperature();
    battery_data_t battery_data = get_battery_data(true);

    NRF_LOG_DEBUG("Temperature read: %d", temperature);
    NRF_LOG_DEBUG("ADC battery: %d / %d mV / %d %% (%d)",
        battery_data.battery_adc_raw, battery_data.battery_mV, battery_data.battery_level_percent, battery_data.battery_level);

    adc_value = get_adc_monitor_result(ADC_VLED_CHANNEL_IDX);
    adc_value = ADC_RESULT_MILLIVOLTS(adc_value, ADC_VLED_CHANNEL_GAIN);
    vled_mV = VLED_CONVERT_MV(adc_value);
    NRF_LOG_DEBUG("ADC VLED: %d / %d mV", adc_value, vled_mV);

    adc_value = get_adc_monitor_result(ADC_ISNS_CHANNEL_IDX);
    adc_value = ADC_RESULT_MILLIVOLTS(adc_value, ADC_ISNS_CHANNEL_GAIN);
    isns_mA = ISNS_CONVERT_MA(adc_value);
    NRF_LOG_DEBUG("ADC ISNS: %d / %d mA",  adc_value, isns_mA);

    // TODO: move to app callback
    monitor_service_battery_level_char_update(&m_ms, battery_data.battery_level, battery_data.battery_mV);
    monitor_service_battery_charge_char_update(&m_ms, battery_data.charging);
    monitor_service_temp_char_update(&m_ms, temperature);
    led_control_service_vled_char_update(&m_lcs, vled_mV);
    led_control_service_isns_char_update(&m_lcs, isns_mA);

    NRF_LOG_RAW_INFO("--- Maintenance service callback ---\n");

    // run timer again
    app_timer_start(m_monitor_service_char_timer_id, BLE_MONITOR_SERVICE_INTERVAL, NULL);
}
/* #endregion MONITOR */



#if defined(BLE_CHAR_VALUES_BACKUP) && BLE_CHAR_VALUES_BACKUP == 1
void backup_characteristic_values()
{
    uint8_t light_mode_flash, enabled_flash; uint32_t tag_id_flash; 
    uint8_t light_mode_ch, enabled_ch; uint32_t tag_id_ch; 

    ble_get_char_value(m_lcs.dim_led_handles.value_handle, LCS_SIZE_DIM_LED_CHAR, &light_mode_ch);

#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
    ble_get_char_value(m_rs.paired_tag_handles.value_handle, RS_SIZE_PAIRED_TAG_CHAR, (uint8_t*) &tag_id_ch);
    ble_get_char_value(m_rs.enabled_handles.value_handle, RS_SIZE_ENABLED_CHAR, &enabled_ch);
#endif

    get_char_values_from_flash(&light_mode_flash, &tag_id_flash, &enabled_flash);

    bool flash_needed = light_mode_flash != light_mode_ch || tag_id_ch != tag_id_flash || enabled_flash != enabled_ch;
    if (flash_needed)
    {
        NRF_LOG_WARNING("(BLE-S): Char. Values backup - %d/%d  %d/%d  %d/%d", 
            light_mode_ch, light_mode_flash,
            tag_id_ch, tag_id_flash,
            enabled_ch, enabled_flash
        );

        erase_and_set_char_values_to_flash(light_mode_ch, tag_id_ch, enabled_ch);
    }
}

static void on_timer_backup_evt()
{
    backup_characteristic_values();
}
#endif


static void dis_init()
{
    ret_code_t err_code;

    ble_dis_init_t dis_init;
    memset(&dis_init, 0, sizeof(ble_dis_init_t));

    ble_srv_ascii_to_utf8(&(dis_init.manufact_name_str), (char *)MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&(dis_init.model_num_str), (char *) MODEL_NUMBER);

    // set read permission
    dis_init.dis_char_rd_sec = SEC_OPEN;

    err_code = ble_dis_init(&dis_init);
    NRF_LOG_INFO("(BLE-S): DIS initialized (ret code: %d)", err_code);
    APP_ERROR_CHECK(err_code);
}

void app_ble_services_get_adv_data(ble_uuid_t *uuid_to_advertise)
{
    // NOTE: advertise only monitor service UUIS
    led_control_service_get_adv_data(&m_lcs, uuid_to_advertise);
}

static void timers_create()
{
    app_timer_create(&m_monitor_service_char_timer_id, APP_TIMER_MODE_SINGLE_SHOT, on_timer_ms_evt);
    app_timer_start(m_monitor_service_char_timer_id, BLE_MONITOR_SERVICE_ON_INIT_DELAY, NULL);

#if defined(BLE_CHAR_VALUES_BACKUP) && BLE_CHAR_VALUES_BACKUP == 1
    app_timer_create(&m_backup_timer_id, APP_TIMER_MODE_REPEATED, on_timer_backup_evt);
    app_timer_start(m_backup_timer_id, BLE_CHAR_VALUES_BACKUP_TIMER_INTERVAL, NULL);
#endif
}

/** Function for initializing services that will be used by the application */
void app_ble_services_init(app_ble_services_init_t *init)
{
    // initialize LCS
    ble_lcs_init_t led_control_init = 
    {
        .debug_led_write_handler = on_ble_led_control_debug_led_write_evt,
        .dim_led_write_handler = on_ble_led_control_dim_led_write_evt,
    };
    led_control_service_init(&m_lcs, &led_control_init);

    // initialize MS
    monitor_service_init(&m_ms);

    // initialize RS
#if defined(BOARD_WITH_RFID) && BOARD_WITH_RFID == 1
    ble_rs_init_t rfid_init = 
    {
        .paired_tag_write_handler = on_ble_rfid_paired_tag_write_evt,
        .enabled_write_handler = on_ble_rfid_enabled_write_evt,
    };
    rfid_service_init(&m_rs, &rfid_init);

    // set initial values
    uint8_t enabled;
    uint32_t tag_id;
    ble_get_char_value(m_rs.enabled_handles.value_handle, RS_SIZE_ENABLED_CHAR, &enabled);
    ble_get_char_value(m_rs.paired_tag_handles.value_handle, RS_SIZE_PAIRED_TAG_CHAR, (uint8_t*)&tag_id);
    on_ble_rfid_enabled_write_evt(&m_rs, enabled);
    on_ble_rfid_paired_tag_write_evt(&m_rs, tag_id);
#endif
    // initialize DIS
    dis_init();

    //initialize DFUS
    ble_dfu_buttonless_init_t dfus_init = {0};
    dfus_init.evt_handler = on_ble_dfu_evt;
    ret_code_t err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    // run MS timers
    timers_create();
}

