#include <stdint.h>
#include <string.h>

#include "app_timer.h"
#include "app_error.h"

#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"
// LOG
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

// BOARD
#include "board_buttons.h"
#include "board_rgbleds.h"
#include "flash.h"

// APP PERIPHERAL
#include "peripheral/adc_rfid.h"
#include "peripheral/pwm.h"

#include "businesslogic.h"

#include "ble_service_utils.h"




#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


// This call can be used for debug purposes during application development.
// @note CAUTION: Activating this code will write the stack to flash on an error.
//                This function should NOT be used in a final product.
//                It is intended STRICTLY for development/debugging purposes.
//                The flash write will happen EVEN if the radio is active, thus interrupting
//                any communication.
//                Use with care. Uncomment the line below to use.
// On assert, the system can only recover with a reset.
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name)
{
  NRF_LOG_ERROR("APP ERROR HANDLER: %s:%d err.code =%d\n", p_file_name, line_num, error_code);
  NRF_LOG_FLUSH();
  nrf_delay_ms(3000);
  NVIC_SystemReset();
}

void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    NRF_LOG_ERROR("Received a fault! id: 0x%08x, pc: 0x%08x, info: 0x%08x", id, pc, info);
    assert_info_t *p_info = (assert_info_t *) info;
    NRF_LOG_ERROR("ASSERTION FAILED at %s:%u", p_info->p_file_name, p_info->line_num);
}

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


static void log_init()
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INTERNAL_FINAL_FLUSH(); //NOTE: this needs to be here because of bootloader 
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle()
{
    if (NRF_LOG_PROCESS() == false)
        nrf_pwr_mgmt_run();
}


static void check_setups()
{
    if (adc_any_measure_req())
    {
        adc_measure();
    }
}


static void board_init() 
{
    // init buttons and rgb leds
    buttons_init();
    rgb_leds_init();

    // init pwm
    pwm_init_channel0();

    // set MIC PWR
    nrf_gpio_cfg_output(MIC_PWR_PIN_NUMBER);
    nrf_gpio_pin_write(MIC_PWR_PIN_NUMBER, 0);

    //set PWM pin
    nrf_gpio_cfg_output(DIM_LED_PWM);
    nrf_gpio_pin_write(DIM_LED_PWM, 0);

    // set UART pins
    nrf_gpio_cfg_input(RX_PIN_NUMBER, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_output(TX_PIN_NUMBER);
    nrf_gpio_pin_write(TX_PIN_NUMBER, 1);

    // set I2C pins
    nrf_gpio_cfg_input(SDA_GYRO_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
    nrf_gpio_cfg_input(SCL_GYRO_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);

    // set STAT/INT1 and button
    // nrf_gpio_cfg_input(STAT_PIN_NUMBER, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(INT1_GYRO_PIN_NUMBER, NRF_GPIO_PIN_PULLDOWN);
    // nrf_gpio_cfg_input(USER_BUTTON_PIN, NRF_GPIO_PIN_PULLUP);


    // assign pull up by defualt to not connected points
    uint8_t not_connected_pins[] = NOT_CONNECTED_PIN_NUMBERS;
    for (int i = 0; i < NOT_CONNECTED_PINS_LENGTH; i++)
    {
        uint32_t pin_number = not_connected_pins[i];
        NRF_GPIO_Type *reg = nrf_gpio_pin_port_decode(&pin_number);
        uint32_t cnf = reg->PIN_CNF[pin_number] & ~GPIO_PIN_CNF_PULL_Msk;

        reg->PIN_CNF[pin_number] = cnf | ((uint32_t)NRF_GPIO_PIN_PULLUP << GPIO_PIN_CNF_PULL_Pos);
    }
}

static void power_management_init()
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

static void timers_init()
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

int main(void)
{

    log_init();
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_INFO("===============================");
    NRF_LOG_INFO("SunFibre Lighting Unit starting!!!");

    board_init();
    power_management_init();

    timers_init();
    flash_init();

    NRF_LOG_INFO("===============================");


    businesslogic_init();

    for (;;)
    { 
        check_setups();
        idle_state_handle();
    }

}