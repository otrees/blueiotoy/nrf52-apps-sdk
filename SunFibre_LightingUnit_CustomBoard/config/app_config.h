#ifndef APP_CONFIG_H__
#define APP_CONFIG_H__

#include <stdint.h>
#include "boards.h"
#include "board_rgbleds.h"

#include "app_state.h"

#include "nrf_drv_pwm.h"

#define MODEL_NUMBER                            "2.1.0 / 1" // type: power optimization added

// DRAINED BATTERY SYSTEM OFF
#define BATTERY_DRAINED_SYS_OFF                 0

// BACKUP CHARACTERISTIC VALUES
#define BLE_CHAR_VALUES_BACKUP                      1
#define BLE_CHAR_VALUES_BACKUP_TIMER_INTERVAL       APP_TIMER_TICKS(10000)


// BUTTON CONFIG
#define USER_BUTTON_PIN            	            BUTTON_0                                /**< Button that will trigger the notification event with the LED Button Service */
#define BUTTON_DETECTION_DELAY_MS               45                     /**< Delay from a until a button is reported as pushed */
#define BUTTON_SHORT_PRESS_MS          	        1000                   /**< Interval  50-1000 ms when button is reported as SHORT PRESS */
#define BUTTON_LONG_PRESS_MS          	        3500                   /**< Interval  1000-3500 ms when button is reported as LONG PRESS */
#define BUTTON_VERY_LONG_PRESS_MS               8000                  /**< Interval   3500-8000 ms when button is reported as VERY_LONG_PRESS . */
#define BUTTON_VERY_VERY_LONG_PRESS_MS          20000                  /**< Interval  8000-15000 ms when button is reported as VERY_LONG_PRESS . */



// #define BUTTON_DETECTION_DELAY                          APP_TIMER_TICKS(30)                     /**< Delay from a until a button is reported as pushed */
// #define BUTTON_SHORT_PRESS          	                APP_TIMER_TICKS(1000)                   /**< Interval  50-1250 ms when button is reported as SHORT PRESS */
// #define BUTTON_LONG_PRESS          	                APP_TIMER_TICKS(4000)                   /**< Interval  1250-4000 ms when button is reported as LONG PRESS */
// #define BUTTON_VERY_LONG_PRESS                          APP_TIMER_TICKS(10000)                   /**< Interval  4000-10000 ms when button is reported as VERY_LONG_PRESS . */
// #define BUTTON_VERY_LONG_PRESS                          APP_TIMER_TICKS(20000)                   /**< Interval  10000-20000 ms when button is reported as VERY_VERY_LONG_PRESS . */
// // #define BUTTON_STUCK		                        APP_TIMER_TICKS(10000)                  /**< Interval  when nothing is reported */
// #define BUTTON_DOUBLE_CLICK_DELAY                       APP_TIMER_TICKS(750)                     /**< Delay to recognize a double click */

// STATE TIMERS
#define POST_CONNECTED_MAX_DURATION                APP_TIMER_TICKS(10000)

#define NOT_CONNECTED_MAX_DURATION                 APP_TIMER_TICKS(20000)              // go to IDLE after X s of no new action
#define NOT_CONNECTED_ADVER_MAX_DURATION           APP_TIMER_TICKS(40000)              // go to IDLE after X s of no new connection
#define CONNECTED_ADVER_MAX_DURATION               APP_TIMER_TICKS(10000)              // go to BLE_ACTIVE_CONNECTED after X seconds if no new connection
#define BONDING_ADVER_MAX_DURATION                 APP_TIMER_TICKS(15000)                   // go back to NOT_CONNECTED after X s of unsuccessful bonding

// TODO: maybe better to avoid these 
#define NOT_CONNECTED_CONNECTED_MAX_DURATION       POST_CONNECTED_MAX_DURATION                // go to IDLE after X s of no new connection
#define CONNECTED_CONNECTED_MAX_DURATION           POST_CONNECTED_MAX_DURATION                // go to IDLE after X s of no new connection
#define BONDING_CONNECTED_MAX_DURATION             POST_CONNECTED_MAX_DURATION              // go back to NOT_CONNECTED after X s when device connected but no security process started


//LED CONFIG
#define INDICATION_RGB_LED_IDX                  0
#define INDICATION_RGB_LED 			            {LED_1_RGB_RED, LED_1_RGB_GREEN, LED_1_RGB_BLUE};
#define BATTERY_RGB_LED_IDX                     1
#define BATTERY_RGB_LED 		                {LED_2_RGB_RED, LED_2_RGB_GREEN, LED_2_RGB_BLUE};
#define DEBUG_RGB_LED_IDX                       2
#define DEBUG_RGB_LED 			                {LED_3_RGB_RED, LED_3_RGB_GREEN, LED_3_RGB_BLUE};

#define INDICATION_LED_BLINK_INTERVAL		    APP_TIMER_TICKS(1000)
#define BATTERY_LED_BLINK_INTERVAL		        APP_TIMER_TICKS(1000)
#define DEBUG_LED_BTN_PRESSED_BLINK_INTERVAL	APP_TIMER_TICKS(100)
#define DIM_LED_SLOW_BLINK_INTERVAL		        APP_TIMER_TICKS(1000)
#define DIM_LED_FAST_BLINK_INTERVAL		        APP_TIMER_TICKS(250)


#define INDICATION_LED_IDLE			                LED_OFF
#define INDICATION_LED_NOT_CONNECTED	        LED_RED
#define INDICATION_LED_NOT_CONNECTED_ADVER     LED_YELLOW
#define INDICATION_LED_CONNECTED	        LED_GREEN
#define INDICATION_LED_CONNECTED_ADVER	LED_GREEN
#define INDICATION_LED_BONDING_ADVER			    LED_BLUE
#define INDICATION_LED_BONDING_CONNECTED	        LED_BLUE

#define INDICATION_LED_COLORS(name) name[NUM_APP_STATES] = { \
                                                INDICATION_LED_IDLE, \
                                                INDICATION_LED_NOT_CONNECTED, \
                                                INDICATION_LED_NOT_CONNECTED_ADVER, \
                                                INDICATION_LED_CONNECTED, \
                                                INDICATION_LED_CONNECTED_ADVER, \
                                                INDICATION_LED_BONDING_ADVER, \
                                                INDICATION_LED_BONDING_CONNECTED, \
                                                } \


#define INDICATION_LED_ERROR_BONDING_MISS_KEY	    LED_AZURE      // BONDING: bonding info deleted on peripheral, old on central
#define INDICATION_LED_ERROR_BONDING_SEC_REQ	    LED_WHITE     // BONDING: no security request during bonding
#define INDICATION_LED_ERROR_BONDING_REPAIR	      LED_PURPLE    // CONNECTION: bonding info deleted on central, old on peripheral


#define BATTERY_LED_LEVEL_4			                LED_GREEN
#define BATTERY_LED_LEVEL_3			                LED_YELLOW
#define BATTERY_LED_LEVEL_2			                LED_PURPLE
#define BATTERY_LED_LEVEL_1                         LED_RED
#define BATTERY_LED_LEVEL_0                         LED_OFF
#define BATTERY_LED_COLORS(name) name[5] = { \
                                                BATTERY_LED_LEVEL_0, \
                                                BATTERY_LED_LEVEL_1, \
                                                BATTERY_LED_LEVEL_2, \
                                                BATTERY_LED_LEVEL_3, \
                                                BATTERY_LED_LEVEL_4, \
                                                } \


// BLE NOTIFICATIONS
#define BLE_MONITOR_SERVICE_ON_INIT_DELAY   APP_TIMER_TICKS(5000)
//TODO: CHANGE FOR PRODUCTION
#define BLE_MONITOR_SERVICE_INTERVAL        APP_TIMER_TICKS(10000)


// PWM CONFIG
// #define PWM_CH0_OUTPUT_PIN0                  (LED_2_RGB_RED | NRF_DRV_PWM_PIN_INVERTED)
#define PWM_CH0_OUTPUT_PIN0                     DIM_LED_PWM
#define PWM_CH0_OUTPUT_PIN1                     NRF_DRV_PWM_PIN_NOT_USED
#define PWM_CH0_OUTPUT_PIN2                     NRF_DRV_PWM_PIN_NOT_USED
#define PWM_CH0_OUTPUT_PIN3                     NRF_DRV_PWM_PIN_NOT_USED


#endif  /* APP_CONFIG_H__ */