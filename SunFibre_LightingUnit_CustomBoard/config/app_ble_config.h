#ifndef APP_BLE_CONFIG_H__
#define APP_BLE_CONFIG_H__

#define DEVICE_NAME                             "SunFibre"
#define MANUFACTURER_NAME                       "FEL CVUT"
#define MANUFACTURER_IDENTIFIER                 0x8888


#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_PM_OBSERVER_PRIO            4                                      /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ALLOW_REPAIRING             1
#define APP_CONN_CFG_TAG                1                                       /**< A tag identifying the SoftDevice BLE configuration. */

// BLE - CONNECTION PARAMETERS
// #define CONN_MIN_INTERVAL               MSEC_TO_UNITS(500, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.5 seconds). */
// #define CONN_MAX_INTERVAL               MSEC_TO_UNITS(1000, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define CONN_MIN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.5 seconds). */
#define CONN_MAX_INTERVAL               MSEC_TO_UNITS(250, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define CONN_SLAVE_LATENCY              0                                      /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(5000, UNIT_10_MS)         /**< Connection supervisory time-out (4 seconds). */


#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000)                   /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */


// BLE - ADVERTISING PARAMETERS
#define ADVER_APPEARANCE							0x04C0 // category: Generic Control Device

#define ADVER_MODE										BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE //NOTE: discoverable all the time and BR/EDR not suppported
#define ADVER_TYPE										BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED
#define ADVER_PRIMARY_PHY			  			BLE_GAP_PHY_1MBPS


#define ADVER_FAST_INTERVAL           MSEC_TO_UNITS(100, UNIT_0_625_MS)     /**< Fast advertising interval (in units of 0.625 ms). */
#define ADVER_SLOW_INTERVAL           MSEC_TO_UNITS(1000, UNIT_0_625_MS)		/**< Slow advertising interval (in units of 0.625 ms). */

#define ADVER_FAST_DURATION           MSEC_TO_UNITS(3000, UNIT_10_MS)     // 3000 ms                        	/**< The advertising duration of fast advertising in units of 10 milliseconds. */
#define ADVER_SLOW_DURATION           BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED       /**< The advertising duration of slow advertising in units of 10 milliseconds. */

#define ADVER_NUM_UUIDS		        		1
#define ADVER_MAX_NUM_UUIDS		    		1





// BLE - SECURITY PARAMETERS
#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16


#endif  /* APP_BLE_CONFIG_H__ */