#ifndef APP_SDK_CONFIG_H
#define APP_SDK_CONFIG_H

#define APP_PRODUCTION 0


// UNDEFINE DEBUG if APP PRODUCTION
#if defined(APP_PRODUCTION) && APP_PRODUCTION == 1
#undef DEBUG
#undef DEBUG_NRF
#define DEBUG 0
#define DEBUG_NRF 0
#endif



// LOGGING
#if defined(DEBUG) && DEBUG == 1
#define NRF_LOG_ENABLED 1
#define NRF_LOG_DEFAULT_LEVEL 4
#else
#define NRF_LOG_ENABLED 0
#endif

// BLE
// <e> NRF_SDH_BLE_ENABLED - nrf_sdh_ble - SoftDevice BLE event handler
#define NRF_SDH_BLE_ENABLED 1
// <i> The SoftDevice handler will configure the stack with these parameters when calling @ref nrf_sdh_ble_default_cfg_set.
// <i> Other libraries might depend on these values; keep them up-to-date even if you are not explicitely calling @ref nrf_sdh_ble_default_cfg_set.
//==========================================================
// <o> NRF_SDH_BLE_GAP_DATA_LENGTH   <27-251> 
#define NRF_SDH_BLE_GAP_DATA_LENGTH 27

// <o> NRF_SDH_BLE_GAP_EVENT_LENGTH - GAP event length. 
// <i> The time set aside for this connection on every connection interval in 1.25 ms units.
#define NRF_SDH_BLE_GAP_EVENT_LENGTH 6

// <o> NRF_SDH_BLE_GATT_MAX_MTU_SIZE - Static maximum MTU size. 
#define NRF_SDH_BLE_GATT_MAX_MTU_SIZE 23

// <o> NRF_SDH_BLE_GATTS_ATTR_TAB_SIZE - Attribute Table size in bytes. The size must be a multiple of 4. 
#define NRF_SDH_BLE_GATTS_ATTR_TAB_SIZE 1408

// <o> NRF_SDH_BLE_VS_UUID_COUNT - The number of vendor-specific UUIDs. 
#define NRF_SDH_BLE_VS_UUID_COUNT 4


// <q> NRF_SDH_BLE_SERVICE_CHANGED  - Include the Service Changed characteristic in the Attribute Table.
#define NRF_SDH_BLE_SERVICE_CHANGED 0
// <o> NRF_SDH_BLE_PERIPHERAL_LINK_COUNT - Maximum number of peripheral links. 
#define NRF_SDH_BLE_PERIPHERAL_LINK_COUNT 	5
// <o> NRF_SDH_BLE_CENTRAL_LINK_COUNT - Maximum number of central links. 
#define NRF_SDH_BLE_CENTRAL_LINK_COUNT 			0
// <o> NRF_SDH_BLE_TOTAL_LINK_COUNT - Total link count. 
#define NRF_SDH_BLE_TOTAL_LINK_COUNT 				5



// BLE Services 
#define BLE_DIS_ENABLED 1

// DFU
#define BLE_DFU_ENABLED 1
#define NRF_DFU_BLE_BUTTONLESS_SUPPORTS_BONDS 0




#endif