#ifndef APP_CONFIG_H__
#define APP_CONFIG_H__

#include "boards.h"

// BLE CONFIG
#define DEVICE_NAME                             "SunFibre-btn"                         
#define MANUFACTURER_NAME                       "FEL CVUT"
#define MANUFACTURER_IDENTIFIER                 0x8888                         
#define MODEL_NUMBER                            "m.01"

//LED MATIX CONFIG
#define LED_MATRIX_DFU_ACTIVE			            0b11111 
#define LED_MATRIX_DFU_STARTED			          0b10101 
#define LED_MATRIX_DFU_FINISHED			          0b00000 
#define LED_MATRIX_DFU_ERROR			          	0b10101 

// BUTTON CONFIG
#define USER_BUTTON_PIN            	        BUTTON_0_PIN            /**< Button that will trigger the notification event with the LED Button Service */
#define USER_BUTTON2_PIN            	      BUTTON_1_PIN            /**< Button that will trigger the notification event with the LED Button Service */

#define BUTTON_DETECTION_DELAY_MS               45                     /**< Delay from a until a button is reported as pushed */
#define BUTTON_SHORT_PRESS_MS          	        1000                   /**< Interval  50-1000 ms when button is reported as SHORT PRESS */  
#define BUTTON_LONG_PRESS_MS          	        3500                   /**< Interval  1000-3500 ms when button is reported as LONG PRESS */
#define BUTTON_VERY_LONG_PRESS_MS               8000                  /**< Interval   3500-8000 ms when button is reported as VERY_LONG_PRESS . */
#define BUTTON_VERY_VERY_LONG_PRESS_MS          20000                  /**< Interval  8000-15000 ms when button is reported as VERY_LONG_PRESS . */

#endif  /* APP_CONFIG_H__ */