#include <stdint.h>

#include "nrf_mbr.h"
#include "nrf_bootloader.h"
#include "nrf_bootloader_app_start.h"
#include "nrf_bootloader_dfu_timers.h"
#include "nrf_dfu.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "app_error.h"
#include "app_error_weak.h"
#include "nrf_bootloader_info.h"
#include "nrf_delay.h"
#include "nrf_power.h"

#include "app_config.h"
#include "app_button.h"
#include "board_buttons.h"
#include "board_ledmatrix_no_apptimer.h"
#include "utils.h"

static void on_error(void)
{
    LED_matrix_turn_on_cols(LED_MATRIX_DFU_ERROR);
    NRF_LOG_FINAL_FLUSH();
#ifdef NRF_DFU_DEBUG_VERSION
    NRF_BREAKPOINT_COND;
#endif
    nrf_delay_ms(1000);
    NVIC_SystemReset();
}


void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    NRF_LOG_ERROR("%s:%d", p_file_name, line_num);
    on_error();
}


void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    NRF_LOG_ERROR("Received a fault! id: 0x%08x, pc: 0x%08x, info: 0x%08x", id, pc, info);
    assert_info_t *p_info = (assert_info_t *) info;
    NRF_LOG_ERROR("ASSERTION FAILED at %s:%u", p_info->p_file_name, p_info->line_num);
    on_error();
}


void app_error_handler_bare(uint32_t error_code)
{
    NRF_LOG_ERROR("Received an error: 0x%08x!", error_code);
    on_error();
}

/**
 * @brief Function notifies certain events in DFU process.
 */
static void app_on_dfu_evt(nrf_dfu_evt_type_t evt_type)
{
    NRF_LOG_INFO("(BOOTLOADER): DFU event %d", evt_type);
    switch (evt_type)
    {
        case NRF_DFU_EVT_DFU_FAILED:
            NRF_LOG_INFO("(BOOTLOADER): DFU failed");
            LED_matrix_turn_on_cols(LED_MATRIX_DFU_ERROR);
        break;
        case NRF_DFU_EVT_DFU_ABORTED:
        case NRF_DFU_EVT_DFU_INITIALIZED:
            NRF_LOG_INFO("(BOOTLOADER): DFU started");
        break;
        case NRF_DFU_EVT_TRANSPORT_ACTIVATED:
        break;
        case NRF_DFU_EVT_DFU_STARTED:
            LED_matrix_turn_on_rows(LED_MATRIX_DFU_STARTED);
        break;
        case NRF_DFU_EVT_TRANSPORT_DEACTIVATED:
        case NRF_DFU_EVT_DFU_COMPLETED:
            NRF_LOG_INFO("(BOOTLOADER): DFU completed");
            LED_matrix_turn_on_rows(LED_MATRIX_DFU_FINISHED);
            NVIC_SystemReset();
        break;

        default: break;
    }
}

void app_on_btn_evt(app_btn_evt_t event, void* btn_context)
{
    NRF_LOG_INFO("(BOOTLOADER): BTN event: %s", app_btn_evt_to_str(event));
    switch (event)
    {
        case EVT_BTN_LONG_PRESS:
        case EVT_BTN_VERY_LONG_PRESS:
        case EVT_BTN_VERY_VERY_LONG_PRESS:
            // // Clear DFU mark in GPREGRET register.
            // nrf_power_gpregret_set(nrf_power_gpregret_get() & ~BOOTLOADER_DFU_START);
            // external reset should clear gpregret register
            // turn off LEDS
            LED_matrix_turn_on_rows(LED_MATRIX_DFU_FINISHED);
            nrf_delay_ms(1000);

            NVIC_SystemReset();
        break;
        default: break;
    }
}


uint32_t nrf_dfu_init_user()
{
    // init app buttons
    app_btn_init_t left_button_init;
    left_button_init.button_pin = USER_BUTTON_PIN;
    left_button_init.app_evt_handler = (app_btn_evt_handler_t) &app_on_btn_evt;
    app_button_init(&left_button_init);

    app_btn_init_t right_button_init;
    right_button_init.button_pin = USER_BUTTON2_PIN;
    right_button_init.app_evt_handler = (app_btn_evt_handler_t) &app_on_btn_evt;
    app_button_init(&right_button_init);

    NRF_LOG_INFO("(BOOTLOADER): Left/Right buttons initialized");
    return NRF_SUCCESS;
}


/**@brief Function for application main entry. */
int main(void)
{

    uint32_t ret_val;

    buttons_init();
    LED_matrix_init();

    // turn on all LEDS
    LED_matrix_turn_on_rows(LED_MATRIX_DFU_ACTIVE);


    // Must happen before flash protection is applied, since it edits a protected page.
    nrf_bootloader_mbr_addrs_populate();

    // Protect MBR and bootloader code from being overwritten.
    ret_val = nrf_bootloader_flash_protect(0, MBR_SIZE);
    APP_ERROR_CHECK(ret_val);
    ret_val = nrf_bootloader_flash_protect(BOOTLOADER_START_ADDR, BOOTLOADER_SIZE);
    APP_ERROR_CHECK(ret_val);

    (void) NRF_LOG_INIT(nrf_bootloader_dfu_timer_counter_get);
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_INFO("===============================");
    NRF_LOG_INFO("(BOOTLOADER): STARTED");

    ret_val = nrf_bootloader_init(app_on_dfu_evt);
    APP_ERROR_CHECK(ret_val);

    NRF_LOG_FLUSH();

    NRF_LOG_ERROR("After main, should never be reached.");
    NRF_LOG_FLUSH();

    APP_ERROR_CHECK_BOOL(false);
}