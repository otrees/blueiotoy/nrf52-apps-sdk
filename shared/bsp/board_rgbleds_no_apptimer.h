
#ifndef BOARD_RGBLEDS_NO_APPTIMER_H__
#define BOARD_RGBLEDS_NO_APPTIMER_H__

#include <stdint.h>


#define MAX_RGB_LEDS				5

// LED COLORS
#define LED_OFF			            0b000
#define LED_RED			            0b100
#define LED_GREEN			        0b010
#define LED_BLUE			        0b001
#define LED_YELLOW			        0b110
#define LED_PURPLE			        0b101
#define LED_AZURE			        0b011
#define LED_WHITE			        0b111


typedef struct
{
    uint8_t led_index;
    uint8_t levels;
} rgb_led_timer_context_t;

void rgb_led_blink_with_delay(uint8_t led_index, uint8_t levels, uint16_t blinks);

void rgb_led_toggle(uint8_t led_index, uint8_t levels);
void rgb_led_write(uint8_t led_index, uint8_t levels);

void rgb_leds_write(uint8_t levels);
void rgb_leds_off();

void rgb_leds_init();

#endif /* BOARD_RGBLEDS_NO_APPTIMER_H__ */