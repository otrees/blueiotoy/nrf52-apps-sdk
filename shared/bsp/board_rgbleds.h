
#ifndef BOARD_RGBLEDS_H__
#define BOARD_RGBLEDS_H__

#include <stdint.h>

#define MAX_RGB_LEDS				(5)

// LED COLORS
#define LED_OFF			            0b000
#define LED_RED			            0b100
#define LED_GREEN			        0b010
#define LED_BLUE			        0b001
#define LED_YELLOW			        0b110
#define LED_PURPLE			        0b101
#define LED_AZURE			        0b011
#define LED_WHITE			        0b111

#define BLINKING_REPEATED           (-1)


void rgb_led_blinking_start(uint8_t led_index, uint8_t levels, uint32_t refresh_rate, uint8_t count);
void rgb_led_blinking_stop(uint8_t led_index);
void rgb_led_blink_with_delay(uint8_t led_index, uint8_t levels, uint16_t blinks);


void rgb_led_toggle(uint8_t led_index, uint8_t levels);
void rgb_led_write(uint8_t led_index, uint8_t levels);


void rgb_leds_blinking_start(uint8_t levels, uint32_t refresh_rate);
void rgb_leds_blinking_stop();
void rgb_leds_write(uint8_t levels);
void rgb_leds_off();

void rgb_leds_init();

#endif /* BOARD_RGBLEDS_H__ */