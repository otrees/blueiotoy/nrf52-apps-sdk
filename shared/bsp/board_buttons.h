#ifndef BOARD_BUTTONS_H__
#define BOARD_BUTTONS_H__ 

#include <stdint.h>

#include "custom_board.h"

void button_wakeup_enable(uint8_t pin);

uint8_t button_pressed(uint8_t pin);

void buttons_init();

#endif /* BOARD_BUTTONS_H__ */