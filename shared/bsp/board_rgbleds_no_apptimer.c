#include "board_rgbleds.h"

#include <stdbool.h>

#include "nrf_delay.h"

#include "custom_board.h"

#define RGB_LED_IDX(led_idx, led) 	(led_idx * 3 + led)

static uint8_t rgb_leds_num = LEDS_NUMBER/3;
static uint8_t m_rgb_leds_pins[LEDS_NUMBER] = LEDS_LIST;


void rgb_led_blink_with_delay(uint8_t led_index, uint8_t levels, uint16_t blinks)
{
    for (int i = 0; i < blinks; i++)
    {
        rgb_led_write(led_index, levels);
        nrf_delay_ms(250);
        rgb_led_write(led_index, LED_OFF);
        nrf_delay_ms(250);
    }
}

static void write_pin(uint8_t pin, bool active)
{
    nrf_gpio_pin_write(pin, !(LEDS_ACTIVE_STATE ^ active));
}


//NOTE: toggle only active colors
void rgb_led_toggle(uint8_t led_index, uint8_t levels)
{
	for (int i = 0; i < 3; i++)
	{
		uint8_t active = (levels & (1 << i)) != 0;
    if (active) nrf_gpio_pin_toggle(m_rgb_leds_pins[RGB_LED_IDX(led_index, 2-i)]);
	}
}
 

void rgb_led_write(uint8_t led_index, uint8_t levels)
{
	// NOTE: order of writing B -> G -> R (reversed)
	for (int i = 0; i < 3; i++)
	{
		uint8_t active = (levels & (1 << i)) != 0;
		write_pin(m_rgb_leds_pins[RGB_LED_IDX(led_index, 2-i)], active);
	}
}

void rgb_leds_write(uint8_t levels)
{
	for (int i = 0; i < rgb_leds_num; i++)
		rgb_led_write(i, levels);
}


void rgb_leds_off()
{
	rgb_leds_write(LED_OFF);
}


void rgb_leds_init()
{
	for (int i = 0; i < LEDS_NUMBER; i++)
    	nrf_gpio_cfg_output(m_rgb_leds_pins[i]);

	rgb_leds_off();
		
}