#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#include "nrf_gpio.h"
#include "nrfx_saadc.h"


// BOARD WITH RFID MODULE OR NOT
#define BOARD_WITH_RFID     1

// TYPE OF RFID - RDM6300 x custom LC oscilator 
#ifdef BOARD_WITH_RFID
   #define RFID_AS_RDM6300  0
   #define RFID_AS_RLC      1
#endif

// TYPE OF LC oscilator -  1 (first version) x 2 (second version)
#if defined(RFID_AS_RLC) && RFID_AS_RLC == 1
   #define PROTOTYPE_VERSION 2
#endif



#define LED_1_RGB_RED    26//4
#define LED_1_RGB_GREEN  1//3
#define LED_1_RGB_BLUE   0//2

#define LED_2_RGB_RED    8//10
#define LED_2_RGB_GREEN  6//8
#define LED_2_RGB_BLUE   4//6

#define LED_3_RGB_RED    12//14
#define LED_3_RGB_GREEN  13//22
#define LED_3_RGB_BLUE   (32+9)//P1.09//12


#define LEDS_ACTIVE_STATE 0



#if !BOARD_WITH_RFID || !RFID_AS_RLC
#define LEDS_NUMBER      9
#define LEDS_LIST { \
   LED_1_RGB_RED, LED_1_RGB_GREEN, LED_1_RGB_BLUE, \
   LED_2_RGB_RED, LED_2_RGB_GREEN, LED_2_RGB_BLUE, \
   LED_3_RGB_RED, LED_3_RGB_GREEN, LED_3_RGB_BLUE  \
   }
#else 
#define LEDS_NUMBER      6
#define LEDS_LIST { \
   LED_1_RGB_RED, LED_1_RGB_GREEN, LED_1_RGB_BLUE, \
   LED_3_RGB_RED, LED_3_RGB_GREEN, LED_3_RGB_BLUE  \
   }

#endif


// add missing BSP board led defines
#define BSP_BOARD_LED_8          8

// BUTTONS 
#define BUTTONS_NUMBER            1
#define BUTTON_0                 25//11
#define BUTTONS_LIST             { BUTTON_0 }

#define BUTTONS_ACTIVE_STATE      0
#define BUTTON_PULL              NRF_GPIO_PIN_PULLUP

// UART
#define RX_PIN_NUMBER            24//36
#define TX_PIN_NUMBER            32//34
#define CTS_PIN_NUMBER           UART_PIN_DISCONNECTED
#define RTS_PIN_NUMBER           UART_PIN_DISCONNECTED

// DIM LED PWM
#define DIM_LED_PWM              (32+2)//P1.02//41

// CHARGER
#define STAT_PIN_NUMBER          15 //24

// MIC
#define MIC_PWR_PIN_NUMBER       (32+4) 


// ACC/Gyro I2C
#define SDA_GYRO_PIN_NUMBER             (20)
#define SCL_GYRO_PIN_NUMBER             (22)
#define INT1_GYRO_PIN_NUMBER            (17) //DEFAULT: forced to ground

// RFID
#if BOARD_WITH_RFID && RFID_AS_RLC
   #define RFID_PWM_PIN           8                     //P.08 
#endif 


//ADC 
   // ADC CHANNELS
#define ADC_BATTERY_CHANNEL			   NRF_SAADC_INPUT_AIN0 // pin 2  (65)
#define ADC_VLED_CHANNEL			      NRF_SAADC_INPUT_AIN5 // pin 29 (67)
#define ADC_ISNS_CHANNEL			      NRF_SAADC_INPUT_AIN7 // pin 31 (69)
#define ADC_VDD_INTERNAL_CHANNEL		   NRF_SAADC_INPUT_VDD
// #define ADC_MIC_CHANNEL			         NRF_SAADC_INPUT_AIN7 // pin 31 (69)
#if BOARD_WITH_RFID && RFID_AS_RLC
   #define ADC_RFID_CHANNEL            NRF_SAADC_INPUT_AIN2    //P.04
#endif

   // ADC CHANNEL IDXs
#define ADC_BATTERY_CHANNEL_IDX		   0	
#define ADC_VLED_CHANNEL_IDX			   1
#define ADC_ISNS_CHANNEL_IDX			   2
#define ADC_VDD_INTERNAL_CHANNEL_IDX   3	
#if BOARD_WITH_RFID && RFID_AS_RLC
   #define ADC_RFID_CHANNEL_IDX			4
#endif

   // ADC CHANNEL GAINS
#define ADC_BATTERY_CHANNEL_GAIN        NRF_SAADC_GAIN1_3     //1.8 V max
#define ADC_VLED_CHANNEL_GAIN           NRF_SAADC_GAIN1_6     //3.6 V max
#define ADC_ISNS_CHANNEL_GAIN           NRF_SAADC_GAIN1       //600 mV max
// #define ADC_VDD_INTERNAL_CHANNEL_GAIN   NRF_SAADC_GAIN1_6
#if BOARD_WITH_RFID && RFID_AS_RLC
   #if PROTOTYPE_VERSION == 1
      #define ADC_RFID_CHANNEL_GAIN       NRF_SAADC_GAIN1_2		   //1.2 V max
   #elif PROTOTYPE_VERSION == 2
      #define ADC_RFID_CHANNEL_GAIN       NRF_SAADC_GAIN1_4		   //2.4 V max
   #endif
#endif

   // ADC CHANNEL GAINS

#define ADC_BATTERY_CHANNEL_ACQ        NRF_SAADC_ACQTIME_3US
#define ADC_VLED_CHANNEL_ACQ           NRF_SAADC_ACQTIME_3US
#define ADC_ISNS_CHANNEL_ACQ           NRF_SAADC_ACQTIME_3US
// #define ADC_VDD_INTERNAL_CHANNEL_ACQ   NRF_SAADC_ACQTIME_5US
#if BOARD_WITH_RFID && RFID_AS_RLC
   #define ADC_RFID_CHANNEL_ACQ       	NRF_SAADC_ACQTIME_5US
#endif 


#define VLED_CONVERT_MV(mV)            ((mV * (10000 + 3300)) / 3300)        // voltage drop on R22        
#define ISNS_CONVERT_MA(mV)             (mV * 1000 / 1500)                    // direct measurment       
#define BATTERY_CONVERT_MV(mV)         (mV *(4700 + 10000) / 4700)           // voltage drop on R18       




// NOT CONNECTED
// #define NOT_CONNECTED_PINS_LENGTH   20
#define NOT_CONNECTED_PINS_LENGTH   19
#define NOT_CONNECTED_PIN_NUMBERS {                 \
   27, 5, 7, (32+8), 11, 14, 16, 21,         \
   30, 28, 3, 19, (32+3), (32+5), 10, 9, (32+7), (32+6), (32+1) \
}
// missing (32+12)
#define RESET_PIN_NUMBER            18

#endif