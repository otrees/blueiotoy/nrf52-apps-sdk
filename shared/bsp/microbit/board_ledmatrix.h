#ifndef BOARD_LEDMATRIX_H__
#define BOARD_LEDMATRIX_H__ 

#include <stdint.h>
#include "app_timer.h"

#include "custom_board.h"

#define LED_MATRIX_REFRESH_RATE                 APP_TIMER_TICKS(5)                    //200 hz flicking


// extern const uint8_t YES[LED_MATRIX_ROWS];
// extern const uint8_t NO[LED_MATRIX_ROWS];

extern const uint8_t LED_MATRIX_I[LED_MATRIX_ROWS];
extern const uint8_t LED_MATRIX_S[LED_MATRIX_ROWS];
extern const uint8_t LED_MATRIX_C[LED_MATRIX_ROWS];
extern const uint8_t LED_MATRIX_E[LED_MATRIX_ROWS];

extern const uint8_t LED_MATRIX_P[LED_MATRIX_ROWS];

// extern const uint8_t LED_MATRIX_A[LED_MATRIX_ROWS];
// extern const uint8_t LED_MATRIX_N[LED_MATRIX_ROWS];
// extern const uint8_t LED_MATRIX_B[LED_MATRIX_ROWS];


// functions
void LED_matrix_draw_pixel(uint8_t c, uint8_t r, bool active);

void LED_matrix_show(const uint8_t bitmap[LED_MATRIX_ROWS]);

void LED_matrix_clear();

void LED_matrix_init();

#endif /* BOARD_LEDMATRIX_H__ */