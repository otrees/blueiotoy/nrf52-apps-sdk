#ifndef BOARD_LEDMATRIX_NO_APPTIMER_H__
#define BOARD_LEDMATRIX_NO_APPTIMER_H__ 

#include <stdint.h>
#include "custom_board.h"

void LED_matrix_turn_on_cols(uint8_t cols_bitmap);

void LED_matrix_turn_on_rows(uint8_t rows_bitmap);

void LED_matrix_init();

#endif /* BOARD_LEDMATRIX_H__ */