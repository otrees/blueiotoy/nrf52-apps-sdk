
#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#include <stdint.h>

#include "nrf_gpio.h"

// LEDs
#define LED_MATRIX_ROWS 		5
#define LED_MATRIX_COLS 		5
#define LEDS_NUMBER              (LED_MATRIX_ROWS * LED_MATRIX_COLS)

// pin numbers for the rows on the microbit LED matrix
#define LED_MATRIX_ROWPINS  { 21, 22, 15, 24, 19 }; 

// pin numbers for the columns on the microbit LED matrix
#define LED_MATRIX_COLPINS  { 28, 11, 31, (32+5), 30 }; 

// defines what row each pixel is in
#define PIX2ROW {            \
    1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3,  \
    3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5      \
};

// defines what column each pixel is in
#define PIX2COL {            \
    1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3,  \
    4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5,     \
};

#define LEDS_ROWS_ACTIVE_STATE 1
#define LEDS_COLS_ACTIVE_STATE 0


// BUTTONS 
#define BUTTONS_NUMBER            2
#define BUTTONS_ACTIVE_STATE      0
#define BUTTON_0_PIN              14
#define BUTTON_1_PIN              23
#define BUTTONS_LIST             { BUTTON_0_PIN, BUTTON_1_PIN }
#define BUTTON_PULL              NRF_GPIO_PIN_PULLUP


// UART - RX/TX
#define RX_PIN_NUMBER            (32+8) 
#define TX_PIN_NUMBER            6 
#define CTS_PIN_NUMBER           UART_PIN_DISCONNECTED
#define RTS_PIN_NUMBER           UART_PIN_DISCONNECTED



//ADC 
// #define ADC_VDD_INTERNAL_CHANNEL		   NRF_SAADC_INPUT_VDD
// #define ADC_BATTERY_CHANNEL			   NRF_SAADC_INPUT_AIN0 // pin 2  (65)
// #define ADC_VLED_CHANNEL			      NRF_SAADC_INPUT_AIN5 // pin 29 (67)
// #define ADC_ISNS_CHANNEL			      NRF_SAADC_INPUT_AIN7 // pin 31 (69)

// #define ADC_VDD_INTERNAL_CHANNEL_IDX   0	
// #define ADC_BATTERY_CHANNEL_IDX		   1	
// #define ADC_VLED_CHANNEL_IDX			   2 
// #define ADC_ISNS_CHANNEL_IDX			   3 

// #define ADC_VLED_MILLIVOLTS_RATIO      ((10 + 3.3) / 3.3)   // voltage drop on R22        
// #define ADC_ISNS_MILLIVOLTS_RATIO      1                    // direct measurment       
// #define ADC_BATTERY_MILLIVOLTS_RATIO   ((4.7 + 10) / 4.7)   // voltage drop on R18       

#define STAT_PIN_NUMBER          15 //24

#endif