#include "board_ledmatrix_no_apptimer.h"


static const uint8_t m_colpins[LED_MATRIX_COLS] = LED_MATRIX_COLPINS;
static const uint8_t m_rowpins[LED_MATRIX_ROWS] = LED_MATRIX_ROWPINS;

static void write_rowpin(uint8_t rowpin, bool active)
{
    nrf_gpio_pin_write(rowpin, !(LEDS_ROWS_ACTIVE_STATE ^ active));
}

static void write_colpin(uint8_t colpin, bool active)
{
    nrf_gpio_pin_write(colpin, !(LEDS_COLS_ACTIVE_STATE ^ active));
}


void LED_matrix_turn_on_rows(uint8_t rows_bitmap)
{
	for (uint8_t c = 0; c < LED_MATRIX_COLS; c++)
		write_colpin(m_colpins[c], true);

	for (uint8_t r = 0; r < LED_MATRIX_ROWS; r++)
		write_rowpin(m_rowpins[LED_MATRIX_ROWS - r -1], (rows_bitmap & 1 << r) != 0);
}

void LED_matrix_turn_on_cols(uint8_t cols_bitmap)
{
	for (uint8_t r = 0; r < LED_MATRIX_ROWS; r++)
		write_rowpin(m_rowpins[r], true);

	for (uint8_t c = 0; c < LED_MATRIX_COLS; c++)
		write_colpin(m_colpins[LED_MATRIX_COLS - c -1], (cols_bitmap & 1 << c) != 0);
}


void LED_matrix_init() 
{
    for (uint8_t c = 0; c < LED_MATRIX_COLS; c++)
        nrf_gpio_cfg_output(m_colpins[c]);

    for (uint8_t r = 0; r < LED_MATRIX_ROWS; r++)
        nrf_gpio_cfg_output(m_rowpins[r]);
}