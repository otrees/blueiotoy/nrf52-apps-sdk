#include <stdint.h>
#include <stdbool.h>

#include "nrf_log.h"

#include "app_timer.h"

#include "custom_board.h"
#include "app_config.h"



APP_TIMER_DEF(m_led_matrix_timer_id);
// APP_TIMER_DEF(m_led_matrix_blink_timer_id);

static uint8_t LED_matrix_buffer[LED_MATRIX_COLS][LED_MATRIX_ROWS];
static uint8_t refreshed_row = 0; 

static const uint8_t m_colpins[LED_MATRIX_COLS] = LED_MATRIX_COLPINS;
static const uint8_t m_rowpins[LED_MATRIX_ROWS] = LED_MATRIX_ROWPINS;


const uint8_t LED_MATRIX_A[5] = {   //AAA
    0b01110,
    0b10001,
    0b11111,
    0b10001,
    0b10001,
};

const uint8_t LED_MATRIX_N[5] = {   //NNN
    0b10001,
    0b11001,
    0b10101,
    0b10011,
    0b10001,
};

const uint8_t LED_MATRIX_C[5] = {   //CCC
    0b01110,
    0b10001,
    0b10000,
    0b10001,
    0b01110,
};

const uint8_t LED_MATRIX_B[5] = {   //BBB
    0b01110,
    0b01001,
    0b01110,
    0b01001,
    0b01110,
};

const uint8_t LED_MATRIX_I[5] = {   //III
    0b01110,
    0b00100,
    0b00100,
    0b00100,
    0b01110,
};

const uint8_t LED_MATRIX_E[5] = {   //EEE
    0b11111,
    0b10000,
    0b11110,
    0b10000,
    0b11111,
};

const uint8_t LED_MATRIX_P[5] = {   //EEE
    0b11110,
    0b10010,
    0b11110,
    0b10000,
    0b10000,
};

// const uint8_t F[5] = {
//     0b11111,
//     0b10000,
//     0b11100,
//     0b10000,
//     0b10000,
// };

// const uint8_t K[5] = {
//     0b10001,
//     0b10010,
//     0b11100,
//     0b10010,
//     0b10001,
// };

const uint8_t LED_MATRIX_S[5] = {
    0b01110,
    0b01000,
    0b00100,
    0b00010,
    0b01110,
};

// const uint8_t M[5] = {
//     0b10001,
//     0b11011,
//     0b10101,
//     0b10001,
//     0b10001,
// };

// const uint8_t R[5] = {
//     0b11110,
//     0b10001,
//     0b11110,
//     0b10010,
//     0b10001,
// };
// const uint8_t D[5] = {
//     0b11110,
//     0b10001,
//     0b10001,
//     0b10001,
//     0b11110,
// };

// const uint8_t NO[5] = {
//     0b10001,
//     0b01010,
//     0b00100,
//     0b01010,
//     0b10001,
// };

// const uint8_t YES[5] = {
//     0b00000,
//     0b00001,
//     0b00010,
//     0b10100,
//     0b01000,
// };

static void write_rowpin(uint8_t rowpin, bool active)
{
    nrf_gpio_pin_write(rowpin, !(LEDS_ROWS_ACTIVE_STATE ^ active));
}

static void write_colpin(uint8_t colpin, bool active)
{
    nrf_gpio_pin_write(colpin, !(LEDS_COLS_ACTIVE_STATE ^ active));
}

static uint8_t row_to_rowpin(uint8_t row)
{
    return m_rowpins[row];
}

static uint8_t col_to_colpin(uint8_t col)
{
    return m_colpins[col];
}


static void on_timer_evt()
{
    // disable current row
    write_rowpin(row_to_rowpin(refreshed_row), 0);

    for (uint8_t x = 0; x < LED_MATRIX_COLS; x++)
        write_colpin(col_to_colpin(x), 0);

    // go to next row
    refreshed_row = (++refreshed_row < LED_MATRIX_ROWS)? refreshed_row : 0;

    // enable new row
    write_rowpin(row_to_rowpin(refreshed_row), 1);

    for (uint8_t x = 0; x < LED_MATRIX_COLS; x++)
        write_colpin(col_to_colpin(x), LED_matrix_buffer[x][refreshed_row]);

}


static void draw_bitmap(const uint8_t bitmap[LED_MATRIX_ROWS])
{
    for (uint8_t y = 0; y < LED_MATRIX_ROWS; y++){

        for (uint8_t x = 0; x < LED_MATRIX_COLS; x++)
            LED_matrix_buffer[LED_MATRIX_COLS-x-1][y] = (bitmap[y] & 1 << x) != 0;
    }
}


void LED_matrix_clear()
{
    memset(LED_matrix_buffer, 0, sizeof(uint8_t) * LEDS_NUMBER);
}

/**
    0b01110,
    0b00100,
    0b00100,
    0b00100,
    0b01110,
 *    @brief  Draw a single pixel/LED on the 5x5 matrix
 *    @param  x 0 to 4 column
 *    @param  y 0 to 4 row
 *    @param  active 1 for LED on, 0 for off
 */
void LED_matrix_draw_pixel(uint8_t x, uint8_t y, bool active)
{
    ASSERT(0 <= x ||  x < LED_MATRIX_COLS || 0 <= y || y < LED_MATRIX_ROWS);

    LED_matrix_clear();
    LED_matrix_buffer[x][y] = active;
}



void LED_matrix_show(const uint8_t bitmap[LED_MATRIX_ROWS])
{
  LED_matrix_clear();
  draw_bitmap(bitmap);
}

void LED_matrix_init() 
{
    for (uint8_t c = 0; c < LED_MATRIX_COLS; c++)
        nrf_gpio_cfg_output(m_colpins[c]);

    for (uint8_t r = 0; r < LED_MATRIX_ROWS; r++)
        nrf_gpio_cfg_output(m_rowpins[r]);

    // init LED matrix buffer
    LED_matrix_clear();

    // initialize LED matrix timer
    app_timer_create(&m_led_matrix_timer_id, APP_TIMER_MODE_REPEATED, (app_timer_timeout_handler_t) on_timer_evt);
    app_timer_start(m_led_matrix_timer_id, LED_MATRIX_REFRESH_RATE, NULL);
}





