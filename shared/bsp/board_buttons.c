#include <stdint.h>
#include <stdbool.h>

#include "nrf_log.h"

#include "custom_board.h"

static const uint8_t m_buttons[BUTTONS_NUMBER] = BUTTONS_LIST;

uint8_t button_pressed(uint8_t pin)
{
    return !(nrf_gpio_pin_read(pin) ^ BUTTONS_ACTIVE_STATE);
}

void button_wakeup_enable(uint8_t pin)
{
    // set as input - might be deinitialized previously
    nrf_gpio_cfg_input(pin, BUTTON_PULL);
    nrf_gpio_pin_sense_t sense = BUTTONS_ACTIVE_STATE ? NRF_GPIO_PIN_SENSE_HIGH : NRF_GPIO_PIN_SENSE_LOW;
    nrf_gpio_cfg_sense_set(pin, sense);
}


void buttons_init()
{
    for (uint8_t i = 0; i < BUTTONS_NUMBER; i++)
    {
        nrf_gpio_cfg_input(m_buttons[i], BUTTON_PULL);
    }
}








