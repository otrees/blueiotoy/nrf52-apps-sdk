#include "board_rgbleds.h"

#include <stdbool.h>

#include "nrf_delay.h"
#include "app_timer.h"

#include "custom_board.h"


// NOTE: defined here because of app_timer_t inside struct
typedef struct 
{
	uint8_t led_index;
	uint8_t levels;

	app_timer_t timer;
	uint32_t counter;
	uint32_t num_of_counts;
	uint32_t refresh_rate;

} rgb_led_timer_wrap_t;

#define RGB_LED_IDX(led_idx, led) 	(led_idx * 3 + led)

static uint8_t m_rgb_leds_pins[LEDS_NUMBER] = LEDS_LIST;
static uint8_t rgb_leds_num = LEDS_NUMBER/3;
static rgb_led_timer_wrap_t m_rgb_leds_timers[LEDS_NUMBER/3] = {0};

static bool is_led_index_valid(uint8_t led_index)
{
	return (0 <= led_index && led_index < LEDS_NUMBER/3);
}

static void on_timer_rgb_led_toggle_evt(void *p_ctx)
{
	rgb_led_timer_wrap_t *timer_ctx = (rgb_led_timer_wrap_t*) p_ctx;
	rgb_led_toggle(timer_ctx->led_index, timer_ctx->levels);

}

static void on_timer_rgb_led_counted_toggle_evt(void *p_ctx)
{
	rgb_led_timer_wrap_t *timer_ctx = (rgb_led_timer_wrap_t*) p_ctx;
	rgb_led_toggle(timer_ctx->led_index, timer_ctx->levels);

	// schedule next toggle
	if (timer_ctx->counter++ <= timer_ctx->num_of_counts)
	{
		app_timer_start(&(timer_ctx->timer), timer_ctx->refresh_rate, (void*) timer_ctx);
	}
}



void rgb_led_blinking_start(uint8_t led_index, uint8_t levels, uint32_t refresh_rate, uint8_t count)
{
	if (!is_led_index_valid(led_index)) return;
	// stop timer if running
	rgb_led_blinking_stop(led_index);

	// assing context parameters
	rgb_led_timer_wrap_t *timer_ctx = &(m_rgb_leds_timers[led_index]);
	timer_ctx->led_index = led_index;
	timer_ctx->levels = levels;
	timer_ctx->refresh_rate = refresh_rate;

	// create and start timer 
	app_timer_id_t timer_id = &(timer_ctx->timer);

	if (count == BLINKING_REPEATED)
	{
    app_timer_create(&timer_id, APP_TIMER_MODE_REPEATED, (app_timer_timeout_handler_t) on_timer_rgb_led_toggle_evt);
		timer_ctx->num_of_counts = BLINKING_REPEATED;
	}
	else
	{
    app_timer_create(&timer_id, APP_TIMER_MODE_SINGLE_SHOT, (app_timer_timeout_handler_t) on_timer_rgb_led_counted_toggle_evt);
		timer_ctx->counter = 0;
		timer_ctx->num_of_counts = count;
	}

	app_timer_start(timer_id, refresh_rate, (void*) timer_ctx);
}

void rgb_led_blinking_stop(uint8_t led_index)
{
	if (!is_led_index_valid(led_index)) return;

	rgb_led_timer_wrap_t *timer_ctx = &(m_rgb_leds_timers[led_index]);
	app_timer_stop(&(timer_ctx->timer));
}


void rgb_led_blink_with_delay(uint8_t led_index, uint8_t levels, uint16_t blinks)
{
	if (!is_led_index_valid(led_index)) return;

	// stop timer if running
	rgb_led_blinking_stop(led_index);

	for (int i = 0; i < blinks; i++)
	{
			rgb_led_write(led_index, levels);
			nrf_delay_ms(250);
			rgb_led_write(led_index, LED_OFF);
			nrf_delay_ms(250);
	}
}

static void write_pin(uint8_t pin, bool active)
{
    nrf_gpio_pin_write(pin, !(LEDS_ACTIVE_STATE ^ active));
}



//NOTE: toggle only active colors
void rgb_led_toggle(uint8_t led_index, uint8_t levels)
{
	if (!is_led_index_valid(led_index)) return;

	for (int i = 0; i < 3; i++)
	{
		uint8_t active = (levels & (1 << i)) != 0;
    if (active) nrf_gpio_pin_toggle(m_rgb_leds_pins[RGB_LED_IDX(led_index, 2-i)]);
	}
}
 

void rgb_led_write(uint8_t led_index, uint8_t levels)
{

	if (!is_led_index_valid(led_index)) return;

	// NOTE: order of writing B -> G -> R (reversed)
	for (int i = 0; i < 3; i++)
	{
		uint8_t active = (levels & (1 << i)) != 0;
		write_pin(m_rgb_leds_pins[RGB_LED_IDX(led_index, 2-i)], active);
	}
}

void rgb_leds_blinking_start(uint8_t levels, uint32_t refresh_rate)
{
	for (int i = 0; i < rgb_leds_num; i++)
		rgb_led_blinking_start(i, levels, refresh_rate, BLINKING_REPEATED);
}

void rgb_leds_blinking_stop()
{
	for (int i = 0; i < rgb_leds_num; i++)
		rgb_led_blinking_stop(i);
}

void rgb_leds_write(uint8_t levels)
{
	for (int i = 0; i < rgb_leds_num; i++)
		rgb_led_write(i, levels);
}


void rgb_leds_off()
{
	rgb_leds_write(LED_OFF);
}

// static void timers_create()
// {
// 	for (int i = 0; i < rgb_leds_num; i++)
// 	{

// 		app_timer_id_t repeated_timer_id = &(m_rgb_leds[i].repated_timer);
//     app_timer_create(&repeated_timer_id, APP_TIMER_MODE_REPEATED, (app_timer_timeout_handler_t) on_timer_rgb_led_toggle_evt);

// 		app_timer_id_t counted_timer_id = &(m_rgb_leds[i].counted_timer);
//     app_timer_create(&counted_timer_id, APP_TIMER_MODE_SINGLE_SHOT, (app_timer_timeout_handler_t) on_timer_rgb_led_toggle_evt);
//   }                                                                                       
// }



void rgb_leds_init()
{
	for (int i = 0; i < LEDS_NUMBER; i++)
    nrf_gpio_cfg_output(m_rgb_leds_pins[i]);

	rgb_leds_off();

	// timers_create();
		
}