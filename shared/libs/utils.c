#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

#include "nrf_log.h"
#include "ble_ranges.h"


/* #region  SDK BLE EVTS STR  */
const static char* pm_evts_str[] = {
    "PM_EVT_BONDED_PEER_CONNECTED",           /**< @brief A connected peer has been identified as one with which we have a bond. When performing bonding with a peer for the first time, this event will not be sent until a new connection is established with the peer. When we are central, this event is always sent when the Peer Manager receives the @ref BLE_GAP_EVT_CONNECTED event. When we are peripheral, this event might in rare cases arrive later. */
    "PM_EVT_CONN_SEC_START",                  /**< @brief A security procedure has started on a link, initiated either locally or remotely. The security procedure is using the last parameters provided via @ref pm_sec_params_set. This event is always followed by either a @ref PM_EVT_CONN_SEC_SUCCEEDED or a @ref PM_EVT_CONN_SEC_FAILED event. This is an informational event; no action is needed for the procedure to proceed. */
    "PM_EVT_CONN_SEC_SUCCEEDED",              /**< @brief A link has been encrypted, either as a result of a call to @ref pm_conn_secure or a result of an action by the peer. The event structure contains more information about the circumstances. This event might contain a peer ID with the value @ref PM_PEER_ID_INVALID, which means that the peer (central) used an address that could not be identified, but it used an encryption key (LTK) that is present in the database. */
    "PM_EVT_CONN_SEC_FAILED",                 /**< @brief A pairing or encryption procedure has failed. In some cases, this means that security is not possible on this link (temporarily or permanently). How to handle this error depends on the application. */
    "PM_EVT_CONN_SEC_CONFIG_REQ",             /**< @brief The peer (central) has requested pairing, but a bond already exists with that peer. Reply by calling @ref pm_conn_sec_config_reply before the event handler returns. If no reply is sent, a default is used. */
    "PM_EVT_CONN_SEC_PARAMS_REQ",             /**< @brief Security parameters (@ref ble_gap_sec_params_t) are needed for an ongoing security procedure. Reply with @ref pm_conn_sec_params_reply before the event handler returns. If no reply is sent, the parameters given in @ref pm_sec_params_set are used. If a peripheral connection, the central's sec_params will be available in the event. */
    "PM_EVT_STORAGE_FULL",                    /**< @brief There is no more room for peer data in flash storage. To solve this problem, delete data that is not needed anymore and run a garbage collection procedure in FDS. */
    "PM_EVT_ERROR_UNEXPECTED",                /**< @brief An unrecoverable error happened inside Peer Manager. An operation failed with the provided error. */
    "PM_EVT_PEER_DATA_UPDATE_SUCCEEDED",      /**< @brief A piece of peer data was stored, updated, or cleared in flash storage. This event is sent for all successful changes to peer data, also those initiated internally in Peer Manager. To identify an operation, compare the store token in the event with the store token received during the initiating function call. Events from internally initiated changes might have invalid store tokens. */
    "PM_EVT_PEER_DATA_UPDATE_FAILED",         /**< @brief A piece of peer data could not be stored, updated, or cleared in flash storage. This event is sent instead of @ref PM_EVT_PEER_DATA_UPDATE_SUCCEEDED for the failed operation. */
    "PM_EVT_PEER_DELETE_SUCCEEDED",           /**< @brief A peer was cleared from flash storage, for example because a call to @ref pm_peer_delete succeeded. This event can also be sent as part of a call to @ref pm_peers_delete or internal cleanup. */
    "PM_EVT_PEER_DELETE_FAILED",              /**< @brief A peer could not be cleared from flash storage. This event is sent instead of @ref PM_EVT_PEER_DELETE_SUCCEEDED for the failed operation. */
    "PM_EVT_PEERS_DELETE_SUCCEEDED",          /**< @brief A call to @ref pm_peers_delete has completed successfully. Flash storage now contains no peer data. */
    "PM_EVT_PEERS_DELETE_FAILED",             /**< @brief A call to @ref pm_peers_delete has failed, which means that at least one of the peers could not be deleted. Other peers might have been deleted, or might still be queued to be deleted. No more @ref PM_EVT_PEERS_DELETE_SUCCEEDED or @ref PM_EVT_PEERS_DELETE_FAILED events are sent until the next time @ref pm_peers_delete is called. */
    "PM_EVT_LOCAL_DB_CACHE_APPLIED",          /**< @brief Local database values for a peer (taken from flash storage) have been provided to the SoftDevice. */
    "PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED",     /**< @brief Local database values for a peer (taken from flash storage) were rejected by the SoftDevice, which means that either the database has changed or the user has manually set the local database to an invalid value (using @ref pm_peer_data_store). */
    "PM_EVT_SERVICE_CHANGED_IND_SENT",        /**< @brief A service changed indication has been sent to a peer, as a result of a call to @ref pm_local_database_has_changed. This event will be followed by a @ref PM_EVT_SERVICE_CHANGED_IND_CONFIRMED event if the peer acknowledges the indication. */
    "PM_EVT_SERVICE_CHANGED_IND_CONFIRMED",   /**< @brief A service changed indication that was sent has been confirmed by a peer. The peer can now be considered aware that the local database has changed. */
    "PM_EVT_SLAVE_SECURITY_REQ",              /**< @brief The peer (peripheral) has requested link encryption, which has been enabled. */
    "PM_EVT_FLASH_GARBAGE_COLLECTED",         /**< @brief The flash has been garbage collected (By FDS), possibly freeing up space. */
    "PM_EVT_FLASH_GARBAGE_COLLECTION_FAILED", /**< @brief Garbage collection was attempted but failed. */
};

const static char* gap_evts_str[] = {
  "BLE_GAP_EVT_CONNECTED",		/**< Connected to peer.                              \n See @ref ble_gap_evt_connected_t             */
  "BLE_GAP_EVT_DISCONNECTED", 		/**< Disconnected from peer.                         \n See @ref ble_gap_evt_disconnected_t.         */
  "BLE_GAP_EVT_CONN_PARAM_UPDATE",   	/**< Connection Parameters updated.                  \n See @ref ble_gap_evt_conn_param_update_t.    */
  "BLE_GAP_EVT_SEC_PARAMS_REQUEST", 	/**< Request to provide security parameters.         \n Reply with @ref sd_ble_gap_sec_params_reply.  \n See @ref ble_gap_evt_sec_params_request_t. */
  "BLE_GAP_EVT_SEC_INFO_REQUEST",	/**< Request to provide security information.        \n Reply with @ref sd_ble_gap_sec_info_reply.    \n See @ref ble_gap_evt_sec_info_request_t.   */
  "BLE_GAP_EVT_PASSKEY_DISPLAY",	/**< Request to display a passkey to the user.       \n In LESC Numeric Comparison, reply with @ref sd_ble_gap_auth_key_reply. \n See @ref ble_gap_evt_passkey_display_t. */
  "BLE_GAP_EVT_KEY_PRESSED",		/**< Notification of a keypress on the remote device.\n See @ref ble_gap_evt_key_pressed_t           */
  "BLE_GAP_EVT_AUTH_KEY_REQUEST",	/**< Request to provide an authentication key.       \n Reply with @ref sd_ble_gap_auth_key_reply.    \n See @ref ble_gap_evt_auth_key_request_t.   */
  "BLE_GAP_EVT_LESC_DHKEY_REQUEST",	/**< Request to calculate an LE Secure Connections DHKey. \n Reply with @ref sd_ble_gap_lesc_dhkey_reply.  \n See @ref ble_gap_evt_lesc_dhkey_request_t */
  "BLE_GAP_EVT_AUTH_STATUS",		/**< Authentication procedure completed with status. \n See @ref ble_gap_evt_auth_status_t.          */
  "BLE_GAP_EVT_CONN_SEC_UPDATE",	/**< Connection security updated.                    \n See @ref ble_gap_evt_conn_sec_update_t.      */
  "BLE_GAP_EVT_TIMEOUT",		/**< Timeout expired.                                \n See @ref ble_gap_evt_timeout_t.              */
  "BLE_GAP_EVT_RSSI_CHANGED",		/**< RSSI report.                                    \n See @ref ble_gap_evt_rssi_changed_t.         */
  "BLE_GAP_EVT_ADV_REPORT",		/**< Advertising report.                             \n See @ref ble_gap_evt_adv_report_t.           */
  "BLE_GAP_EVT_SEC_REQUEST",		/**< Security Request.                               \n Reply with @ref sd_ble_gap_authenticate	     */
  "BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST",	/**< Connection Parameter Update Request.            \n Reply with @ref sd_ble_gap_conn_param_update. \n See @ref ble_gap_evt_conn_param_update_request_t. */
  "BLE_GAP_EVT_SCAN_REQ_REPORT",	/**< Scan request report.                            \n See @ref ble_gap_evt_scan_req_report_t. */
  "BLE_GAP_EVT_PHY_UPDATE_REQUEST",	/**< PHY Update Request.                             \n Reply with @ref sd_ble_gap_phy_update. \n See @ref ble_gap_evt_phy_update_request_t. */
  "BLE_GAP_EVT_PHY_UPDATE",		/**< PHY Update Procedure is complete.               \n See @ref ble_gap_evt_phy_update_t.           */
  "BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST",	/**< Data Length Update Request.                     \n Reply with @ref sd_ble_gap_data_length_update. \n See @ref ble_gap_evt_data_length_update_request_t. */
  "BLE_GAP_EVT_DATA_LENGTH_UPDATE",	/**< LL Data Channel PDU payload length updated.     \n See @ref ble_gap_evt_data_length_update_t. */
  "BLE_GAP_EVT_QOS_CHANNEL_SURVEY_REPORT",	/**< Channel survey report.                          \n See @ref ble_gap_evt_qos_channel_survey_report_t. */
  "BLE_GAP_EVT_ADV_SET_TERMINATED",	/**< Advertising set terminated.                     \n See @ref ble_gap_evt_adv_set_terminated_t. */
};

const static char* discovery_evts_str[] = {
    "BLE_DB_DISCOVERY_COMPLETE",      /**< Event indicating that the discovery of one service is complete. */
    "BLE_DB_DISCOVERY_ERROR",         /**< Event indicating that an internal error has occurred in the DB Discovery module. This could typically be because of the SoftDevice API returning an error code during the DB discover.*/
    "BLE_DB_DISCOVERY_SRV_NOT_FOUND", /**< Event indicating that the service was not found at the peer.*/
    "BLE_DB_DISCOVERY_AVAILABLE"      /**< Event indicating that the DB discovery instance is available.*/
};
/* #endregion  SDK BLE EVTS STR  */



/* #region  APP EVTS STR  */
static const char *app_btn_evt_str[] = {
    "EVT_BTN_SHORT_PRESS",
    "EVT_BTN_LONG_PRESS",
    "EVT_BTN_VERY_LONG_PRESS",
    "EVT_BTN_VERY_VERY_LONG_PRESS",
    "EVT_BTN_DOUBLE_PRESS",
    "EVT_BTN_PRESS_START",
    "EVT_BTN_PRESSINTERVAL_PASSED",
};

static const char *app_ble_evt_str[] = {
    "EVT_BLE_CONNECTED",
    "EVT_BLE_CONNECTED_AND_BONDED",
    "EVT_BLE_DISCONNECTED",
    "EVT_BLE_BONDING_SUCCESSFUL",
    "EVT_BLE_SEC_ERROR",
    "EVT_BLE_BONDING_REMOVED",
};

static const char *app_battery_evt_str[] = {
    "EVT_BATTERY_CHARGER_CONNECTED",
    "EVT_BATTERY_CHARGER_DISCONNECTED",
    "EVT_BATTERY_MESURED",
};

static const char *app_rfid_evt_str[] = {
    "EVT_RFID_DETECTED",
};

/* #endregion  APP EVTS STR  */

uint32_t binArray2int(uint8_t *bin_array, int length)
{
    uint32_t value = 0;
    for (int i = 0; i < length; i++)
    {
        value = value << 1;
        value += bin_array[i];
    }
    return value;
}

/**
 * Function: hex2int
 * Take a hex string and convert it to a 32bit number (max 8 hex digits)
 */
uint32_t hex2int(uint8_t *hex_str, int length) {
    // if (length > 8) return -1; //NOT SUPPORTED

    uint32_t val = 0;
    for (int i = 0; i < length; i ++){
        // get current character then increment
        unsigned char byte = *hex_str++; 
        // transform hex character to the 4bit equivalent number, using the ascii table indexes
        if (byte >= '0' && byte <= '9') byte = byte - '0';
        else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
        else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;    
        // shift 4 to make space for new digit, and add the 4 bits of the new digit 
        val = (val << 4) | (byte & 0xF);
    }
    return val;
}

void hex_array_to_str(uint8_t *hex_array, uint8_t *str, int length)
{
    for (int i = 0; i < length; i++)
    {
        uint8_t val = hex_array[i];
        if (val < 10) str[i] = val + '0'; 
        if (val >= 10) str[i] = val + 'A' - 10; 
    }
    str[length] = 0;
}



void print_hex_array(uint8_t *array, uint32_t len)
{
    char *str = malloc((5 *len + 1) * sizeof(char));
    char *p_str = str;
    if (str == NULL) return;

    for (int i = 0; i < len; i++){
        str += sprintf(str, "0x%02X ", array[i]);
    }
    str[len] = '\0';
    NRF_LOG_INFO("Hex dump(%d): %s", len, p_str);
    free(p_str);
}


// EVTS as string printing

const char* pm_evt_id_to_str(int evt)
{
    return pm_evts_str[evt];
}

const char* gap_evt_id_to_str(int evt)
{
    if (evt >= BLE_GAP_EVT_BASE && evt <= BLE_GAP_EVT_BASE + 22 )
        return gap_evts_str[evt - BLE_GAP_EVT_BASE];
    else
        return "";
}

const char* discovery_evt_id_to_str(int evt)
{
    return discovery_evts_str[evt];
}

const char *app_btn_evt_to_str(int evt)
{
    return app_btn_evt_str[evt];
}

const char *app_ble_evt_to_str(int evt)
{
    return app_ble_evt_str[evt];
}

const char *app_battery_evt_to_str(int evt)
{
    return app_battery_evt_str[evt];
}

const char *app_rfid_evt_to_str(int evt)
{
    return app_rfid_evt_str[evt];
}
