#include "app_button.h"

#include <stdlib.h>

#include "nrf_drv_gpiote.h"
#include "nrf_timer.h"
#include "nrfx_timer.h"
#include "nrf_log.h"

#include "board_buttons.h"
#include "app_config.h"

#define TIMER_MS_TO_TICKS(ms)  nrfx_timer_ms_to_ticks(&m_timer1, ms)
#define TIMER_TICKS_TO_MS(ticks) ((ticks) * 1000 / 31250)

// FUNCTION DECLARATIONS
static void timer_schedule_next_btn_click_check(uint8_t app_button_state_idx);
static void timer_schedule_next_btn_pressedinterval_event(uint8_t app_button_state_idx, uint32_t timer_ticks);


uint32_t BUTTON_EVENTS_LOOKUP_MS(btn_events_lookup);

static bool m_timer_inited = false;
static nrfx_timer_t m_timer1 = NRFX_TIMER_INSTANCE(1);

static uint8_t app_buttons_count = 0;
static app_button_state_t app_button_states[MAX_NUM_BUTTONS];

// static bool button_double_click_detecting = false;
// static int short_press_counter = 0;

// CONFIGS
static nrfx_timer_config_t timer_cfg = 
{
    .bit_width = NRF_TIMER_BIT_WIDTH_32,           
    .frequency = NRF_TIMER_FREQ_31250Hz,          
    .interrupt_priority = APP_IRQ_PRIORITY_LOW,
    .mode = NRF_TIMER_MODE_TIMER
};


static app_button_state_t *_find_app_button_state(uint8_t pin)
{
    for (int i = 0; i < app_buttons_count; i++)
    {
        if (app_button_states[i].pin == pin) return &(app_button_states[i]);
    }
    return NULL;
}

static uint8_t get_timer_next_event(uint32_t diff_ms)
{
    for (int i = 0; i < NUM_BTN_PRESS_EVENTS; i++)
    {
        uint32_t event_ms = btn_events_lookup[i];
        if (diff_ms < event_ms)
            return i;
    }
    return -1;
}

static void call_app_evt_handler(app_button_state_t *app_button_state, app_btn_evt_t event)
{

    app_btn_evt_handler_t app_evt_handler = app_button_state->app_evt_handler;

    if (!app_evt_handler) return;

    app_btn_evt_context_t *app_btn_evt_context = (app_btn_evt_context_t *) malloc(sizeof(app_btn_evt_context_t));
    app_btn_evt_context->button_pin = app_button_state->pin;
    app_btn_evt_context->button_index = app_button_state->index;

    app_evt_handler(event, app_btn_evt_context);

    free(app_btn_evt_context);
}

static void on_button_state_change(app_button_state_t *app_button_state)
{
    uint32_t current_count = nrfx_timer_capture(&m_timer1, NRF_TIMER_CC_CHANNEL0);


    if (app_button_state->state == BUTTON_RELEASED)
    {
        // disable timer
        nrfx_timer_disable(&m_timer1);

        uint32_t diff_ms = TIMER_TICKS_TO_MS(current_count - app_button_state->_timer_count);
        NRF_LOG_DEBUG("(BTN-%d): RELEASED: %d", app_button_state->pin, diff_ms);

        uint8_t event = get_timer_next_event(diff_ms);
        if (event != -1)
            call_app_evt_handler(app_button_state, event);
        else
            NRF_LOG_ERROR("Button pressed for more than %d ms! Might be stucked!", BUTTON_VERY_VERY_LONG_PRESS_MS);
    }
    
    else if (app_button_state->state == BUTTON_PRESSED)
    {
        // NRF_LOG_DEBUG("BUTTON PRESSED");
        app_button_state->_timer_count = current_count;

        timer_schedule_next_btn_pressedinterval_event(app_button_state->index, current_count + TIMER_MS_TO_TICKS(btn_events_lookup[0]));

        call_app_evt_handler(app_button_state, EVT_BTN_PRESS_START);
    }
}

/* #region  TIMER SCHEDULE  */
static void timer_schedule_next_btn_pressedinterval_event(uint8_t app_button_state_idx, uint32_t timer_ticks)
{
    nrf_timer_cc_channel_t channel = 0;
    switch (app_button_state_idx)
    {
        case 0: channel = NRF_TIMER_CC_CHANNEL3; break;
        case 1: channel = NRF_TIMER_CC_CHANNEL4; break;
        default: NRFX_ASSERT(false);
    }
    nrfx_timer_compare(&m_timer1, channel, timer_ticks, true);
}

static void timer_schedule_next_btn_click_check(uint8_t app_button_state_idx)
{

    uint32_t current_count = nrfx_timer_capture(&m_timer1, NRF_TIMER_CC_CHANNEL0);
    uint32_t time_ticks = nrfx_timer_ms_to_ticks(&m_timer1, BUTTON_DETECTION_DELAY_MS/3);

    nrf_timer_cc_channel_t channel = 0;
    switch (app_button_state_idx)
    {
        case 0: channel = NRF_TIMER_CC_CHANNEL1; break;
        case 1: channel = NRF_TIMER_CC_CHANNEL2; break;
        default: NRFX_ASSERT(false);
    }
    nrfx_timer_compare(&m_timer1, channel, current_count + time_ticks, true);
}

static void _on_timer_btn_pressedinterval_evt(uint8_t app_btn_state_idx)
{
    app_button_state_t *app_button_state = &(app_button_states[app_btn_state_idx]);

    uint32_t current_count = nrfx_timer_capture(&m_timer1, NRF_TIMER_CC_CHANNEL0);
    uint32_t diff_ms = TIMER_TICKS_TO_MS(current_count - app_button_state->_timer_count);
    uint8_t next_event = get_timer_next_event(diff_ms);

    ASSERT(next_event != -1);

    if (next_event != NUM_BTN_PRESS_EVENTS && app_button_state->state == BUTTON_PRESSED)
    {
        uint32_t next_event_ms = btn_events_lookup[next_event];
        uint32_t next_event_ticks =  app_button_state->_timer_count + TIMER_MS_TO_TICKS(next_event_ms);
        timer_schedule_next_btn_pressedinterval_event(app_btn_state_idx, next_event_ticks);

        call_app_evt_handler(app_button_state, EVT_BTN_PRESSINTERVAL_PASSED);
    }
}

static void _on_timer_btn_click_check_evt(uint8_t app_btn_state_idx)
{
    app_button_state_t *app_button_state = &(app_button_states[app_btn_state_idx]);
    button_state_t button_active = button_pressed(app_button_state->pin);

    // false alarm
    if (button_active == app_button_state->state){
        // enable button
        nrfx_gpiote_in_event_enable(app_button_state->pin, true);

        // disable timer
        nrfx_timer_disable(&m_timer1);
        return;
    }
    // SCAN COMPLETED
    if (++app_button_state->_scan_counter == 3)
    {
        // change button state
        app_button_state->state = button_active;
        // enable button
        nrfx_gpiote_in_event_enable(app_button_state->pin, true);
        on_button_state_change(app_button_state);
    }
    else
        timer_schedule_next_btn_click_check(app_button_state->index);
}

static void on_timer_evt(nrf_timer_event_t event_type, void* p_context)
{
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE1:
            _on_timer_btn_click_check_evt(0);
        break;

        case NRF_TIMER_EVENT_COMPARE2:
            _on_timer_btn_click_check_evt(1);
        break;

        case NRF_TIMER_EVENT_COMPARE3:
            _on_timer_btn_pressedinterval_evt(0);
        break;

        case NRF_TIMER_EVENT_COMPARE4:
            _on_timer_btn_pressedinterval_evt(1);
        break;
    
        default: NRFX_ASSERT(false);
    }
}
/* #endregion  TIMER SCHEDULE  */

static void on_button_change_evt(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t button_action)
{
    app_button_state_t *app_button_state = _find_app_button_state((uint8_t) pin);
    // NRF_LOG_DEBUG("(BTN-%d): cb", pin);

    // assert
    NRFX_ASSERT(app_button_state != NULL);
    NRFX_ASSERT(button_action == NRF_GPIOTE_POLARITY_TOGGLE);

    // disable interrupt
    nrfx_gpiote_in_event_disable(pin);
    button_state_t button_active = button_pressed(pin);
    //TODO: this assert is sometimes false, why ??? (are we missing any changes)
    // assert - missed change
    // NRFX_ASSERT(button_active != app_button_state->state);

    app_button_state->_scan_counter = 0;

    // schedule scan
    timer_schedule_next_btn_click_check(app_button_state->index);

    // enable timer (might be enabled from past)
    if (!nrfx_timer_is_enabled(&m_timer1))
        nrfx_timer_enable(&m_timer1);
}


static void gpiote_init(uint8_t button_pin)
{
    ret_code_t err_code;

    // check gpiote
    if (!nrfx_gpiote_is_init())
    {
        err_code = nrfx_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }

    // setup config 
    // !!! High accuracy must be enabled !!!
    nrfx_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    config.pull = BUTTON_PULL;
    err_code = nrfx_gpiote_in_init(button_pin, &config, on_button_change_evt);
    APP_ERROR_CHECK(err_code);

    // enable button 
    nrfx_gpiote_in_event_enable(button_pin, true);
}


static void timer_deinit()
{
    if (!m_timer_inited) return;

    nrfx_timer_disable(&m_timer1);
    nrfx_timer_uninit(&m_timer1);

    m_timer_inited = false;
}

static void timer_init()
{
    if (m_timer_inited) return;

    ret_code_t err_code = nrfx_timer_init(&m_timer1, &timer_cfg, on_timer_evt);
    APP_ERROR_CHECK(err_code);    
    m_timer_inited = true;
}


// void app_button_set_app_evt_handler(app_btn_evt_handler_t app_btn_evt_handler)
// {
//     app_evt_handler = app_btn_evt_handler;
// }

void app_buttons_deinit()
{

    timer_deinit();

    for (int i = 0; i < app_buttons_count; i++)
    {
        uint8_t button_pin = app_button_states[i].pin;
        nrfx_gpiote_in_uninit(button_pin);

    }

}

void app_button_init(app_btn_init_t *init)
{
    // check number of buttons
    if (app_buttons_count >= MAX_NUM_BUTTONS) return;

    uint8_t button_pin = init->button_pin;

    // get state
    button_state_t button_active = button_pressed(button_pin);

    // init struct 
    app_button_state_t *btn_state = &(app_button_states[app_buttons_count]);
    btn_state->pin = init->button_pin;
    btn_state->state = button_active;
    btn_state->index = app_buttons_count;
    btn_state->_scan_counter = 0;
    btn_state->_timer_count = 0;
    btn_state->app_evt_handler = init->app_evt_handler;

    app_buttons_count++;

    NRF_LOG_WARNING("(BTN-%d): %d at the initialization (read %d)", button_pin, button_active, nrf_gpio_pin_read(button_pin));

    // init gpiote
    gpiote_init(button_pin);

    //create timers
    timer_init();
}
