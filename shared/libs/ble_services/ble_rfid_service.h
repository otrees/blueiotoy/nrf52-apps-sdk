#ifndef RFID_SERVICE_H__
#define RFID_SERVICE_H__

#include <stdint.h>

#include "nrf_sdh_ble.h"
#include "ble.h"
#include "ble_srv_common.h"

#define RS_UUID_BASE     {{0x25, 0xD3, 0x15, 0xF1, 0x61, 0x7A, 0x25, 0x17, 0xE0, 0xF1, 0x14, 0x15, 0x00, 0x00, 0x00, 0x00}}

#define RS_UUID_SERVICE                   0xAAAA
#define RS_UUID_DETECTED_TAG_CHAR         0xBBBB
#define RS_UUID_PAIRED_TAG_CHAR           0xCCCC
#define RS_UUID_ENABLED_CHAR              0xDDDD

#define RS_SIZE_DETECTED_TAG_CHAR          4 
#define RS_SIZE_PAIRED_TAG_CHAR            4 
#define RS_SIZE_ENABLED_CHAR               1 

#define RS_INITIAL_DETECTED_TAG_CHAR         -1 
#define RS_INITIAL_PAIRED_TAG_CHAR           -1 
#define RS_INITIAL_ENABLED_CHAR               1 



struct ble_rs_s;

typedef void (*ble_rfid_paired_tag_write_handler_t) (struct ble_rs_s *p_rfid, uint32_t tag_id);
typedef void (*ble_rfid_enabled_write_handler_t) (struct ble_rs_s *p_rfid, uint8_t enabled);


typedef struct
{
    ble_rfid_paired_tag_write_handler_t paired_tag_write_handler;
    ble_rfid_enabled_write_handler_t enabled_write_handler;

} ble_rs_init_t;

typedef struct ble_rs_s
{
    // HANDLE OF THE SERVICE
    uint16_t                    service_handle;
    uint8_t                     uuid_type;
    ble_gatts_char_handles_t    detected_tag_handles;
    ble_gatts_char_handles_t    paired_tag_handles;
    ble_gatts_char_handles_t    enabled_handles;

    // WRITE HANDLERS
    ble_rfid_paired_tag_write_handler_t paired_tag_write_handler;
    ble_rfid_enabled_write_handler_t enabled_write_handler;
} ble_rs_t;


#define BLE_RS_DEF(_name)                                                                \
static ble_rs_t _name;                                                                   \
NRF_SDH_BLE_OBSERVER(_name ## _obs, BLE_RS_OBSERVER_PRIO, rfid_service_on_ble_evt, &_name)


void rfid_service_detected_tag_char_notify(ble_rs_t *p_rfid, uint32_t tag_id, uint16_t conn_handle);
void rfid_service_detected_tag_char_update(ble_rs_t * p_rfid, uint32_t tag_id);

/** Function for handling BLE Stack events related to service and its characteristic.*/
void rfid_service_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);

void rfid_service_get_adv_data(ble_rs_t *p_rfid, ble_uuid_t *uuid);

/** Function for initializing service.*/
void rfid_service_init(ble_rs_t *p_rfid, ble_rs_init_t *p_rfid_init);

#endif  /*  RFID_SERVICE_H__ */