#include "ble_led_control_service.h"

#include <string.h>

#include "nrf_log.h"
#include "app_error.h"

#include "utils.h"
#include "ble_service_utils.h"


/* #region CHAR DEFINITIONS */
static ret_code_t debug_led_char_add(ble_lcs_t *p_led_control)
{
    ret_code_t err_code;
    ble_uuid_t char_uuid = {.type = BLE_UUID_TYPE_VENDOR_BEGIN, .uuid = LCS_UUID_DEBUG_LED_CHAR};

    ble_uuid128_t base_uuid = LCS_UUID_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    APP_ERROR_CHECK(err_code);

    // Characteristic metadata
    //  - add R/W permission (guideline)
    ble_gatts_char_md_t char_md;
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    
    // Attribute metadata 
    // NOTE: where in memory to store the attributes
    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&attr_md.write_perm);
    attr_md.vloc = BLE_GATTS_VLOC_STACK; //store in softdevice


    // Characteristic value attribute
    // CHARACTERISTIC VALUE ATTRIBUTE (UUID + ATTRIBUTE METADATA + VALUE)
    uint8_t initial_value = LCS_INITIAL_DEBUG_LED_CHAR;
    uint8_t char_len = LCS_SIZE_DEBUG_LED_CHAR;

    ble_gatts_attr_t attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;

    attr_char_value.max_len = char_len;
    attr_char_value.init_len = char_len;
    attr_char_value.p_value = &initial_value;


    // Add our new characteristic to the service
    err_code = sd_ble_gatts_characteristic_add(p_led_control->service_handle,
                                   &char_md,
                                   &attr_char_value,
                                   &p_led_control->debug_led_handles);
    APP_ERROR_CHECK(err_code);

    return NRF_SUCCESS;
}


//TODO: do we want to also preserve the light mode??
static uint8_t get_dim_led_initial_value()
{
    return LCS_INITIAL_DIM_LED_CHAR;
}

static ret_code_t dim_led_char_add(ble_lcs_t *p_led_control)
{
    ret_code_t err_code;
    ble_uuid_t char_uuid = {.type = BLE_UUID_TYPE_VENDOR_BEGIN, .uuid = LCS_UUID_DIM_LED_CHAR};

    ble_uuid128_t base_uuid = LCS_UUID_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    APP_ERROR_CHECK(err_code);

    ble_gatts_char_md_t char_md;
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    char_md.char_props.notify = 1;

    // Client Characteristic Configuration Descriptor metadata 
    ble_gatts_attr_md_t cccd_md;
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&cccd_md.write_perm);
    cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    

    char_md.p_cccd_md           = &cccd_md;

    // User Descriptor metadata 
    ble_gatts_attr_md_t user_md;
    memset(&user_md, 0, sizeof(user_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&user_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&user_md.write_perm);
    user_md.vloc                = BLE_GATTS_VLOC_STACK;    

    uint8_t desc[] = "DIM LED Characteristic";
    char_md.p_user_desc_md = &user_md;
    char_md.p_char_user_desc = desc;
    char_md.char_user_desc_max_size = sizeof(desc);
    char_md.char_user_desc_size = sizeof(desc);

    // Characteristic metadata
    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&attr_md.write_perm);
    attr_md.vloc = BLE_GATTS_VLOC_STACK; //store in softdevice

    // Characteristic value 
    uint8_t char_len = LCS_SIZE_DIM_LED_CHAR;
    uint8_t initial_value = get_dim_led_initial_value();
    ble_gatts_attr_t attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.max_len = char_len;
    attr_char_value.init_len = char_len;
    attr_char_value.p_value = &initial_value;

    err_code = sd_ble_gatts_characteristic_add(p_led_control->service_handle,
                                   &char_md,
                                   &attr_char_value,
                                   &p_led_control->dim_led_handles);
    APP_ERROR_CHECK(err_code);

    return NRF_SUCCESS;
}

static ret_code_t vled_char_add(ble_lcs_t *p_led_control)
{
    ble_add_char_params_t  add_char_params;
    uint16_t initial_value = LCS_INITIAL_VLED_CHAR;

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = LCS_UUID_VLED_CHAR;
    add_char_params.max_len           = LCS_SIZE_VLED_CHAR;
    add_char_params.init_len          = LCS_SIZE_VLED_CHAR;
    add_char_params.p_init_value      = (uint8_t *) &initial_value;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_led_control->service_handle, &add_char_params, &(p_led_control->vled_handles));
}

static ret_code_t isns_char_add(ble_lcs_t *p_led_control)
{
    ble_add_char_params_t  add_char_params;
    uint16_t initial_value = LCS_INITIAL_ISNS_CHAR;

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = LCS_UUID_ISNS_CHAR;
    add_char_params.max_len           = LCS_SIZE_ISNS_CHAR;
    add_char_params.init_len          = LCS_SIZE_ISNS_CHAR;
    add_char_params.p_init_value      = (uint8_t*) &initial_value;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_led_control->service_handle, &add_char_params, &(p_led_control->isns_handles));
}

/* #endregion CHAR DEFINITIONS */

/* #region CHAR - UPDATE & NOTIFY*/
void led_control_service_dim_led_char_update(ble_lcs_t *p_led_control, uint8_t light_mode)
{
    uint16_t handle = p_led_control->dim_led_handles.value_handle;
    ble_set_char_value(handle, LCS_SIZE_DIM_LED_CHAR, &light_mode, "LCS");
}

void led_control_service_dim_led_char_notify(ble_lcs_t *p_led_control, uint8_t light_mode, uint16_t conn_handle)
{
    uint16_t handle = p_led_control->dim_led_handles.value_handle;
    ble_send_notification(handle, LCS_SIZE_DIM_LED_CHAR, &light_mode, conn_handle, "LCS");
}

void led_control_service_vled_char_update(ble_lcs_t *p_led_control, uint16_t vled_mV)
{
    uint16_t handle = p_led_control->vled_handles.value_handle;
    ble_set_char_value(handle, LCS_SIZE_VLED_CHAR, (uint8_t*) &vled_mV, "LCS");
}

void led_control_service_vled_char_notify(ble_lcs_t *p_led_control, uint16_t vled_mV, uint16_t conn_handle)
{
    uint16_t handle = p_led_control->vled_handles.value_handle;
    ble_send_notification(handle, LCS_SIZE_VLED_CHAR, (uint8_t*) &vled_mV, conn_handle, "LCS");
}

void led_control_service_isns_char_update(ble_lcs_t *p_led_control, uint16_t isns_mA)
{
    uint16_t handle = p_led_control->isns_handles.value_handle;
    ble_set_char_value(handle, LCS_SIZE_ISNS_CHAR, (uint8_t*) &isns_mA, "LCS");
}

void led_control_service_isns_char_notify(ble_lcs_t *p_led_control, uint16_t isns_mA, uint16_t conn_handle)
{
    uint16_t handle = p_led_control->isns_handles.value_handle;
    ble_send_notification(handle, LCS_SIZE_ISNS_CHAR, (uint8_t*) &isns_mA, conn_handle, "LCS");
}
/* #endregion */

static void print_write_req(ble_gatts_evt_write_t *p_evt_write)
{
    NRF_LOG_INFO("(LCS): GATTS_EVT_WRITE:");
    print_hex_array(p_evt_write->data, p_evt_write->len);
}

void led_control_service_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context)
{
  	ble_lcs_t *p_led_control =(ble_lcs_t *) p_context;  

    switch (p_ble_evt->header.evt_id)
    { 
        case BLE_GATTS_EVT_WRITE: {
            ble_gatts_evt_write_t *p_evt_write = (ble_gatts_evt_write_t*) &p_ble_evt->evt.gatts_evt.params.write;
            
            if (p_evt_write->handle == p_led_control->debug_led_handles.value_handle){
                if (p_evt_write->len == LCS_SIZE_DEBUG_LED_CHAR && p_led_control->debug_led_write_handler != NULL)
                {
                    print_write_req(p_evt_write);
                    p_led_control->debug_led_write_handler(p_led_control, p_evt_write->data);
                }
            }
             if (p_evt_write->handle == p_led_control->dim_led_handles.value_handle){
                if (p_evt_write->len == LCS_SIZE_DIM_LED_CHAR && p_led_control->dim_led_write_handler != NULL)
                {
                    print_write_req(p_evt_write);
                    p_led_control->dim_led_write_handler(p_led_control, p_evt_write->data[0]);
                }
            }
            break;
        }

        default: break;
    }
}

void led_control_service_get_adv_data(ble_lcs_t *p_led_control, ble_uuid_t *uuid)
{
    uuid->uuid = LCS_UUID_SERVICE;
    uuid->type = p_led_control->uuid_type;
}

void led_control_service_init(ble_lcs_t *p_led_control, ble_lcs_init_t *p_led_control_init)
{
	ret_code_t err_code;
    ble_uuid_t service_uuid;
    // add vendor specific uuid
    ble_uuid128_t base_uuid = LCS_UUID_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_led_control->uuid_type);
    APP_ERROR_CHECK(err_code);

    service_uuid.type = p_led_control->uuid_type;
    service_uuid.uuid = LCS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &p_led_control->service_handle);
    APP_ERROR_CHECK(err_code);	

    dim_led_char_add(p_led_control);
    debug_led_char_add(p_led_control);
    vled_char_add(p_led_control);
    isns_char_add(p_led_control);

    NRF_LOG_INFO("(LCS): Service UUID: 0x%04x", service_uuid.uuid);
    NRF_LOG_INFO("(LCS): Service UUID type: 0x%02x", service_uuid.type);
    NRF_LOG_INFO("(LCS): Service handle: 0x%02x", p_led_control->service_handle);
    NRF_LOG_INFO("(LCS): Debug char handles: 0x%02x/ 0x%02x", p_led_control->debug_led_handles.value_handle, p_led_control->debug_led_handles.cccd_handle);
    NRF_LOG_INFO("(LCS): DIM char handles: 0x%02x/ 0x%02x", p_led_control->dim_led_handles.value_handle, p_led_control->dim_led_handles.cccd_handle);
    NRF_LOG_INFO("(LCS): VLED char handles: 0x%02x/ 0x%02x", p_led_control->vled_handles.value_handle, p_led_control->vled_handles.cccd_handle);
    NRF_LOG_INFO("(LCS): ISNS char handles: 0x%02x/ 0x%02x", p_led_control->isns_handles.value_handle, p_led_control->isns_handles.cccd_handle);


    // ASSIGN INIT DATA (HANDLERS)
    p_led_control->debug_led_write_handler = p_led_control_init->debug_led_write_handler;
    p_led_control->dim_led_write_handler = p_led_control_init->dim_led_write_handler;
}