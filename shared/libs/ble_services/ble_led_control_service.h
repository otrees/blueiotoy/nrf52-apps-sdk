#ifndef LED_CONTROL_SERVICE_H__
#define LED_CONTROL_SERVICE_H__

#include <stdint.h>

#include "nrf_sdh_ble.h"
#include "ble.h"
#include "ble_srv_common.h"

#define LCS_UUID_BASE     {{0x23, 0xD1, 0x13, 0xEF, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}}

#define LCS_UUID_SERVICE                0xAAAA
#define LCS_UUID_DEBUG_LED_CHAR         0xBBBB
#define LCS_UUID_DIM_LED_CHAR           0xCCCC
#define LCS_UUID_VLED_CHAR              0xDDDD
#define LCS_UUID_ISNS_CHAR              0xEEEE


#define LCS_SIZE_DEBUG_LED_CHAR         3
#define LCS_SIZE_DIM_LED_CHAR           1
#define LCS_SIZE_VLED_CHAR              2
#define LCS_SIZE_ISNS_CHAR              2

#define LCS_INITIAL_DEBUG_LED_CHAR      0 
#define LCS_INITIAL_DIM_LED_CHAR        0 
#define LCS_INITIAL_VLED_CHAR           0
#define LCS_INITIAL_ISNS_CHAR           0


struct ble_lcs_s;

typedef void (*ble_led_control_debug_led_write_handler_t) (struct ble_lcs_s *p_led_control, uint8_t levels[LCS_SIZE_DEBUG_LED_CHAR]);
typedef void (*ble_led_control_dim_led_write_handler_t) (struct ble_lcs_s *p_led_control, uint8_t light_mode);

typedef struct
{
    ble_led_control_debug_led_write_handler_t debug_led_write_handler;
    ble_led_control_dim_led_write_handler_t dim_led_write_handler;
} ble_lcs_init_t;

typedef struct ble_lcs_s
{
    // HANDLE OF THE SERVICE
    uint16_t                    service_handle;
    uint8_t                     uuid_type;
    ble_gatts_char_handles_t    debug_led_handles;
    ble_gatts_char_handles_t    dim_led_handles;
    ble_gatts_char_handles_t    vled_handles;
    ble_gatts_char_handles_t    isns_handles;

    // READ / WRITE HANDLERS
    ble_led_control_debug_led_write_handler_t debug_led_write_handler;
    ble_led_control_dim_led_write_handler_t dim_led_write_handler;
    // ble_led_control_dim_led_read_handler_t      dim_led_read_handler;
    // ble_led_control_debug_led_read_handler_t    debug_led_read_handler;

} ble_lcs_t;


#define BLE_LCS_DEF(_name)                                                                        \
static ble_lcs_t _name;                                                                   \
NRF_SDH_BLE_OBSERVER(_name ## _obs, BLE_LCS_OBSERVER_PRIO, led_control_service_on_ble_evt, &_name)


void led_control_service_dim_led_char_notify(ble_lcs_t *p_led_control, uint8_t light_mode, uint16_t conn_handle);
void led_control_service_dim_led_char_update(ble_lcs_t * p_led_control, uint8_t light_mode);

void led_control_service_vled_char_update(ble_lcs_t *p_led_control, uint16_t vled_mV);
void led_control_service_vled_char_notify(ble_lcs_t *p_led_control, uint16_t vled_mV, uint16_t conn_handle);
void led_control_service_isns_char_update(ble_lcs_t *p_led_control, uint16_t isns_mA);
void led_control_service_isns_char_notify(ble_lcs_t *p_led_control, uint16_t isns_mA, uint16_t conn_handle);

/** Function for handling BLE Stack events related to service and its characteristic.*/
void led_control_service_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);

void led_control_service_get_adv_data(ble_lcs_t *p_led_control, ble_uuid_t *uuid);

/** Function for initializing service.*/
void led_control_service_init(ble_lcs_t *p_led_control, ble_lcs_init_t *p_led_control_init);

#endif  /* _ LED_CONTROL_SERVICE_H__ */