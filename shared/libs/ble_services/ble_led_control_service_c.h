#ifndef LED_CONTROL_SERVICE_C_H__
#define LED_CONTROL_SERVICE_C_H__

#include <stdint.h>

#include "nrf_sdh_ble.h"
#include "nrf_ble_gq.h"
#include "ble.h"
#include "ble_srv_common.h"
#include "ble_db_discovery.h" 

#define LCS_UUID_BASE     {{0x23, 0xD1, 0x13, 0xEF, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}}

#define LCS_UUID_SERVICE                0xAAAA
#define LCS_UUID_DEBUG_LED_CHAR         0xBBBB
#define LCS_UUID_DIM_LED_CHAR           0xCCCC


#define LCS_SIZE_DEBUG_LED_CHAR         3
#define LCS_SIZE_DIM_LED_CHAR           1


#define BLE_LCS_C_DEF(_name)                                                                        \
static ble_lcs_c_t _name;                                                                           \
NRF_SDH_BLE_OBSERVER(_name ## _obs, BLE_LCS_C_OBSERVER_PRIO, ble_lcs_c_on_ble_evt, &_name)


//TODO: onHvX
//TODO: send

typedef struct ble_lcs_c_s ble_lcs_c_t;

typedef enum
{
    BLE_LCS_C_EVT_DISCOVERY_COMPLETE = 1,           /** event indicating that the LED Control Service was discovered at the peer. */
    BLE_LCS_C_EVT_DIM_LED_NOTIFICATION,             /** event indicating that a notification of the LED Dim characteristic was received from the peer. */
    BLE_LCS_C_EVT_DIM_LED_READ   
} ble_lcs_c_evt_type_t;

typedef struct
{
    uint16_t dim_led_cccd_handle;
    uint16_t dim_led_value_handle;       
    uint16_t debug_led_value_handle;  
} ble_lcs_c_db_t;


typedef struct
{
    ble_lcs_c_evt_type_t evt_type;    
    uint16_t conn_handle; 
    union
    {
        ble_lcs_c_db_t lcs_handles_peer_db;         /** handles related to the service, found on the peer device. This is filled if the evt_type is @ref BLE_LCS_C_EVT_DISCOVERY_COMPLETE.*/
        uint8_t        light_mode;                  /** information about the Dim LED light mode, received from peer. This field is used for the events @ref BLE_LCS_EVT_DIMLED_NOTIFICATION and @ref READ_RESP.*/
    } params;
} ble_lcs_c_evt_t;


typedef void (* ble_lcs_c_evt_handler_t) (ble_lcs_c_t * p_ble_lbs_c, ble_lcs_c_evt_t * p_evt);

struct ble_lcs_c_s
{
    uint16_t                    conn_handle;   
    uint8_t                     uuid_type;     
    ble_gatts_char_handles_t    debug_led_handles;
    ble_gatts_char_handles_t    dim_led_handles;

    ble_lcs_c_evt_handler_t     evt_handler;        /** application event handler to be called when there is an event related to the LED Button service. */
    nrf_ble_gq_t*               p_gatt_queue;       /** pointer to the BLE GATT Queue instance. */
};

typedef struct
{
    ble_lcs_c_evt_handler_t     evt_handler;        /** event handler to be called by the LED Button Client module when there is an event related to the LED Button Service. */
    nrf_ble_gq_t*               p_gatt_queue;       /** pointer to the BLE GATT Queue instance. */
    ble_srv_error_handler_t     error_handler;      /** function to be called in case of an error. */
} ble_lcs_init_c_t;



ret_code_t ble_lcs_c_handles_assign(ble_lcs_c_t *p_ble_lcs_c, uint16_t conn_handle, ble_lcs_c_db_t *p_peer_handles);

ret_code_t ble_lcs_c_dimled_read(ble_lcs_c_t *p_ble_lcs_c);
ret_code_t ble_lcs_c_dimled_notification_enable(ble_lcs_c_t *p_ble_lcs_c, bool enable);
ret_code_t ble_lcs_c_dimled_write(ble_lcs_c_t *p_ble_lcs_c, uint8_t value);
ret_code_t ble_lcs_c_debugled_write(ble_lcs_c_t *p_ble_lcs_c, uint8_t value[3]);


void ble_lcs_c_on_db_disc_evt(ble_lcs_c_t *p_ble_lcs_c, ble_db_discovery_evt_t const *p_evt);

void ble_lcs_c_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);


void ble_lcs_c_init(ble_lcs_c_t *p_ble_lcs_c, ble_lcs_init_c_t *p_ble_lcs_c_init);



#endif  /* _ LED_CONTROL_SERVICE_C_H__ */