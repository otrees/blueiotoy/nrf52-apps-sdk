#ifndef BLE_UTILS_H__
#define BLE_UTILS_H__

#include <stdint.h>

#include "ble_led_control_service.h"
#include "ble_rfid_service.h"

#include "flash.h"

#define CHAR_VALIDITY_SYMBOL									0x88

#define FLASH_SIZE_LCS_DIM_LED_CHAR     			(FLASH_PROGRAM_UNIT)
#define FLASH_SIZE_RS_PAIRED_TAG_CHAR         (2*FLASH_PROGRAM_UNIT)
#define FLASH_SIZE_RS_ENABLED_CHAR         		(FLASH_PROGRAM_UNIT) 

#define FLASH_SIZE_CHARS_TOTAL								(4*FLASH_PROGRAM_UNIT) 												

#define FLASH_ADDR_LCS_DIM_LED_CHAR     			APP_FLASH_START_ADDR
#define FLASH_ADDR_RS_PAIRED_TAG_CHAR         (FLASH_ADDR_LCS_DIM_LED_CHAR + FLASH_SIZE_LCS_DIM_LED_CHAR)
#define FLASH_ADDR_RS_ENABLED_CHAR         		(FLASH_ADDR_RS_PAIRED_TAG_CHAR + FLASH_SIZE_RS_PAIRED_TAG_CHAR) 

uint8_t get_lcs_dim_led_from_flash();
uint32_t get_rs_paired_tag_from_flash();
uint8_t get_rs_enabled_from_flash();
// void set_lcs_dim_led_to_flash(uint8_t light_mode);
// void set_rs_paired_tag_to_flash(uint32_t tag_id);
// void set_rs_enabled_to_flash(uint8_t enabled);

void get_char_values_from_flash(uint8_t *light_mode, uint32_t *rfid_tag_id, uint8_t *rfid_enabled);
void erase_and_set_char_values_to_flash(uint8_t light_mode, uint32_t tag_id, uint8_t enabled);


void ble_send_notification(uint16_t handle, uint16_t len, uint8_t *data, uint16_t conn_handle, char *service_name);

void ble_set_char_value(uint16_t handle, uint16_t len, uint8_t *data, char *service_name);
void ble_get_char_value(uint16_t handle, uint16_t len, uint8_t *data);

#endif  /*  BLE_UTILS_H__ */