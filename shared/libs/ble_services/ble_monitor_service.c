#include "ble_monitor_service.h"
#include <stdint.h>

#include "nrf_log.h"
#include "app_error.h"

#include "ble_service_utils.h"


/* #region CHAR DEFINITIONS */
static ret_code_t battery_level_char_add(ble_ms_t *p_monitor)
{
    ble_add_char_params_t  add_char_params;
    uint8_t initial_value[MS_SIZE_BATTERY_LEVEL_CHAR] = {MS_INITIAL_VALUE_CHAR, 0, 0};

    memcpy(&p_monitor->battery_level_current, initial_value, MS_SIZE_BATTERY_LEVEL_CHAR);
    memcpy(add_char_params.p_init_value, initial_value, MS_SIZE_BATTERY_LEVEL_CHAR);

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = MS_UUID_BATTERY_LEVEL_CHAR;
    add_char_params.max_len           = MS_SIZE_BATTERY_LEVEL_CHAR;
    add_char_params.init_len          = MS_SIZE_BATTERY_LEVEL_CHAR;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_monitor->service_handle, &add_char_params, &(p_monitor->battery_level_handles));
}

static ret_code_t battery_charge_char_add(ble_ms_t *p_monitor)
{
    ble_add_char_params_t  add_char_params;
    uint8_t                initial_value = MS_INITIAL_VALUE_CHAR;
    p_monitor->battery_charge_current = initial_value;

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = MS_UUID_BATTERY_CHARGE_CHAR;
    add_char_params.max_len           = MS_SIZE_BATTERY_CHARGE_CHAR;
    add_char_params.init_len          = MS_SIZE_BATTERY_CHARGE_CHAR;
    add_char_params.p_init_value      = &initial_value;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_monitor->service_handle, &add_char_params, &(p_monitor->battery_charge_handles));
}

static ret_code_t temp_char_add(ble_ms_t *p_monitor)
{
    ble_add_char_params_t  add_char_params;
    int32_t                initial_value = 0;
    p_monitor->temp_current = initial_value;

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = MS_UUID_TEMP_CHAR;
    add_char_params.max_len           = MS_SIZE_TEMP_CHAR;
    add_char_params.init_len          = MS_SIZE_TEMP_CHAR;
    add_char_params.p_init_value      = (uint8_t*) &initial_value;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_monitor->service_handle, &add_char_params, &(p_monitor->temp_handles));
}
/* #endregion */


/* #region BATTERY LEVEL CHAR - update */
static void get_battery_level_char_value(uint8_t *data, uint8_t battery_level, int16_t battery_voltage)
{
    data[0] = battery_level;
    memcpy(&(data[1]), &battery_voltage, sizeof(int16_t));
}

void monitor_service_battery_level_char_update(ble_ms_t* p_monitor, uint8_t battery_level, int16_t battery_voltage)
{
    uint8_t new_value[MS_SIZE_BATTERY_LEVEL_CHAR];
    get_battery_level_char_value(new_value, battery_level, battery_voltage);

    // do not update value when it is same
    if (!memcmp(p_monitor->battery_level_current, new_value, MS_SIZE_BATTERY_LEVEL_CHAR)) return;

    uint16_t handle = p_monitor->battery_level_handles.value_handle;

    ble_set_char_value(handle, MS_SIZE_BATTERY_LEVEL_CHAR, new_value, "MS");
}

void monitor_service_battery_level_char_notify(ble_ms_t* p_monitor, uint8_t battery_level, int16_t battery_voltage, uint16_t conn_handle)
{
    uint8_t new_value[MS_SIZE_BATTERY_LEVEL_CHAR];
    get_battery_level_char_value(new_value, battery_level, battery_voltage);
    uint16_t handle = p_monitor->battery_level_handles.value_handle;

    ble_send_notification(handle, MS_SIZE_BATTERY_LEVEL_CHAR, new_value, conn_handle, "MS");
}
/* #endregion */


/* #region BATTERY CHARGE CHAR - update */
void monitor_service_battery_charge_char_update(ble_ms_t* p_monitor, uint8_t battery_charge)
{
    // do not update value when it is same
    if (p_monitor->battery_charge_current == battery_charge) return;
    uint16_t handle = p_monitor->battery_charge_handles.value_handle;
    ble_set_char_value(handle, MS_SIZE_BATTERY_CHARGE_CHAR, &battery_charge, "MS");
}

void monitor_service_battery_charge_char_notify(ble_ms_t* p_monitor, uint8_t battery_charge, uint16_t conn_handle)
{
    uint16_t handle = p_monitor->battery_charge_handles.value_handle;
    ble_send_notification(handle, MS_SIZE_BATTERY_CHARGE_CHAR, &battery_charge, conn_handle, "MS");
}
/* #endregion */


/* #region TEMPERATURE CHAR - update */
void monitor_service_temp_char_update(ble_ms_t* p_monitor, int32_t temperature)
{
    // do not update value when it is same
    if (p_monitor->temp_current == temperature) return;
    uint16_t handle = p_monitor->temp_handles.value_handle;
    ble_set_char_value(handle, MS_SIZE_TEMP_CHAR, (uint8_t*) &temperature, "MS");
}

void monitor_service_temp_char_notify(ble_ms_t* p_monitor, int32_t temperature, uint16_t conn_handle)
{
    uint16_t handle = p_monitor->temp_handles.value_handle;
    ble_send_notification(handle, MS_SIZE_TEMP_CHAR, (uint8_t*) &temperature, conn_handle, "MS");
}
/* #endregion */


void monitor_service_get_adv_data(ble_ms_t *p_monitor, ble_uuid_t *uuid)
{
    uuid->type = p_monitor->uuid_type;
    uuid->uuid = MS_UUID_SERVICE;
}

void monitor_service_init(ble_ms_t *p_monitor)
{
	uint32_t err_code;
    ble_uuid_t service_uuid;
    ble_uuid128_t base_uuid = MS_UUID_BASE;

    // add vendor specific uuid (index is written by softdevice)
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_monitor->uuid_type);
    APP_ERROR_CHECK(err_code);

    service_uuid.type = p_monitor->uuid_type;
    service_uuid.uuid = MS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &p_monitor->service_handle);
    APP_ERROR_CHECK(err_code);
	
    // ADD CHARACTERISTICS
    battery_level_char_add(p_monitor);
    battery_charge_char_add(p_monitor);
    temp_char_add(p_monitor);

    NRF_LOG_INFO("(MS): Service UUID: 0x%04x", service_uuid.uuid);
    NRF_LOG_INFO("(MS): Service UUID type: 0x%02x", service_uuid.type);
    NRF_LOG_INFO("(MS): Service handle: 0x%04x", p_monitor->service_handle);
    NRF_LOG_INFO("(MS): Battery Level handles: 0x%02x/ 0x%02x", p_monitor->battery_level_handles.value_handle, p_monitor->battery_level_handles.cccd_handle);
    NRF_LOG_INFO("(MS): Battery Charging handles: 0x%02x/ 0x%02x", p_monitor->battery_charge_handles.value_handle, p_monitor->battery_charge_handles.cccd_handle);
    NRF_LOG_INFO("(MS): Temperature handles: 0x%02x/ 0x%02x", p_monitor->temp_handles.value_handle, p_monitor->temp_handles.cccd_handle);
}