#include "ble_rfid_service.h"

#include <string.h>

#include "nrf_log.h"
#include "app_error.h"

#include "app_config.h"
#include "utils.h"
#include "ble_service_utils.h"


static uint32_t get_paired_tag_initial_value()
{
#if defined(BLE_CHAR_VALUES_BACKUP) && BLE_CHAR_VALUES_BACKUP == 1
    uint32_t tag_id = get_rs_paired_tag_from_flash();
    if (tag_id == (uint32_t) -1) return RS_INITIAL_PAIRED_TAG_CHAR;
    NRF_LOG_INFO("(RS): Initial tag id value read from flash: %d", tag_id);
    return tag_id;
#else
    return RS_INITIAL_PAIRED_TAG_CHAR;
#endif
}

static uint8_t get_enabled_initial_value()
{
#if defined(BLE_CHAR_VALUES_BACKUP) && BLE_CHAR_VALUES_BACKUP == 1
    uint8_t enabled = get_rs_enabled_from_flash();
    if (enabled == (uint8_t) -1) return RS_INITIAL_ENABLED_CHAR;
    NRF_LOG_INFO("(RS): Initial enabled value read from flash: %d", enabled);
    return enabled;
#else
    return RS_INITIAL_ENABLED_CHAR;
#endif
}


/* #region CHAR DEFINITIONS */
static ret_code_t detected_tag_char_add(ble_rs_t *p_rfid)
{
    ble_add_char_params_t  add_char_params;
    uint32_t                initial_value = RS_INITIAL_DETECTED_TAG_CHAR;

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = RS_UUID_DETECTED_TAG_CHAR;
    add_char_params.max_len           = RS_SIZE_DETECTED_TAG_CHAR;
    add_char_params.init_len          = RS_SIZE_DETECTED_TAG_CHAR;
    add_char_params.p_init_value      = (uint8_t*) &initial_value;
    add_char_params.char_props.notify = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 0;
    add_char_params.cccd_write_access = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;

    return characteristic_add(p_rfid->service_handle, &add_char_params, &(p_rfid->detected_tag_handles));
}

static ret_code_t paired_tag_char_add(ble_rs_t *p_rfid)
{
    ble_add_char_params_t  add_char_params;
    uint32_t                initial_value = get_paired_tag_initial_value();

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = RS_UUID_PAIRED_TAG_CHAR;
    add_char_params.max_len           = RS_SIZE_PAIRED_TAG_CHAR;
    add_char_params.init_len          = RS_SIZE_PAIRED_TAG_CHAR;
    add_char_params.p_init_value      = (uint8_t*) &initial_value;
    add_char_params.char_props.notify = 0;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;
    add_char_params.write_access      = SEC_JUST_WORKS;

    return characteristic_add(p_rfid->service_handle,&add_char_params, &(p_rfid->paired_tag_handles));
}

static ret_code_t enabled_char_add(ble_rs_t *p_rfid)
{
    ble_add_char_params_t  add_char_params;
    uint8_t                initial_value = get_enabled_initial_value();

    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = RS_UUID_ENABLED_CHAR;
    add_char_params.max_len           = RS_SIZE_ENABLED_CHAR;
    add_char_params.init_len          = RS_SIZE_ENABLED_CHAR;
    add_char_params.p_init_value      = &initial_value;
    add_char_params.char_props.notify = 0;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.write  = 1;
    add_char_params.read_access       = SEC_JUST_WORKS;
    add_char_params.write_access      = SEC_JUST_WORKS;

    return characteristic_add(p_rfid->service_handle,&add_char_params, &(p_rfid->enabled_handles));
}
/* #endregion CHAR DEFINITIONS */

/* #region DETECTED TAG CHAR - update */
void rfid_service_detected_tag_char_update(ble_rs_t * p_rfid, uint32_t tag_id)
{
    uint16_t handle = p_rfid->detected_tag_handles.value_handle;
    ble_set_char_value(handle, RS_SIZE_DETECTED_TAG_CHAR, (uint8_t*) &tag_id, "RS");
}


void rfid_service_detected_tag_char_notify(ble_rs_t *p_rfid, uint32_t tag_id, uint16_t conn_handle)
{
    uint16_t handle = p_rfid->detected_tag_handles.value_handle;
    ble_send_notification(handle, RS_SIZE_DETECTED_TAG_CHAR, (uint8_t*) &tag_id, conn_handle, "RS");
}
/* #endregion */

static void print_write_req(ble_gatts_evt_write_t *p_evt_write)
{
    NRF_LOG_INFO("(RS): GATTS_EVT_WRITE:");
    print_hex_array(p_evt_write->data, p_evt_write->len);
}

void rfid_service_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context)
{
  	ble_rs_t *p_rfid =(ble_rs_t *) p_context;  

    switch (p_ble_evt->header.evt_id)
    { 
        case BLE_GATTS_EVT_WRITE: {
            ble_gatts_evt_write_t *p_evt_write = (ble_gatts_evt_write_t*) &p_ble_evt->evt.gatts_evt.params.write;

            
            if (p_evt_write->handle == p_rfid->paired_tag_handles.value_handle){
                if (p_evt_write->len == RS_SIZE_DETECTED_TAG_CHAR && p_rfid->paired_tag_write_handler != NULL)
                {
                    print_write_req(p_evt_write);
                    p_rfid->paired_tag_write_handler(p_rfid, *((uint32_t*) p_evt_write->data));
                    print_write_req(p_evt_write);
                }
            }
             if (p_evt_write->handle == p_rfid->enabled_handles.value_handle){
                if (p_evt_write->len == RS_SIZE_ENABLED_CHAR && p_rfid->enabled_write_handler != NULL)
                {
                    print_write_req(p_evt_write);
                    p_rfid->enabled_write_handler(p_rfid, p_evt_write->data[0]);
                }
            }
            break;
        }

        default: break;
    }
}

void rfid_service_get_adv_data(ble_rs_t *p_rfid, ble_uuid_t *uuid)
{
    uuid->uuid = RS_UUID_SERVICE;
    uuid->type = p_rfid->uuid_type;
}

void rfid_service_init(ble_rs_t *p_rfid, ble_rs_init_t *p_rfid_init)
{
	ret_code_t err_code;
    ble_uuid_t service_uuid;
    // add vendor specific uuid
    ble_uuid128_t base_uuid = RS_UUID_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_rfid->uuid_type);
    APP_ERROR_CHECK(err_code);

    service_uuid.type = p_rfid->uuid_type;
    service_uuid.uuid = RS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &p_rfid->service_handle);
    APP_ERROR_CHECK(err_code);	

    // ADD CHARACTERISTICS
    detected_tag_char_add(p_rfid);
    paired_tag_char_add(p_rfid);
    enabled_char_add(p_rfid);

    NRF_LOG_INFO("(RS): Service UUID: 0x%04x", service_uuid.uuid);
    NRF_LOG_INFO("(RS): Service UUID: 0x%04x", base_uuid.uuid128);
    NRF_LOG_INFO("(RS): Service UUID type: 0x%02x", service_uuid.type);
    NRF_LOG_INFO("(RS): Service handle: 0x%02x", p_rfid->service_handle);
    NRF_LOG_INFO("(RS): Detected tag char handles: 0x%02x/ 0x%02x", p_rfid->detected_tag_handles.value_handle, p_rfid->detected_tag_handles.cccd_handle);
    NRF_LOG_INFO("(RS): Paired tag char handles: 0x%02x", p_rfid->paired_tag_handles.value_handle);
    NRF_LOG_INFO("(RS): Enabled char handles: 0x%02x", p_rfid->enabled_handles.value_handle);

    // ASSIGN INIT DATA (HANDLERS)
    p_rfid->paired_tag_write_handler= p_rfid_init->paired_tag_write_handler;
    p_rfid->enabled_write_handler = p_rfid_init->enabled_write_handler;
}