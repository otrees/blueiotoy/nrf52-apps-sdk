#ifndef MONITOR_SERVICE_H__
#define MONITOR_SERVICE_H__

#include <stdint.h>
#include "ble.h"
#include "ble_srv_common.h"

#define MS_UUID_BASE     {{0x24, 0xD2, 0x14, 0xF0, 0x60, 0x79, 0x24, 0x16, 0xDF, 0xF0, 0x13, 0x14, 0x00, 0x00, 0x00, 0x00}}

#define MS_UUID_SERVICE                 0xAAAA
#define MS_UUID_BATTERY_LEVEL_CHAR      0xBBBB
#define MS_UUID_BATTERY_CHARGE_CHAR     0xCCCC
#define MS_UUID_TEMP_CHAR               0xDDDD

#define MS_UUID_BATTERY_LEVEL_DESC      0xBBFF


#define MS_SIZE_BATTERY_LEVEL_CHAR      3
#define MS_SIZE_BATTERY_CHARGE_CHAR     1
#define MS_SIZE_TEMP_CHAR               4

#define MS_INITIAL_VALUE_CHAR           -1

#define BLE_MS_DEF(_name)                               \
static ble_ms_t _name;                         \

typedef struct
{
    //HANDLE OF THE SERVICE
    uint16_t                    service_handle;
    uint8_t                     uuid_type;
    ble_gatts_char_handles_t    battery_level_handles;
    ble_gatts_char_handles_t    battery_charge_handles;
    ble_gatts_char_handles_t    temp_handles;

    // CURRENT
    uint8_t battery_level_current[MS_SIZE_BATTERY_LEVEL_CHAR];
    uint8_t battery_charge_current;
    int32_t temp_current;

} ble_ms_t;


void monitor_service_battery_level_char_update(ble_ms_t* p_monitor, uint8_t battery_level, int16_t battery_voltage);
void monitor_service_battery_level_char_notify(ble_ms_t* p_monitor, uint8_t battery_level, int16_t battery_voltage, uint16_t conn_handle);

void monitor_service_battery_charge_char_update(ble_ms_t* p_monitor, uint8_t batery_charge);
void monitor_service_battery_charge_char_notify(ble_ms_t* p_monitor, uint8_t batery_charge, uint16_t conn_handle);

void monitor_service_temp_char_update(ble_ms_t* p_monitor, int32_t temperature);
void monitor_service_temp_char_notify(ble_ms_t* p_monitor, int32_t temperature, uint16_t conn_handle);

/**@brief Function for initializing service.
 *
 * @param[in]   p_monitor       Pointer to Our Service structure.
 */
void monitor_service_init(ble_ms_t *p_monitor);

void monitor_service_get_adv_data(ble_ms_t *p_monitor, ble_uuid_t *uuid);


#endif  /* _ MONITOR_SERVICE_H__ */