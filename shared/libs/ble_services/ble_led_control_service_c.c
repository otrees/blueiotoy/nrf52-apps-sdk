#include <stdint.h>
#include <string.h>

#include "app_error.h"
#include "nrf_log.h"

#include "ble_led_control_service_c.h"
#include "utils.h"


static void gatt_error_handler(uint32_t nrf_error, void *p_ctx, uint16_t conn_handle)
{
    // ble_lcs_c_t *p_lcs_c = (ble_lcs_c_t*) p_ctx;

    NRF_LOG_ERROR("(LCS): A GATT Client error has occurred on conn_handle: 0X%X", conn_handle);
}

static void check_and_assign_conn_handles(ble_lcs_c_t *p_ble_lcs_c, ble_lcs_c_db_t *p_peer_handles)
{
    if ((p_ble_lcs_c->dim_led_handles.value_handle == BLE_GATT_HANDLE_INVALID)&&
        (p_ble_lcs_c->dim_led_handles.cccd_handle == BLE_GATT_HANDLE_INVALID)&&
        (p_ble_lcs_c->debug_led_handles.value_handle == BLE_GATT_HANDLE_INVALID))
    {
        p_ble_lcs_c->dim_led_handles.value_handle = p_peer_handles->dim_led_value_handle;
        p_ble_lcs_c->dim_led_handles.cccd_handle = p_peer_handles->dim_led_cccd_handle;
        p_ble_lcs_c->debug_led_handles.value_handle = p_peer_handles->debug_led_value_handle;
        NRF_LOG_INFO("H: %d", p_peer_handles->dim_led_value_handle);
        NRF_LOG_INFO("H: %d", p_peer_handles->dim_led_cccd_handle);
        NRF_LOG_INFO("H: %d", p_peer_handles->debug_led_value_handle);
    }
}


// public function
/** NOTE: called on BLE_LCS_C_EVT_DISCOVERY_COMPLETE **/
ret_code_t ble_lcs_c_handles_assign(ble_lcs_c_t *p_ble_lcs_c, uint16_t conn_handle, ble_lcs_c_db_t *p_peer_handles)
{
    p_ble_lcs_c->conn_handle = conn_handle;
    if (p_peer_handles != NULL)
        check_and_assign_conn_handles(p_ble_lcs_c, p_peer_handles);
    return nrf_ble_gq_conn_handle_register(p_ble_lcs_c->p_gatt_queue, conn_handle);
}

ret_code_t ble_lcs_c_dimled_read(ble_lcs_c_t *p_ble_lcs_c)
{
    if (p_ble_lcs_c->conn_handle == BLE_CONN_HANDLE_INVALID) return NRF_ERROR_INVALID_STATE;
    NRF_LOG_DEBUG("Reading Dim LED light mode");

    nrf_ble_gq_req_t lcs_read_req;
    memset(&lcs_read_req, 0, sizeof(nrf_ble_gq_req_t));
    lcs_read_req.type                     = NRF_BLE_GQ_REQ_GATTC_READ;
    lcs_read_req.error_handler.cb         = gatt_error_handler;
    lcs_read_req.error_handler.p_ctx      = p_ble_lcs_c;
    lcs_read_req.params.gattc_read.handle = p_ble_lcs_c->dim_led_handles.value_handle;

    return nrf_ble_gq_item_add(p_ble_lcs_c->p_gatt_queue, &lcs_read_req, p_ble_lcs_c->conn_handle);
}

ret_code_t ble_lcs_c_dimled_notification_enable(ble_lcs_c_t *p_ble_lcs_c, bool enable)
{

    if (p_ble_lcs_c->conn_handle == BLE_CONN_HANDLE_INVALID) return NRF_ERROR_INVALID_STATE;

    NRF_LOG_DEBUG("Configuring Dim LED CCCD. CCCD Handle = %d, Connection Handle = %d",
                  p_ble_lcs_c->dim_led_handles.cccd_handle, p_ble_lcs_c->conn_handle);

    uint16_t cccd_val = enable? BLE_GATT_HVX_NOTIFICATION : 0;
    uint8_t  cccd[BLE_CCCD_VALUE_LEN];
    cccd[0] = LSB_16(cccd_val);
    cccd[1] = MSB_16(cccd_val);

    nrf_ble_gq_req_t lcs_writecccd_req;
    memset(&lcs_writecccd_req, 0, sizeof(nrf_ble_gq_req_t));
    lcs_writecccd_req.type                        = NRF_BLE_GQ_REQ_GATTC_WRITE;
    lcs_writecccd_req.error_handler.cb            = gatt_error_handler;
    lcs_writecccd_req.error_handler.p_ctx         = p_ble_lcs_c;
    lcs_writecccd_req.params.gattc_write.handle   = p_ble_lcs_c->dim_led_handles.cccd_handle;
    lcs_writecccd_req.params.gattc_write.len      = BLE_CCCD_VALUE_LEN;
    lcs_writecccd_req.params.gattc_write.offset   = 0;
    lcs_writecccd_req.params.gattc_write.p_value  = cccd;
    lcs_writecccd_req.params.gattc_write.write_op = BLE_GATT_OP_WRITE_REQ;

    return nrf_ble_gq_item_add(p_ble_lcs_c->p_gatt_queue, &lcs_writecccd_req, p_ble_lcs_c->conn_handle);
}

ret_code_t ble_lcs_c_dimled_write(ble_lcs_c_t *p_ble_lcs_c, uint8_t value)
{
    if (p_ble_lcs_c->conn_handle == BLE_CONN_HANDLE_INVALID) return NRF_ERROR_INVALID_STATE;

    NRF_LOG_DEBUG("Writing Dim LED light mode 0x%x", value);

    nrf_ble_gq_req_t lcs_write_req;
    memset(&lcs_write_req, 0, sizeof(nrf_ble_gq_req_t));
    lcs_write_req.type                        = NRF_BLE_GQ_REQ_GATTC_WRITE;
    lcs_write_req.error_handler.cb            = gatt_error_handler;
    lcs_write_req.error_handler.p_ctx         = p_ble_lcs_c;
    lcs_write_req.params.gattc_write.handle   = p_ble_lcs_c->dim_led_handles.value_handle;
    lcs_write_req.params.gattc_write.len      = LCS_SIZE_DIM_LED_CHAR;
    lcs_write_req.params.gattc_write.p_value  = &value;
    lcs_write_req.params.gattc_write.offset   = 0;
    lcs_write_req.params.gattc_write.write_op = BLE_GATT_OP_WRITE_CMD; 

    return nrf_ble_gq_item_add(p_ble_lcs_c->p_gatt_queue, &lcs_write_req, p_ble_lcs_c->conn_handle);
}

ret_code_t ble_lcs_c_debugled_write(ble_lcs_c_t *p_ble_lcs_c, uint8_t value[3])
{
    if (p_ble_lcs_c->conn_handle == BLE_CONN_HANDLE_INVALID) return NRF_ERROR_INVALID_STATE;

    NRF_LOG_INFO("Writing Debug LED color 0x%x, 0x%x, 0x%x", value[0], value[1], value[2]);

    nrf_ble_gq_req_t lcs_write_req;
    memset(&lcs_write_req, 0, sizeof(nrf_ble_gq_req_t));
    lcs_write_req.type                        = NRF_BLE_GQ_REQ_GATTC_WRITE;
    lcs_write_req.error_handler.cb            = gatt_error_handler;
    lcs_write_req.error_handler.p_ctx         = p_ble_lcs_c;
    lcs_write_req.params.gattc_write.handle   = p_ble_lcs_c->debug_led_handles.value_handle;
    lcs_write_req.params.gattc_write.len      = LCS_SIZE_DEBUG_LED_CHAR;
    lcs_write_req.params.gattc_write.p_value  = value;
    lcs_write_req.params.gattc_write.offset   = 0;
    lcs_write_req.params.gattc_write.write_op = BLE_GATT_OP_WRITE_CMD; 

    return nrf_ble_gq_item_add(p_ble_lcs_c->p_gatt_queue, &lcs_write_req, p_ble_lcs_c->conn_handle);
}

// public function
void ble_lcs_c_on_ble_evt(ble_evt_t const *p_ble_evt, void * p_context)
{
    if (p_context == NULL || p_ble_evt == NULL) return;

    // ble_lcs_c_t *p_ble_lcs_c = (ble_lcs_c_t*) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        // case BLE_GATTC_EVT_HVX:
        //     on_hvx(p_ble_lbs_c, p_ble_evt);
        //     break;

        // case BLE_GAP_EVT_DISCONNECTED:
        //     on_disconnected(p_ble_lbs_c, p_ble_evt);
        //     break;

        default: break;
    }
}


// public function
void ble_lcs_c_on_db_disc_evt(ble_lcs_c_t *p_ble_lcs_c, ble_db_discovery_evt_t const *p_evt)
{
    // check if the LED Control Service was discovered.
    if (p_evt->evt_type != BLE_DB_DISCOVERY_COMPLETE ||
        p_evt->params.discovered_db.srv_uuid.uuid != LCS_UUID_SERVICE ||
        p_evt->params.discovered_db.srv_uuid.type != p_ble_lcs_c->uuid_type)
    return;

    ble_lcs_c_evt_t evt = {
        .evt_type  = BLE_LCS_C_EVT_DISCOVERY_COMPLETE,
        .conn_handle = p_evt->conn_handle
    };

    ble_lcs_c_db_t *handles = &(evt.params.lcs_handles_peer_db);

    for (int i = 0; i < p_evt->params.discovered_db.char_count; i++)
    {
        const ble_gatt_db_char_t *p_char = &(p_evt->params.discovered_db.charateristics[i]);
        NRF_LOG_INFO("CHAR: %x", p_char-> characteristic.uuid.uuid);

        if (p_char-> characteristic.uuid.uuid == LCS_UUID_DEBUG_LED_CHAR)
        {
            NRF_LOG_INFO("CHAR: debug led");
            handles->debug_led_value_handle = p_char->characteristic.handle_value;
        }

        if (p_char-> characteristic.uuid.uuid == LCS_UUID_DIM_LED_CHAR)
        {
            NRF_LOG_INFO("CHAR: dim led");
            handles->dim_led_value_handle = p_char->characteristic.handle_value;
            handles->dim_led_cccd_handle = p_char->cccd_handle;
        }
    }

    NRF_LOG_DEBUG("LED Control Service discovered at peer.");

    //TODO: check this
    // // if the conn_handle was assigned prior to db_discovery, assign the db_handles
    // if (p_ble_lcs_c->conn_handle != BLE_CONN_HANDLE_INVALID)
    //     check_and_assign_conn_handles(p_ble_lcs_c, handles);

    p_ble_lcs_c->evt_handler(p_ble_lcs_c, &evt);
}


// public function
void ble_lcs_c_init(ble_lcs_c_t *p_ble_lcs_c, ble_lcs_init_c_t *p_ble_lcs_c_init)
{
    ret_code_t err_code;
    ble_uuid_t lcs_uuid;
    ble_uuid128_t lcs_base_uuid = LCS_UUID_BASE;

    p_ble_lcs_c->dim_led_handles.cccd_handle    = BLE_GATT_HANDLE_INVALID;
    p_ble_lcs_c->dim_led_handles.value_handle   = BLE_GATT_HANDLE_INVALID;
    p_ble_lcs_c->debug_led_handles.value_handle = BLE_GATT_HANDLE_INVALID;
    p_ble_lcs_c->conn_handle                    = BLE_CONN_HANDLE_INVALID;
    p_ble_lcs_c->evt_handler                    = p_ble_lcs_c_init->evt_handler;
    p_ble_lcs_c->p_gatt_queue                   = p_ble_lcs_c_init->p_gatt_queue;
    // p_ble_lcs_c->error_handler                  = p_ble_lcs_c_init->error_handler;

    err_code = sd_ble_uuid_vs_add(&lcs_base_uuid, &p_ble_lcs_c->uuid_type);
    APP_ERROR_CHECK(err_code);

    lcs_uuid.type = p_ble_lcs_c->uuid_type;
    lcs_uuid.uuid = LCS_UUID_SERVICE;

    // register service for discovery
    err_code = ble_db_discovery_evt_register(&lcs_uuid);
    APP_ERROR_CHECK(err_code);
}