#include "ble_service_utils.h"

#include "ble_conn_state.h"
#include "ble_gatts.h"

#include "nrf_log.h"
#include "nrf_delay.h"

#include "app_error.h"


static uint8_t read_buf[FLASH_SIZE_CHARS_TOTAL];
static uint8_t write_buf[FLASH_SIZE_CHARS_TOTAL];

static bool check_validity(uint8_t *char_value, uint8_t char_len)
{
    return char_value[char_len] == CHAR_VALIDITY_SYMBOL;
}


void get_char_values_from_flash(uint8_t *light_mode, uint32_t *tag_id, uint8_t *enabled)
{
    flash_read(FLASH_ADDR_LCS_DIM_LED_CHAR, read_buf, FLASH_SIZE_CHARS_TOTAL);
    // for (int i =0; i < 16; i++) NRF_LOG_INFO("%d", data[i]);

    // light mode
    uint8_t idx = 0;
    *light_mode = check_validity(&(read_buf[idx]), LCS_SIZE_DIM_LED_CHAR)? read_buf[idx] : -1;

    // paired tag
    idx += FLASH_SIZE_LCS_DIM_LED_CHAR; 
    uint32_t tmp = *((uint32_t*) &(read_buf[idx]));
    *tag_id = check_validity(&(read_buf[idx]), RS_SIZE_PAIRED_TAG_CHAR)? tmp : -1;

    //enabled
    idx += FLASH_SIZE_RS_PAIRED_TAG_CHAR; 
    *enabled = check_validity(&(read_buf[idx]), RS_SIZE_ENABLED_CHAR)? read_buf[idx] : -1;
}

void erase_and_set_char_values_to_flash(uint8_t light_mode, uint32_t tag_id, uint8_t enabled)
{
    memset(write_buf, 0, FLASH_SIZE_CHARS_TOTAL);
    uint8_t idx = 0;

    flash_erase(FLASH_ADDR_LCS_DIM_LED_CHAR, 1);

    memcpy(write_buf, &light_mode, LCS_SIZE_DIM_LED_CHAR);
    write_buf[idx + LCS_SIZE_DIM_LED_CHAR] = CHAR_VALIDITY_SYMBOL;
    idx += FLASH_SIZE_LCS_DIM_LED_CHAR; 

    memcpy(&(write_buf[idx]), &tag_id, RS_SIZE_PAIRED_TAG_CHAR);
    write_buf[idx + RS_SIZE_PAIRED_TAG_CHAR] = CHAR_VALIDITY_SYMBOL;
    idx += FLASH_SIZE_RS_PAIRED_TAG_CHAR; 

    memcpy(&(write_buf[idx]), &enabled, RS_SIZE_ENABLED_CHAR);
    write_buf[idx + RS_SIZE_ENABLED_CHAR] = CHAR_VALIDITY_SYMBOL;
    idx += FLASH_SIZE_RS_ENABLED_CHAR; 
    // for (int i =0; i < 16; i++) NRF_LOG_INFO("%d", data[i]);

    flash_write(FLASH_ADDR_LCS_DIM_LED_CHAR, write_buf, FLASH_SIZE_CHARS_TOTAL);
}

uint8_t get_lcs_dim_led_from_flash()
{
    flash_read(FLASH_ADDR_LCS_DIM_LED_CHAR, read_buf, FLASH_SIZE_LCS_DIM_LED_CHAR);
    return (check_validity(read_buf, LCS_SIZE_DIM_LED_CHAR))? *read_buf: -1;
}

uint32_t get_rs_paired_tag_from_flash()
{
    flash_read(FLASH_ADDR_RS_PAIRED_TAG_CHAR, read_buf, FLASH_SIZE_RS_PAIRED_TAG_CHAR);
    return (check_validity(read_buf, RS_SIZE_PAIRED_TAG_CHAR))? *((uint32_t *) read_buf): -1;
}

uint8_t get_rs_enabled_from_flash()
{
    flash_read(FLASH_ADDR_RS_ENABLED_CHAR, read_buf, FLASH_SIZE_RS_ENABLED_CHAR);
    return check_validity(read_buf, RS_SIZE_ENABLED_CHAR)? *read_buf: -1;
}

// void set_lcs_dim_led_to_flash(uint8_t light_mode)
// {
//     flash_erase(APP_FLASH_START_ADDR, 1); 

//     uint8_t light_mode_array[FLASH_SIZE_LCS_DIM_LED_CHAR];
//     light_mode_array[0] = light_mode;
//     light_mode_array[LCS_SIZE_DIM_LED_CHAR] = CHAR_VALIDITY_SYMBOL;

//     flash_write(FLASH_ADDR_LCS_DIM_LED_CHAR, light_mode_array, FLASH_SIZE_LCS_DIM_LED_CHAR);
// }

// void set_rs_paired_tag_to_flash(uint32_t tag_id)
// {
//     uint8_t tag_id_array[FLASH_SIZE_RS_PAIRED_TAG_CHAR];
//     memcpy(tag_id_array, &tag_id, RS_SIZE_PAIRED_TAG_CHAR);
//     tag_id_array[RS_SIZE_PAIRED_TAG_CHAR] = CHAR_VALIDITY_SYMBOL;

//     flash_write(FLASH_ADDR_RS_PAIRED_TAG_CHAR, tag_id_array, FLASH_SIZE_RS_PAIRED_TAG_CHAR);
// }

// void set_rs_enabled_to_flash(uint8_t enabled)
// {
//     uint8_t enabled_array[FLASH_SIZE_RS_ENABLED_CHAR];
//     enabled_array[0] = enabled;
//     enabled_array[RS_SIZE_ENABLED_CHAR] = CHAR_VALIDITY_SYMBOL;

//     flash_write(FLASH_ADDR_RS_ENABLED_CHAR, enabled_array, FLASH_SIZE_RS_ENABLED_CHAR);
// }




static void check_notification_error_code(ret_code_t err_code, uint16_t conn_handle, char *service_name)
{
    if (err_code == NRF_ERROR_INVALID_STATE || BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    {
        NRF_LOG_WARNING("(%s): Notification disabled on connection (0x%X)", service_name, conn_handle);
    }
    else if (err_code == NRF_SUCCESS)
    {
        NRF_LOG_DEBUG("(%s): Notification sent successfully on connection (0x%X)", service_name, conn_handle);
    }
    else
        NRF_LOG_ERROR("(%s): Notification error (err. code: %d)", service_name, err_code);
}

static void _send_notification(ble_gatts_hvx_params_t *data, uint16_t conn_handle, char *service_name)
{
    // send notification
    ret_code_t err_code = sd_ble_gatts_hvx(conn_handle, (const ble_gatts_hvx_params_t*) data);
    // check error code
    check_notification_error_code(err_code, conn_handle, service_name);
}

static void _send_notification_to_all(ble_gatts_hvx_params_t *data, char *service_name)
{
    ble_conn_state_conn_handle_list_t conn_handles = ble_conn_state_conn_handles();

    if (conn_handles.len == 0) return;

    for (int i = 0; i < conn_handles.len; i++)
    {
        uint16_t conn_handle = conn_handles.conn_handles[i];
        if (ble_conn_state_status(conn_handle) != BLE_CONN_STATUS_CONNECTED) continue;
        _send_notification(data, conn_handle, service_name);
    }
}

void ble_send_notification(uint16_t handle, uint16_t len, uint8_t *data, uint16_t conn_handle, char *service_name)
{
    ble_gatts_hvx_params_t hvx_params;

    // return if INVALID
    if (conn_handle == BLE_CONN_HANDLE_INVALID) return;

    memset(&hvx_params, 0, sizeof(hvx_params));
    hvx_params.handle = handle;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    hvx_params.p_len  = &len;
    hvx_params.p_data = data;  

    (conn_handle == BLE_CONN_HANDLE_ALL)? 
        _send_notification_to_all(&hvx_params, service_name) :
        _send_notification(&hvx_params, conn_handle, service_name);
}


void ble_set_char_value(uint16_t handle, uint16_t len, uint8_t *data, char *service_name)
{
    ret_code_t err_code;
    ble_gatts_value_t char_value;
    memset(&char_value, 0, sizeof(char_value));
    char_value.p_value = data;
    char_value.len     = len;

    err_code = sd_ble_gatts_value_set(BLE_CONN_HANDLE_INVALID, handle, &char_value);
    APP_ERROR_CHECK(err_code);

    if (err_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(%s): Error during characteristic (0x%02x) value update - 0x%08x", service_name, handle, err_code);    
}


void ble_get_char_value(uint16_t handle, uint16_t len, uint8_t *data)
{
    ret_code_t err_code;
    ble_gatts_value_t char_value; 
    memset(&char_value, 0, sizeof(char_value));
    char_value.p_value = data;
    char_value.len = len;

    err_code = sd_ble_gatts_value_get(BLE_CONN_HANDLE_INVALID, handle, &char_value);
    APP_ERROR_CHECK(err_code);
}