
#ifndef APP_BLE_ADVERTISING_H__
#define APP_BLE_ADVERTISING_H__

#include <stdbool.h>
#include "ble_gap.h"

void advertising_stop();

void advertising_start(bool use_whitelist, bool fast_mode);

void advertising_init(ble_uuid_t *p_adv_uuids, uint8_t adv_uuids_num);

#endif /* APP_BLE_ADVERTISING_H__ */
