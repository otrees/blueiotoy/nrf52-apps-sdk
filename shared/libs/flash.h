#ifndef FLASH_H__
#define FLASH_H__

#include <stdint.h>

#define APP_FLASH_START_ADDR			(0x70000)
#define APP_FLASH_END_ADDR				(0x71000) //4 kiB

#define FLASH_PROGRAM_UNIT				4

void flash_write(uint32_t addr, uint8_t *data, int len);
void flash_read(uint32_t addr, uint8_t *data, int len);
void flash_erase(uint32_t addr, int len);
void flash_init();

#endif /* FLASH_H__ */