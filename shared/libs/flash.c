#include "flash.h"

#include "nrf_fstorage.h"
#include "nrf_log.h"


nrf_fstorage_api_t 	nrf_fstorage_sd;

NRF_FSTORAGE_DEF(nrf_fstorage_t m_flash) =
{
    .evt_handler    = NULL,
    .start_addr     = APP_FLASH_START_ADDR,
    .end_addr       = APP_FLASH_END_ADDR,
};

void flash_erase(uint32_t addr, int len)
{
    nrf_fstorage_erase(&m_flash, addr, len, NULL);
}

void flash_write(uint32_t addr, uint8_t *data, int len)
{
    ret_code_t ret_code = nrf_fstorage_write(&m_flash, addr, data, len, NULL);

    if (ret_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(FLASH:) Write Error :%d", ret_code);
}

void flash_read(uint32_t addr, uint8_t *data, int len)
{
    ret_code_t ret_code = nrf_fstorage_read(&m_flash, addr, data, len);

    if (ret_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(FLASH:) Read Error :%d", ret_code);
}

void flash_init()
{
    nrf_fstorage_init(&m_flash, &nrf_fstorage_sd, NULL);
}