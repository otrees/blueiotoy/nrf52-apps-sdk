#ifndef APP_BUTTON_H__
#define APP_BUTTON_H__ 

#include <stdint.h>

#include "app_config.h"

#define MAX_NUM_BUTTONS         2                               /** currently suport of maximum 2 buttons */

/* #region  APP BUTTON EVENTS  */
typedef enum
{
    EVT_BTN_SHORT_PRESS,
    EVT_BTN_LONG_PRESS,
    EVT_BTN_VERY_LONG_PRESS,
    EVT_BTN_VERY_VERY_LONG_PRESS,
    EVT_BTN_DOUBLE_PRESS,
    EVT_BTN_PRESS_START,
    EVT_BTN_PRESSINTERVAL_PASSED,
    NUM_EVTS_BTN
} app_btn_evt_t;

// BUTTON PRESS DELAYS 
#ifndef BUTTON_DETECTION_DELAY_MS
#define BUTTON_DETECTION_DELAY_MS               45                     /** delay until a button is reported as pressed */
#endif 

#ifndef BUTTON_SHORT_PRESS_MS
#define BUTTON_SHORT_PRESS_MS          	        1000                   /** interval  45-1000 ms when button is reported as SHORT PRESS */
#endif

#ifndef BUTTON_LONG_PRESS_MS
#define BUTTON_LONG_PRESS_MS          	        3500                   /** interval  1000-3500 ms when button is reported as LONG PRESS */
#endif

#ifndef BUTTON_VERY_LONG_PRESS_MS
#define BUTTON_VERY_LONG_PRESS_MS               8000                  /** interval   3500-8000 ms when button is reported as VERY_LONG_PRESS */
#endif

#ifndef BUTTON_VERY_VERY_LONG_PRESS_MS
#define BUTTON_VERY_VERY_LONG_PRESS_MS          20000                  /** interval  8000-20000 ms when button is reported as VERY_LONG_PRESS  */
#endif


#define NUM_BTN_PRESS_EVENTS  4
#define BUTTON_EVENTS_LOOKUP_MS(name) name[NUM_BTN_PRESS_EVENTS] = { \
                                                BUTTON_SHORT_PRESS_MS, \
                                                BUTTON_LONG_PRESS_MS, \
                                                BUTTON_VERY_LONG_PRESS_MS, \
                                                BUTTON_VERY_VERY_LONG_PRESS_MS, \
                                                } \


/* #endregion  APP BUTTON EVENTS  */

typedef enum
{
    BUTTON_RELEASED,    //0
    BUTTON_PRESSED,     //1
} button_state_t;


typedef struct 
{
    uint8_t button_index;
    uint8_t button_pin;
    uint8_t button_event_passed;
} app_btn_evt_context_t;

typedef void (*app_btn_evt_handler_t)(app_btn_evt_t event, app_btn_evt_context_t *context);

typedef struct 
{
    uint8_t                 pin;
    button_state_t          state;
    uint8_t                 index;
    uint32_t                _scan_counter;
    uint32_t                _timer_count;
    app_btn_evt_handler_t   app_evt_handler;
} app_button_state_t;


typedef struct
{
    uint8_t button_pin;
    app_btn_evt_handler_t app_evt_handler;
} app_btn_init_t;


// void app_button_set_app_evt_handler(app_btn_evt_handler_t app_btn_evt_handler);
void app_button_init(app_btn_init_t *init);

void app_buttons_deinit();

#endif /* APP_BUTTON_H__ */
