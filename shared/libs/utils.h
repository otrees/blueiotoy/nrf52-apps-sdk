#ifndef UTILS_H__
#define UTILS_H__

#include <stdint.h>



uint32_t binArray2int(uint8_t *bin_array, int length);

uint32_t hex2int(uint8_t *hex_str, int length);

void hex_array_to_str(uint8_t *hex_array, uint8_t *str, int length);

void print_hex_array(uint8_t *array, uint32_t len);

// EVT to STR
const char* pm_evt_id_to_str(int evt);
const char* gap_evt_id_to_str(int evt);
const char* discovery_evt_id_to_str(int evt);

const char *app_btn_evt_to_str(int evt);
const char *app_ble_evt_to_str(int evt);
const char *app_battery_evt_to_str(int evt);
const char *app_rfid_evt_to_str(int evt);

#endif  /* UTILS_H__ */