#include "app_ble_advertising.h"

#include "nrf_log.h"
#include "nrf_error.h"

#include "ble_advdata.h"
#include "peer_manager.h"

#include "app_config.h"
#include "app_ble_config.h"

#include "app_ble.h" //TODO: CIRCULAR DEPENDENCY HERE

static uint8_t      m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;                   // advertising handle used to identify an advertising set

// ADV UUIDS
static ble_uuid_t   adv_uuids[ADVER_MAX_NUM_UUIDS];                                

// ADV DATA
static uint8_t      m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX];                    // buffer for storing an encoded advertising set
static uint8_t      m_enc_scan_response_data[BLE_GAP_ADV_SET_DATA_SIZE_MAX];         // buffer for storing an encoded scan data

static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = m_enc_scan_response_data,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX

    }
};

static void whitelist_get_info()
{
    ret_code_t err_code;
    ble_gap_addr_t whitelist_addrs[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
    ble_gap_irk_t  whitelist_irks[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
    uint32_t       addr_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
    uint32_t       irk_cnt  = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    // NOTE: return previously set whitelist
    err_code = pm_whitelist_get(whitelist_addrs, &addr_cnt,
                                whitelist_irks,  &irk_cnt);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_WARNING("(PM): pm_whitelist_get returns %d addr in whitelist and %d irk whitelist", addr_cnt, irk_cnt);

}

static void whitelist_unset()
{
    ret_code_t err_code = pm_whitelist_set(NULL, 0);
    APP_ERROR_CHECK(err_code);
}

static void whitelist_set(pm_peer_id_list_skip_t skip)
{
    pm_peer_id_t peer_ids[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
    uint32_t     peer_id_count = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    ret_code_t err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_INFO("(BLE): whitelist_peer_cnt: %d, MAX_PEERS_WLIST: %d", peer_id_count+1, BLE_GAP_WHITELIST_ADDR_MAX_COUNT);

    err_code = pm_whitelist_set(peer_ids, peer_id_count);
    APP_ERROR_CHECK(err_code);
}

static void whitelist_unset_identities()
{
    ret_code_t err_code = pm_device_identities_list_set(NULL, 0);
    APP_ERROR_CHECK(err_code);
}

static void whitelist_set_identities(pm_peer_id_list_skip_t skip)
{
    ret_code_t err_code;

    whitelist_get_info();

    pm_peer_id_t peer_ids[BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT];
    uint32_t     peer_id_count = BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT;

    // NOTE: retrieve all allowed peer ids
    err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    APP_ERROR_CHECK(err_code);

    // NOTE: peer_ids are added to the identities list
    err_code = pm_device_identities_list_set(peer_ids, peer_id_count);
    APP_ERROR_CHECK(err_code);
}


static void flags_set(ble_gap_adv_params_t *p_adv_params, uint8_t flags)
{
    uint8_t * p_flags = ble_advdata_parse(m_adv_data.adv_data.p_data, m_adv_data.adv_data.len, BLE_GAP_AD_TYPE_FLAGS);

    if (p_flags != NULL)
        *p_flags = flags;
}

static void advertising_set_advertising_slow(bool use_whitelist)
{
    ret_code_t err_code;

    ble_gap_adv_params_t adv_params;
    memset(&adv_params, 0, sizeof(adv_params));

    adv_params.primary_phy     = ADVER_PRIMARY_PHY;
    adv_params.duration        = ADVER_SLOW_DURATION;
    adv_params.interval        = ADVER_SLOW_INTERVAL;
    adv_params.properties.type = ADVER_TYPE;
    adv_params.p_peer_addr     = NULL;

    if (use_whitelist)
    {
        adv_params.filter_policy  = BLE_GAP_ADV_FP_FILTER_CONNREQ;
        // set correct flags
        flags_set(&adv_params, BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED);
    }
    else
    {
        adv_params.filter_policy   = BLE_GAP_ADV_FP_ANY;
        // set correct flags
        flags_set(&adv_params, ADVER_MODE);
    }

    err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &adv_params);
    APP_ERROR_CHECK(err_code);

}


void advertising_start(bool use_whitelist, bool fast_mode)
{
    NRF_LOG_INFO("(BLE-ADV): Starting advertising, with whitelist: %d", use_whitelist);
    ret_code_t err_code;

    if (use_whitelist)
    {
        // NOTE: add only peers with ID address
        whitelist_set(PM_PEER_ID_LIST_SKIP_NO_ID_ADDR);
        whitelist_set_identities(PM_PEER_ID_LIST_SKIP_NO_IRK);
    }
    //TODO: must be commented, caused fatal error on Microbit
    // else
    // {
    //     whitelist_unset();
    //     whitelist_unset_identities();
    // }

    if (fast_mode)
        advertising_set_advertising_slow(use_whitelist);
    else
        advertising_set_advertising_slow(use_whitelist);


    err_code = sd_ble_gap_adv_start(m_adv_handle, APP_CONN_CFG_TAG);
    if (err_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(BLE-ADV): ERROR - advertising start (err. code: %d)", err_code);
    // APP_ERROR_CHECK(err_code);
}

void advertising_stop(){

    ret_code_t err_code = sd_ble_gap_adv_stop(m_adv_handle);

    // NOTE: NRF_ERROR_INVALID_STATE occures often because it is stopped when did not started
    if (err_code == NRF_SUCCESS)
    {
        NRF_LOG_INFO("(BLE-ADV): Advertising stopped");
    }
    else if (err_code != NRF_ERROR_INVALID_STATE)
    {
        NRF_LOG_ERROR("(BLE): ERROR - advertising stop (err. code: %d)", err_code);
        APP_ERROR_CHECK(err_code);
    }
}


/**
 * Setting advertising data and scan response data 
 **/
static void advertising_set_adv_data(ble_uuid_t *p_adv_uuids, uint8_t adv_uuids_num)
{
    ret_code_t    err_code;
    ble_advdata_t advdata; // advertising data
    ble_advdata_t srdata;  // scan response data 

    memset(&advdata, 0, sizeof(advdata));
    
    // variable to hold manufacturer specific data
    ble_advdata_manuf_data_t manuf_data;
    uint8_t data[] = MANUFACTURER_NAME; 
    manuf_data.company_identifier = MANUFACTURER_IDENTIFIER; 
    manuf_data.data.p_data = data;
    manuf_data.data.size = sizeof(data);

    // build and set advertising data.
    advdata.p_manuf_specific_data = &manuf_data;
    advdata.name_type = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance = true;
    advdata.flags = ADVER_MODE;

    // copy all uuids to advertise to local variable
    memcpy(adv_uuids, p_adv_uuids, adv_uuids_num*sizeof(ble_uuid_t));

    // construct scan response data
    memset(&srdata, 0, sizeof(srdata));
    srdata.uuids_complete.uuid_cnt = adv_uuids_num;
    srdata.uuids_complete.p_uuids  = adv_uuids;

    err_code = ble_advdata_encode(&advdata, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
    APP_ERROR_CHECK(err_code);
    err_code = ble_advdata_encode(&srdata, m_adv_data.scan_rsp_data.p_data, &m_adv_data.scan_rsp_data.len);
    APP_ERROR_CHECK(err_code);
}

void advertising_init(ble_uuid_t *p_adv_uuids, uint8_t adv_uuids_num)
{
    // set appearance
    sd_ble_gap_appearance_set(ADVER_APPEARANCE); 

    // SET ADVERTISING DATA
    advertising_set_adv_data(p_adv_uuids, adv_uuids_num);

    // SET ADVERTISING PARAMETERS
    advertising_set_advertising_slow(false);
}
