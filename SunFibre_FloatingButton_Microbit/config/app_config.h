#ifndef APP_CONFIG_H__
#define APP_CONFIG_H__

#include <stdint.h>
#include "boards.h"
#include "board_ledmatrix.h"

#define MODEL_NUMBER                            "m.01"

//LED MATIX CONFIG
#define LED_MATRIX_CENTRAL_IDLE			          LED_MATRIX_I 
#define LED_MATRIX_CENTRAL_SCANNING	          LED_MATRIX_S
#define LED_MATRIX_CENTRAL_CONNECTED	        LED_MATRIX_C
#define LED_MATRIX_ERROR			                LED_MATRIX_E
#define LED_MATRIX_PERIPHERAL			            LED_MATRIX_P

// array of pointers
#define LED_MATRIX_LETTERS(name) name[NUM_APP_CENTRAL_STATES] = { \
                                                LED_MATRIX_CENTRAL_IDLE, \
                                                LED_MATRIX_CENTRAL_SCANNING, \
                                                LED_MATRIX_CENTRAL_CONNECTED, \
                                                LED_MATRIX_ERROR, \
                                                } \

// BUTTON CONFIG
#define USER_BUTTON_PIN            	        BUTTON_0_PIN            /**< Button that will trigger the notification event with the LED Button Service */
#define USER_BUTTON2_PIN            	      BUTTON_1_PIN            /**< Button that will trigger the notification event with the LED Button Service */

#define BUTTON_DETECTION_DELAY_MS               45                     /**< Delay from a until a button is reported as pushed */
#define BUTTON_SHORT_PRESS_MS          	        1000                   /**< Interval  50-1000 ms when button is reported as SHORT PRESS */  
#define BUTTON_LONG_PRESS_MS          	        3500                   /**< Interval  1000-3500 ms when button is reported as LONG PRESS */
#define BUTTON_VERY_LONG_PRESS_MS               8000                  /**< Interval   3500-8000 ms when button is reported as VERY_LONG_PRESS . */
#define BUTTON_VERY_VERY_LONG_PRESS_MS          20000                  /**< Interval  8000-15000 ms when button is reported as VERY_LONG_PRESS . */

// STATE TIMERS
#define POST_CONNECTED_MAX_DURATION                     APP_TIMER_TICKS(10000)
  //central
#define CENTRAL_SCANNING_MAX_DURATION                   APP_TIMER_TICKS(20000)              // go to IDLE after X s of no new action 
  //peripheral
#define PERIPHERAL_ADVERTISING_MAX_DURATION             APP_TIMER_TICKS(20000)               
#define PERIPHERAL_ADVER_CONNECTED_MAX_DURATION         POST_CONNECTED_MAX_DURATION    



// BLE NOTIFICATIONS
#define BLE_MONITOR_SERVICE_ON_INIT_DELAY   APP_TIMER_TICKS(1000) 
#define BLE_MONITOR_SERVICE_INTERVAL        APP_TIMER_TICKS(90000) 

#endif  /* APP_CONFIG_H__ */