#ifndef APP_BLE_CONFIG_H__
#define APP_BLE_CONFIG_H__

#define DEVICE_NAME                             "SunFibre-btn"                         
#define TARGET_DEVICE_NAME                      "SunFibre"                         
#define MANUFACTURER_NAME                       "FEL CVUT"
#define MANUFACTURER_IDENTIFIER                 0x8888                         

#define APP_BLE_OBSERVER_PRIO           3                      			/** application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_PM_OBSERVER_PRIO            4                                       /** application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_SOC_OBSERVER_PRIO       	1                                   	/** applications' SoC observer priority. You shouldn't need to modify this value. */

#define APP_ALLOW_REPAIRING             1
#define APP_CONN_CFG_TAG                1                                       /**< A tag identifying the SoftDevice BLE configuration. */

// BLE - CONNECTION PARAMETERS as peripheral
#define PER_CONN_MIN_INTERVAL               MSEC_TO_UNITS(250, UNIT_1_25_MS)         /**< Minimum acceptable connection interval (0.5 seconds). */
#define PER_CONN_MAX_INTERVAL               MSEC_TO_UNITS(500, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define PER_CONN_SLAVE_LATENCY              0                                        /**< Slave latency. */
#define PER_CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory time-out (4 seconds). */

// BLE - CONNECTION PARAMETERS as central 
#define CEN_CONN_MIN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)         /**< Minimum acceptable connection interval (0.5 seconds). */
#define CEN_CONN_MAX_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define CEN_CONN_SLAVE_LATENCY              0                                        /**< Slave latency. */
#define CEN_CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory time-out (4 seconds). */


// BLE - SCANNING PARAMETERS
// #define SCAN_INTERVAL                   MSEC_TO_UNITS(500, UNIT_0_625_MS)       //2.5 s                  
// #define SCAN_WINDOW                     MSEC_TO_UNITS(130, UNIT_0_625_MS)       //1.25 s                  
#define SCAN_INTERVAL                   MSEC_TO_UNITS(160, UNIT_0_625_MS)       //2.5 s                  
#define SCAN_WINDOW                     MSEC_TO_UNITS(80, UNIT_0_625_MS)       //1.25 s                 l
#define SCAN_FILTER_POLICY              BLE_GAP_SCAN_FP_ACCEPT_ALL              // Scan request is sent to all devices
#define SCAN_PRIMARY_PHY			    BLE_GAP_PHY_1MBPS
#define SCAN_TIMEOUT                    BLE_GAP_SCAN_TIMEOUT_UNLIMITED


// BLE - ADVERTISING PARAMETERS
#define ADVER_APPEARANCE							0x04C0 // category: Generic Control Device

#define ADVER_MODE										BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE //NOTE: discoverable all the time and BR/EDR not suppported
#define ADVER_TYPE										BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED
#define ADVER_PRIMARY_PHY			  			BLE_GAP_PHY_1MBPS


#define ADVER_FAST_INTERVAL           MSEC_TO_UNITS(100, UNIT_0_625_MS)     /**< Fast advertising interval (in units of 0.625 ms). */
#define ADVER_SLOW_INTERVAL           MSEC_TO_UNITS(1000, UNIT_0_625_MS)		/**< Slow advertising interval (in units of 0.625 ms). */

#define ADVER_FAST_DURATION           MSEC_TO_UNITS(3000, UNIT_10_MS)     // 3000 ms                        	/**< The advertising duration of fast advertising in units of 10 milliseconds. */
#define ADVER_SLOW_DURATION           BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED       /**< The advertising duration of slow advertising in units of 10 milliseconds. */

#define ADVER_NUM_UUIDS		        		1
#define ADVER_MAX_NUM_UUIDS		    		1



// BLE - SECURITY PARAMETERS
#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16


#endif  /* APP_BLE_CONFIG_H__ */