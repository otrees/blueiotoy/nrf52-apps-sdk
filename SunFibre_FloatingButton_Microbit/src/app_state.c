#include "app_state.h"

static const char *app_central_state_str[] = {
    "CENTRAL_IDLE",
    "CENTRAL_SCANNING",
    "CENTRAL_CONNECTED",
    "CENTRAL_ERROR"
};

static const char *app_peripheral_state_str[] = {
    "PERIPHERAL_IDLE",
    "PERIPHERAL_ADVERTISING",
    "PERIPHERAL_CONNECTED",
    "PERIPHERAL_ERROR"
};

static const char *app_error_str[] = {
    "BONDING_MISS_KEY",
    "BONDING_NO_SEC_REQ",     
    "CONNECTION_REPAIR",     
    "UNIDENTIFIED_ERROR",
};


const char *app_central_state_to_str(app_central_state_t state){
    return app_central_state_str[state];
}

const char *app_peripheral_state_to_str(app_peripheral_state_t state){
    return app_peripheral_state_str[state];
}

const char *app_error_to_str(app_error_t error){
    return app_error_str[error];
}