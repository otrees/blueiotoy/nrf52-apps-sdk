#include "businesslogic.h"
#include <stdint.h>

#include "nrf_log.h"
#include "nrf_delay.h"

#include "ble_conn_state.h"
#include "app_timer.h"

#include "app_config.h"
#include "app_ble_config.h"
#include "app_state.h"
#include "utils.h"

#include "board_ledmatrix.h"
#include "app_ble_services.h"


#define START_APP_TIMER_CEN(duration)   app_timer_start(m_app_timer_central_id, duration, NULL);
#define STOP_APP_TIMER_CEN()            app_timer_stop(m_app_timer_central_id);
#define START_APP_TIMER_PER(duration)   app_timer_start(m_app_timer_peripheral_id, duration, NULL);
#define STOP_APP_TIMER_PER()            app_timer_stop(m_app_timer_peripheral_id);

// TIMERS
APP_TIMER_DEF(m_app_timer_central_id);
APP_TIMER_DEF(m_app_timer_peripheral_id);

// APPLICATION
static app_central_state_t     app_central_state;
static app_peripheral_state_t  app_peripheral_state;

// CONNECTIONS
static conn_handle_t m_active_central_conn = BLE_CONN_HANDLE_INVALID;
static conn_handle_t m_incoming_central_conn = BLE_CONN_HANDLE_INVALID;
static conn_handle_t m_active_peripheral_conn = BLE_CONN_HANDLE_INVALID;
static conn_handle_t m_incoming_peripheral_conn = BLE_CONN_HANDLE_INVALID;

// HELPER
static uint8_t const *LED_MATRIX_LETTERS(led_matrix_letters);

static void extend_app_timer(app_timer_id_t app_timer_id, int extended_duration)
{
    app_timer_stop(app_timer_id);
    app_timer_start(app_timer_id, extended_duration, NULL);
    NRF_LOG_DEBUG("(APP): State timer extended.");
}

static void timers_create()
{
    app_timer_create(&m_app_timer_central_id, APP_TIMER_MODE_SINGLE_SHOT, (app_timer_timeout_handler_t) app_on_state_timer_central_evt);
    app_timer_create(&m_app_timer_peripheral_id, APP_TIMER_MODE_SINGLE_SHOT, (app_timer_timeout_handler_t) app_on_state_timer_peripheral_evt);
}

static void set_led_matrix()
{
    // show P if peripheral state active advertising/connected
    if (app_peripheral_state != PERIPHERAL_IDLE)
        LED_matrix_show(LED_MATRIX_PERIPHERAL);
    // show corresponding letter to central state 
    else
        LED_matrix_show(led_matrix_letters[app_central_state]);
}


void change_peripheral_light_mode()
{
    app_ble_services_set_p_dimled(0xFF); //NOTE: increment light mode
}

static void change_app_central_state(app_central_state_t new_state)
{
    // stop state timer
    STOP_APP_TIMER_CEN();

    // stop scanning
    ble_stop_scan();

    NRF_LOG_DEBUG("(APP-CEN): State tranzition (%s - %s)", app_central_state_to_str(app_central_state), app_central_state_to_str(new_state));
    NRF_LOG_RAW_INFO("\n");

    app_central_state = new_state;

    switch (app_central_state)
    {
        case CENTRAL_IDLE:
            m_active_central_conn = BLE_CONN_HANDLE_INVALID;
            m_incoming_central_conn = BLE_CONN_HANDLE_INVALID;
        break;

        case CENTRAL_SCANNING:
            m_active_central_conn = BLE_CONN_HANDLE_INVALID;
            m_incoming_central_conn = BLE_CONN_HANDLE_INVALID;
            NRF_LOG_INFO("(APP-CEN): START scanning");
            ble_start_scan();

            START_APP_TIMER_CEN(CENTRAL_SCANNING_MAX_DURATION);
        break;

        case CENTRAL_CONNECTED:
            m_active_central_conn = m_incoming_central_conn;
            m_incoming_central_conn = BLE_CONN_HANDLE_INVALID;
        break;


        default: break;
    }

    set_led_matrix(new_state);
}

static void change_app_central_state_error(app_error_t error_code)
{
    // stop state timer
    STOP_APP_TIMER_CEN();
    NRF_LOG_ERROR("(APP) error: %s!!!", app_error_to_str(error_code));

    app_central_state = CENTRAL_ERROR;

    switch (error_code)
    {
        case BONDING_MISS_KEY: //NOT_CONNECTED_ADVERTISING
            ble_disconnect(m_incoming_central_conn);
        break;

        case CONNECTION_REPAIR: //BONDING
            ble_disconnect(m_incoming_central_conn);
        break;

        case BONDING_NO_SEC_REQ:
        break;
        default: break;
    }

    set_led_matrix();
}

static void change_app_peripheral_state(app_peripheral_state_t new_state)
{
    STOP_APP_TIMER_PER();

    ble_stop_advertising();

    NRF_LOG_DEBUG("(APP-PER): State tranzition (%s - %s)", app_peripheral_state_to_str(app_peripheral_state), app_peripheral_state_to_str(new_state));
    NRF_LOG_RAW_INFO("\n");

    app_peripheral_state = new_state;


    switch (new_state)
    {
        case PERIPHERAL_ADVERTISING:
            m_incoming_peripheral_conn = BLE_CONN_HANDLE_INVALID;
            m_active_peripheral_conn = BLE_CONN_HANDLE_INVALID;

            NRF_LOG_INFO("(APP-PER): START advertising");
            ble_start_advertising_without_whitelist();
            START_APP_TIMER_PER(PERIPHERAL_ADVERTISING_MAX_DURATION);
        break;

        case PERIPHERAL_CONNECTED:
            m_incoming_peripheral_conn = BLE_CONN_HANDLE_INVALID;
            m_active_peripheral_conn = m_incoming_peripheral_conn;
        break;

        case PERIPHERAL_IDLE:
            m_incoming_peripheral_conn = BLE_CONN_HANDLE_INVALID;
            m_active_peripheral_conn = BLE_CONN_HANDLE_INVALID;
        break;
        default: break;
    }

    set_led_matrix(new_state);
}

/* #region APPLICATION HANDLERS PERIPHERAL */

void app_on_state_timer_peripheral_evt(app_statetimer_evt_t event)
{
    app_peripheral_state_t state = app_peripheral_state;
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_DEBUG("(APP-PER): Timer event: %s", "EVT_STATE_TIMER_ELAPSED");

    switch (state)
    {
        case PERIPHERAL_ADVERTISING:
            if (m_incoming_peripheral_conn != BLE_CONN_HANDLE_INVALID)
            {
                NRF_LOG_WARNING("(APP-PER): connect/add PEER - connected device (0x%x), security not started", m_incoming_peripheral_conn);
                ble_disconnect(m_incoming_peripheral_conn);
            }
            else
                NRF_LOG_INFO("(APP-PER): connect/add PEER - no connection");

            change_app_peripheral_state(PERIPHERAL_IDLE);
        break;
        default: break;
    }
}


void app_on_ble_peripheral_evt(app_ble_evt_t event, void* ble_context)
{
    app_peripheral_state_t state = app_peripheral_state;
    app_ble_evt_context_t *context = (app_ble_evt_context_t*) ble_context;
    conn_handle_t conn_handle = context->conn_handle;

    NRF_LOG_INFO("(APP-PER): BLE event: %s", app_ble_evt_to_str(event));
    switch (event)
    {
        case EVT_BLE_CONNECTED: 
        {
            // do not accept any further connection
            m_incoming_peripheral_conn = conn_handle;
            ble_stop_advertising();

            switch (state)
            {
                case PERIPHERAL_ADVERTISING:
                    extend_app_timer(m_app_timer_peripheral_id, PERIPHERAL_ADVER_CONNECTED_MAX_DURATION);
                    NRF_LOG_INFO("(APP-PER):  connect/add PEER - connection (0x%x)", conn_handle);
                break;

                default: NRF_LOG_ERROR("NO CONNECTION SHOULD HAPPED IN OTHER STATES (%s)", app_peripheral_state_to_str(state));
            }
        } break;

        case EVT_BLE_DISCONNECTED:
        {
            switch(state)
            {
                case PERIPHERAL_ADVERTISING:
                    ASSERT(conn_handle == m_incoming_peripheral_conn);
                    NRF_LOG_INFO("(APP): connect/add PEER - disconnected (0x%X)", conn_handle);
                    change_app_peripheral_state(PERIPHERAL_IDLE);
                break;

                case PERIPHERAL_CONNECTED:
                    NRF_LOG_INFO("(APP-PER): disconnected connection (0x%x)", conn_handle);
                    change_app_peripheral_state(PERIPHERAL_IDLE);
                break;

                //!!! ERROR case !!!
                case PERIPHERAL_IDLE:
                    NRF_LOG_WARNING("(APP-PER): disconnected connection (0x%x) in ERROR phase", conn_handle);
                break;
                default: break;
            }
        }
        break;

        case EVT_BLE_CONNECTED_AND_BONDED:
            switch(state)
            {
                case PERIPHERAL_ADVERTISING:
                    NRF_LOG_INFO("(APP-PER): connect/add PEER - security ok (0x%x)", conn_handle);
                    change_app_peripheral_state(PERIPHERAL_CONNECTED);
                break;
                default: break;
            }
        break;

        case EVT_BLE_SEC_ERROR:
        {
            uint16_t error_code = context->error_code;
            NRF_LOG_ERROR("(APP-PER) connect/add PEER - security ERROR: %s!!!", app_error_to_str(error_code));

            //!!! ERROR case !!!
            // NOTE: only BONDING MISS KEY should appear when repairing is allowed
            // CASE: BONDING_MISS_KEY 
            // flow:  --fill--
            // CASE: no bonding key on peripheral (here), bonding request from central (phone)
            ASSERT(!APP_ALLOW_REPAIRING || error_code == BONDING_MISS_KEY);
            ble_disconnect(m_incoming_peripheral_conn);
            change_app_peripheral_state(PERIPHERAL_IDLE);
        } break;

        default: break;
    }
}

void app_on_btn_peripheral_evt(app_btn_evt_t event, void* btn_context)
{
    NRF_LOG_DEBUG("(APP) btn right event: %s", app_btn_evt_to_str(event));
    
    app_peripheral_state_t state = app_peripheral_state;

    switch (event)
    {
        case EVT_BTN_VERY_LONG_PRESS:
        {
            switch (state)
            {
                case PERIPHERAL_IDLE:
                    change_app_peripheral_state(PERIPHERAL_ADVERTISING);
                break;

                case PERIPHERAL_ADVERTISING:{
                    // if previously connected device (no security started yet)
                    if(m_incoming_peripheral_conn) ble_disconnect(m_incoming_peripheral_conn);
                    change_app_peripheral_state(PERIPHERAL_IDLE);

                } break;
                case PERIPHERAL_CONNECTED:
                    ble_disconnect(m_active_peripheral_conn);
                    change_app_peripheral_state(PERIPHERAL_IDLE);
                break;
                default: break;
            } 
        } break;

        case EVT_BTN_VERY_VERY_LONG_PRESS:
            NVIC_SystemReset();
        break;
        default: break;
    }
}
/* #endregion APPLICATION HANDLERS PERIPHERAL */


/* #region APPLICATION HANDLERS CENTRAL */
void app_on_state_timer_central_evt(app_statetimer_evt_t event)
{
    app_central_state_t state = app_central_state;
    NRF_LOG_RAW_INFO("\r\n");
    NRF_LOG_DEBUG("(APP-CEN): Timer event: %s", "EVT_STATE_TIMER_ELAPSED");

    switch (state)
    {
        case CENTRAL_SCANNING:
            if (m_incoming_central_conn != BLE_CONN_HANDLE_INVALID)
            {
                NRF_LOG_WARNING("(APP-CEN): connect/add PEER - connected device (0x%x), security not started", m_incoming_central_conn);
                ble_disconnect(m_incoming_central_conn);
            }
            else
                NRF_LOG_INFO("(APP-CEN): connect/add PEER - no connection");

            change_app_central_state(CENTRAL_IDLE);
        break;

        default: break;
    }
}

void app_on_ble_central_evt(app_ble_evt_t event, void* ble_context)
{
    app_central_state_t state = app_central_state;
    app_ble_evt_context_t *context = (app_ble_evt_context_t*) ble_context;
    conn_handle_t conn_handle = context->conn_handle;

    NRF_LOG_INFO("(APP-CEN): BLE event: %s", app_ble_evt_to_str(event));

    switch (event)
    {
        case EVT_BLE_CONNECTED:
        {
            switch (state)
            {
                case CENTRAL_SCANNING:
                    NRF_LOG_INFO("(APP-CEN): connect/add PEER - connection (0x%x)", conn_handle);
                    m_incoming_central_conn = conn_handle;
                break;

                default: NRF_LOG_ERROR("NO CONNECTION SHOULD HAPPED IN OTHER STATES (%s)", app_central_state_to_str(state));
            }
        } break;

        case EVT_BLE_CONNECTED_AND_BONDED:
        {
            switch(state)
            {
                case CENTRAL_SCANNING:
                    NRF_LOG_INFO("(APP-CEN): connect/add PEER - security ok (0x%x)", conn_handle);
                    change_app_central_state(CENTRAL_CONNECTED); 
                break;

                default: NRF_LOG_ERROR("NO CONN/BOND SHOULD HAPPED IN OTHER STATES (%s)", app_central_state_to_str(state));
            }
        } break;


        case EVT_BLE_DISCONNECTED:
        {
            switch(state)
            {
                // NOTE: after disconnect all buttton action
                case CENTRAL_IDLE: 
                    NRF_LOG_INFO("(APP-CEN): disconnected connection (0x%x)", conn_handle);
                break;

                // NOTE: client disconnet without any action
                case CENTRAL_CONNECTED: 
                    NRF_LOG_INFO("(APP-CEN): disconnected connection (0x%x)", conn_handle);
                    change_app_central_state(CENTRAL_SCANNING);
                break;
                // !!! Error case !!!: disconnection after error: BONDING_MISS_KEY 
                // !!! Error case !!!: disconnection after error: CONN_REPAIR 
                case CENTRAL_ERROR:
                    NRF_LOG_INFO("(APP-CEN): disconnected connection in ERROR state (0x%x)", conn_handle);
                break;

                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)state);
            } 

        } break;

        case EVT_BLE_BONDING_REMOVED:
        {
            NRF_LOG_INFO("(APP): All bonds has been removed!");
            change_app_central_state(CENTRAL_IDLE);
        } break;

        case EVT_BLE_SEC_ERROR:
        {
            uint16_t error_code = context->error_code;

            switch(state)
            {
                // !!! SECURITY ERROR during bonding !!!
                // CASE01: CONN_REPAIR
                // flow: SECURITY_STARTED -> CONNECTED -> SECURITY_FAILED -> DISCONNECTED (automatically)

                // CASE02: BONDING_MISS_KEY 
                // flow: ALREADY_PAIRED -> SECURITY_STARTED -> CONNECTED -> SECURITY_FAILED -> DISCONNECTED (automatically)
                case CENTRAL_SCANNING:
                    change_app_central_state_error(error_code);
                break;
                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)state);
            } 

        } break;

        default: break;
    }
    NRF_LOG_RAW_INFO("\r\n");
}

void app_on_btn_central_evt(app_btn_evt_t event, void* btn_context)
{
    NRF_LOG_DEBUG("(APP) btn left event: %s", app_btn_evt_to_str(event));
    
    app_central_state_t state = app_central_state;

    switch (event)
    {
        case EVT_BTN_SHORT_PRESS:
        {
            switch (state)
            {
                case CENTRAL_ERROR:
                    change_app_central_state(CENTRAL_IDLE);
                break;

                case CENTRAL_IDLE: {
                    uint32_t num_peers = ble_get_peers_count();
                    NRF_LOG_DEBUG("(APP): num of peers %d", num_peers);
                    change_app_central_state(CENTRAL_SCANNING);
                } break;

                case CENTRAL_SCANNING:
                    extend_app_timer(m_app_timer_central_id, CENTRAL_SCANNING_MAX_DURATION);
                break;
                
                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)state);
            }
        } break;

        case EVT_BTN_LONG_PRESS: 
        {
            if (state == CENTRAL_CONNECTED)
                change_peripheral_light_mode();
        } break;

        case EVT_BTN_VERY_LONG_PRESS:
        {
            switch (state)
            {
                // NOTE: disconnect all connected devices
                case CENTRAL_CONNECTED:
                    change_app_central_state(CENTRAL_IDLE);
                    ble_disconnect_all();
                break;
                
                default: NRF_LOG_WARNING("Non-defined event-state (%d, %d)", (int)event, (int)state);
            } 
        } break;

        case EVT_BTN_VERY_VERY_LONG_PRESS:
        {
            switch (state)
            {
                case CENTRAL_IDLE:
                case CENTRAL_SCANNING:
                case CENTRAL_CONNECTED:
                    // stop previous state timer
                    STOP_APP_TIMER_CEN();

                    // diconnect 
                    ble_disconnect_all();

                    // delete bonds
                    ble_delete_bonds();
                break;
                default: break;
            }
        } break;
        default: break;
    }
}
/* #endregion APPLICATION HANDLERS CENTRAL */

void businesslogic_init()
{
    timers_create();
    ble_conn_state_init();

    // init app button
    app_btn_init_t left_button_init;
    left_button_init.button_pin = USER_BUTTON_PIN;
    left_button_init.app_evt_handler = (app_btn_evt_handler_t) &app_on_btn_central_evt;
    app_button_init(&left_button_init);
    NRF_LOG_INFO("(APP): Left button initialized");

    app_btn_init_t right_button_init;
    right_button_init.button_pin = USER_BUTTON2_PIN;
    right_button_init.app_evt_handler = (app_btn_evt_handler_t) &app_on_btn_peripheral_evt;
    app_button_init(&right_button_init);
    NRF_LOG_INFO("(APP): Right button initialized");

    // init BLE
    app_ble_init_t init;
    init.app_central_evt_handler = (app_ble_evt_handler_t) &app_on_ble_central_evt; 
    init.app_peripheral_evt_handler = (app_ble_evt_handler_t) &app_on_ble_peripheral_evt; 
    ble_init(&init);
    NRF_LOG_INFO("BLE initialized");

    // init battery
    // battery_init();
    // battery_set_app_evt_handler((app_battery_evt_handler_t) &app_on_battery_evt);

    change_app_central_state(CENTRAL_IDLE);
}