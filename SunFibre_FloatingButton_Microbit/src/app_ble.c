#include "app_ble.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "nrf_sdh.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_scan.h"
#include "nrf_ble_gq.h"
#include "nrf_log.h"
#include "nrf_fstorage.h"

#include "ble_conn_state.h"
#include "ble_db_discovery.h"

#include "fds.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"

#include "app_config.h"
#include "app_ble_config.h"
#include "app_state.h"

#include "app_ble_services.h"
#include "app_ble_advertising.h"
#include "utils.h"


// pointer to application BLE event handler
static app_ble_evt_handler_t app_central_evt_handler;
static app_ble_evt_handler_t app_peripheral_evt_handler;

static bool     m_memory_access_in_progress;                        /**< Flag to keep track of ongoing operations on persistent memory. */

static ble_gatt_db_srv_t m_peer_srv_buf[BLE_SERVICES_NUM_DISCOVERED] = {0};       /** array of services with room to store  */

NRF_BLE_GQ_DEF(m_gatt_queue, NRF_SDH_BLE_CENTRAL_LINK_COUNT, NRF_BLE_GQ_QUEUE_SIZE);
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                           /**< Scanning module instance. */

BLE_DB_DISCOVERY_DEF(m_db_disc);                                    /**< DB discovery module instance. */

/* #region  GETTERS */
nrf_ble_gq_t* get_gatt_queue()
{
    return &m_gatt_queue;
}

/* #endregion  GETTERS */

static void _on_security_and_discovery_done(conn_handle_t conn_handle)
{
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = conn_handle;

    // call app event handler
    app_central_evt_handler(EVT_BLE_CONNECTED_AND_BONDED, (void*)app_ble_evt_context);

    free(app_ble_evt_context);
}


/* #region DB DISCOVERY */
static void _on_db_discoverycomplete_evt(ble_db_discovery_evt_t *p_evt)
{
    // store peer info to flash
    ret_code_t err_code;
    pm_peer_id_t peer_id;
    err_code = pm_peer_id_get(p_evt->conn_handle, &peer_id);
    APP_ERROR_CHECK(err_code);

    //!!! DO NOT CHANGE sizeof(m_peer_srv_buf), sizeof(ble_gatt_srv_char_t)
    // copy to permanent buffer
    memcpy(m_peer_srv_buf, &(p_evt->params.discovered_db), sizeof(m_peer_srv_buf));
    err_code = pm_peer_data_remote_db_store(peer_id, m_peer_srv_buf, ALIGN_NUM(4, sizeof(m_peer_srv_buf)), NULL);
    NRF_LOG_INFO("(BLE-DISC): peer service&characteristics info saved to flash, peerid: %d, error: %d", peer_id, err_code);
    if (err_code == NRF_ERROR_STORAGE_FULL){
        err_code = fds_gc();
        NRF_LOG_ERROR("(BLE-DISC): Garbage collector...");
    } 
    APP_ERROR_CHECK(err_code);

    // call app services handler
    on_ble_db_disc_evt(p_evt);
}

static void on_db_discovery_evt(ble_db_discovery_evt_t *p_evt)
{
    NRF_LOG_INFO("(BLE-DISC): on evt %s (%d)", discovery_evt_id_to_str(p_evt->evt_type), p_evt->evt_type);

    switch(p_evt->evt_type)
    {
        case (BLE_DB_DISCOVERY_COMPLETE):
            _on_db_discoverycomplete_evt(p_evt);
            _on_security_and_discovery_done(p_evt->conn_handle);
        break;

        case(BLE_DB_DISCOVERY_ERROR):
            NRF_LOG_INFO("(BLE-DISC): ERROR: %d", p_evt->params.err_code);
        break;

        default: break;
    }
}

static void db_discovery_init(void)
{
    ble_db_discovery_init_t db_init;
    memset(&db_init, 0, sizeof(ble_db_discovery_init_t));
    db_init.evt_handler  = on_db_discovery_evt;
    db_init.p_gatt_queue = &m_gatt_queue;

    ret_code_t err_code = ble_db_discovery_init(&db_init);
    APP_ERROR_CHECK(err_code);
}
/* #endregion db discovery */

/* #region SCAN */
static void scan_start(void)
{
    ret_code_t err_code;

    if (nrf_fstorage_is_busy(NULL))
    {
        m_memory_access_in_progress = true;
        return;
    }

    NRF_LOG_INFO("(BLE): Starting scan.");

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);
}

static void scan_stop(void)
{
    NRF_LOG_INFO("(BLE): Stopping scan.");
    nrf_ble_scan_stop();
}


static void on_scan_evt(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;
    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
            err_code = p_scan_evt->params.connecting_err.err_code;
            NRF_LOG_ERROR("(BLE-SCAN): Error: %d", err_code);
            APP_ERROR_CHECK(err_code);
        break;

        case NRF_BLE_SCAN_EVT_FILTER_MATCH:
            NRF_LOG_INFO("(BLE-SCAN): Filter match: %d", p_scan_evt->scan_evt_id);
            break;
        case NRF_BLE_SCAN_EVT_WHITELIST_ADV_REPORT: break;

        case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
            NRF_LOG_ERROR("This should never happen!!!");
        break;

        default: break;
    }
}


static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    ble_gap_conn_params_t const conn_param = 
    {
        .slave_latency      = CEN_CONN_SLAVE_LATENCY,
        .min_conn_interval  = CEN_CONN_MIN_INTERVAL,
        .max_conn_interval  = CEN_CONN_MAX_INTERVAL,
        .conn_sup_timeout   = CEN_CONN_SUP_TIMEOUT
    };

    ble_gap_scan_params_t const scan_param =
    {
        .active        = 0x01,
        .interval      = SCAN_INTERVAL,
        .window        = SCAN_WINDOW,
        .filter_policy = SCAN_FILTER_POLICY, 
        .timeout       = SCAN_TIMEOUT,
        .scan_phys     = SCAN_PRIMARY_PHY,
    };


    memset(&init_scan, 0, sizeof(nrf_ble_scan_init_t));
    init_scan.p_scan_param = &scan_param;
    init_scan.p_conn_param = &conn_param;

    init_scan.connect_if_match = true;
    init_scan.conn_cfg_tag     = APP_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, on_scan_evt);
    APP_ERROR_CHECK(err_code);

    // Setting filters for scanning.
    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_NAME_FILTER, false);
    APP_ERROR_CHECK(err_code);

    if (strlen(TARGET_DEVICE_NAME) != 0)
    {
        err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, TARGET_DEVICE_NAME);
        APP_ERROR_CHECK(err_code);
    }    
}

/* #endregion SCAN */

/* #region GATT */
static void on_gatt_evt(nrf_ble_gatt_t *p_gatt, nrf_ble_gatt_evt_t const *p_evt)
{
    switch (p_evt->evt_id)
    {
        case NRF_BLE_GATT_EVT_ATT_MTU_UPDATED:
            NRF_LOG_INFO("(BLE): GATT ATT MTU on connection 0x%x changed to %d", p_evt->conn_handle, p_evt->params.att_mtu_effective);
        break;

        case NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED:
            NRF_LOG_INFO("(BLE): GATT Data length on connection 0x%x changed to %d", p_evt->conn_handle, p_evt->params.data_length);
        break;
        default: break;
    }
}

/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, on_gatt_evt);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */


/* #region  GAP  */
static void _on_ble_connected_evt(ble_gap_evt_t const *p_gap_evt)
{
    uint8_t role = ble_conn_state_role(p_gap_evt->conn_handle);

    uint32_t central_link_cnt = ble_conn_state_central_conn_count();
    uint32_t peripheral_link_cnt = ble_conn_state_peripheral_conn_count();
    NRF_LOG_INFO("(BLE): Connected - link: 0x%x, role: %d, total: %d/%d", p_gap_evt->conn_handle, role, peripheral_link_cnt, central_link_cnt);

    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_gap_evt->conn_handle;

    // run app event handler
    app_ble_evt_handler_t app_evt_handler = (role == BLE_GAP_ROLE_CENTRAL)? app_central_evt_handler : app_peripheral_evt_handler;
    app_evt_handler(EVT_BLE_CONNECTED, (void*)app_ble_evt_context);

    free(app_ble_evt_context);
}

// static void _on_ble_connected_evt_deprecated(ble_gap_evt_t const *p_gap_evt)
// {
//     ret_code_t err_code;
//     pm_conn_sec_status_t status;
//     uint32_t    central_link_cnt = ble_conn_state_central_conn_count();

//     NRF_LOG_INFO("(BLE): Connected - link 0x%x / total - %d.", p_gap_evt->conn_handle, central_link_cnt);
//     err_code = pm_conn_sec_status_get(p_gap_evt->conn_handle, &status);

//     if (!status.bonded)
//     {
//         NRF_LOG_DEBUG("Starting discovery...");
//         err_code = ble_db_discovery_start(&m_db_disc, p_gap_evt->conn_handle);
//         NRF_LOG_INFO("Discovery error: %d", err_code);
//         // APP_ERROR_CHECK(err_code);

//         NRF_LOG_DEBUG("Starting security...");
//         err_code = pm_conn_secure(p_gap_evt->conn_handle, false);
//         NRF_LOG_INFO("PM sec error: %d", err_code);
//         // APP_ERROR_CHECK(err_code);
//     }

//     app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
//     app_ble_evt_context->conn_handle = p_gap_evt->conn_handle;

//     // run app event handler
//     if (app_evt_handler) app_evt_handler(EVT_BLE_CONNECTED, (void*)app_ble_evt_context);
//     free(app_ble_evt_context);
// }

static void _on_ble_disconnected_evt(ble_gap_evt_t const *p_gap_evt)
{
    uint8_t role = ble_conn_state_role(p_gap_evt->conn_handle);
    uint32_t periph_link_cnt = ble_conn_state_peripheral_conn_count();
    uint32_t central_link_cnt = ble_conn_state_central_conn_count();
    uint16_t disconnect_reason = p_gap_evt->params.disconnected.reason;

    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_gap_evt->conn_handle;
    app_ble_evt_context->error_code = disconnect_reason;

    NRF_LOG_INFO("(BLE): Disconnected - link: 0x%x, total: %d/%d, role: %d, reason: %d",
                 p_gap_evt->conn_handle, periph_link_cnt, central_link_cnt, role, disconnect_reason);

    // run app event handler
    app_ble_evt_handler_t app_evt_handler = (role == BLE_GAP_ROLE_CENTRAL)? app_central_evt_handler : app_peripheral_evt_handler;
    app_evt_handler(EVT_BLE_DISCONNECTED, (void*)app_ble_evt_context);

    free(app_ble_evt_context);
}


static void on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context)
{
    ret_code_t err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    // starts security if BLE_GAP_CONNECTED
    pm_handler_secure_on_connection(p_ble_evt);

    // NRF_LOG_INFO("(BLE): on evt %s (%d)", gap_evt_id_to_str(p_ble_evt->header.evt_id), p_ble_evt->header.evt_id);

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
        {
            NRF_LOG_INFO("(BLE): CONNECTED");
            _on_ble_connected_evt(p_gap_evt);
        } break;

        case BLE_GAP_EVT_DISCONNECTED:
        {
            NRF_LOG_INFO("(BLE): DISCONNECTED, reason 0x%x.", p_gap_evt->params.disconnected.reason);
            _on_ble_disconnected_evt(p_gap_evt);
        } break;

        case BLE_GAP_EVT_CONN_SEC_UPDATE:
        break;

        case BLE_GAP_EVT_TIMEOUT:
        {
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
                NRF_LOG_INFO("Connection Request timed out.");
        } break;

        case BLE_GAP_SEC_STATUS_TIMEOUT:
            NRF_LOG_ERROR("SEC");
        break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
    
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SEC_PARAMS_REQUEST");
            break;

        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_INFO("BLE_GAP_EVT_AUTH_KEY_REQUEST");
            break;

        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_INFO("BLE_GAP_EVT_LESC_DHKEY_REQUEST");
            break;

         case BLE_GAP_EVT_AUTH_STATUS:
             NRF_LOG_INFO("BLE_GAP_EVT_AUTH_STATUS: status=0x%x bond=0x%x lv4: %d kdist_own:0x%x kdist_peer:0x%x",
                          p_ble_evt->evt.gap_evt.params.auth_status.auth_status,
                          p_ble_evt->evt.gap_evt.params.auth_status.bonded,
                          p_ble_evt->evt.gap_evt.params.auth_status.sm1_levels.lv4,
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_own),
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_peer));
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    // BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = PER_CONN_MIN_INTERVAL;
    gap_conn_params.max_conn_interval = PER_CONN_MAX_INTERVAL;
    gap_conn_params.slave_latency     = PER_CONN_SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = PER_CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */


/* #region  PEER MANAGER */
static void _on_pm_central_connsecsucceded_evt(pm_evt_t const *p_evt, bool *peer_db_discovered)
{
    if (p_evt->peer_id == PM_PEER_ID_INVALID) return;

    ret_code_t err_code;
    // load peer data from flash    
    uint32_t data_len = sizeof(m_peer_srv_buf);
    err_code = pm_peer_data_remote_db_load(p_evt->peer_id, m_peer_srv_buf, &data_len);
    // NRF_LOG_INFO("Loading info about peer %d", p_evt->peer_id);
    // NRF_LOG_INFO("?????? %d x %d", sizeof(m_peer_srv_buf), sizeof(ble_gatt_db_srv_t));

    if (err_code == NRF_ERROR_NOT_FOUND)
    {
        NRF_LOG_DEBUG("(BLE-PM): Could not find the remote database in flash.");
        NRF_LOG_DEBUG("(BLE-PM): Starting discovery...");
        err_code = ble_db_discovery_start(&m_db_disc, p_evt->conn_handle);
        APP_ERROR_CHECK(err_code);
        *peer_db_discovered = false;
    }
    else
    {
        ASSERT(data_len == sizeof(m_peer_srv_buf));
        // APP_ERROR_CHECK(err_code);
        NRF_LOG_INFO("(BLE-PM): Remote Database loaded from flash.");

        // create programatically discovery event
        ble_db_discovery_evt_t db_evt = 
        {
            .evt_type = BLE_DB_DISCOVERY_COMPLETE,
            .conn_handle = p_evt->conn_handle,
            .params.discovered_db = m_peer_srv_buf[0]

        };
        on_ble_db_disc_evt(&db_evt);
        *peer_db_discovered = true;
    }
}

static void _on_pm_peripheral_connsecsucceded_evt(pm_evt_t const *p_evt)
{
    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_evt->conn_handle;

    // call app event handler
    app_peripheral_evt_handler(EVT_BLE_CONNECTED_AND_BONDED, (void*)app_ble_evt_context);

    free(app_ble_evt_context);
}

static void _on_pm_connsecsucceded_evt(pm_evt_t const *p_evt)
{

    // NRF_LOG_DEBUG("Connection secured. Role: %d. conn_handle: %d, Procedure: %d",
    //              ble_conn_state_role(p_evt->conn_handle),
    //              p_evt->conn_handle,
    //              p_evt->params.conn_sec_succeeded.procedure);

    uint8_t role = ble_conn_state_role(p_evt->conn_handle);
    if (role == BLE_GAP_ROLE_CENTRAL)
    {
        bool peer_db_discovered;
        _on_pm_central_connsecsucceded_evt(p_evt, &peer_db_discovered);
        if (peer_db_discovered)
            _on_security_and_discovery_done(p_evt->conn_handle);
    }
    else
        _on_pm_peripheral_connsecsucceded_evt(p_evt);
}

static void _on_pm_securityfailed_evt(pm_evt_t const *p_evt)
{
    uint16_t error_code;
    uint16_t security_error = p_evt->params.conn_sec_failed.error;
    uint8_t role = ble_conn_state_role(p_evt->conn_handle);

    switch (security_error)
    {
        case PM_CONN_SEC_ERROR_DISCONNECT:          // central is disconnected at peripheral side if BONDING_MISS_KEY
        case PM_CONN_SEC_ERROR_PIN_OR_KEY_MISSING:
            error_code = BONDING_MISS_KEY;
        break;
        case BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP:
            error_code = CONNECTION_REPAIR;
        break;
        default:
            error_code = UNIDENTIFIED_ERROR;
    }
    NRF_LOG_INFO("(BLE-PM): Security Failed - link: 0x%x, role: %d, reason: %d (0x%x)/%d", p_evt->conn_handle, role, security_error, security_error, error_code);

    app_ble_evt_context_t *app_ble_evt_context = (app_ble_evt_context_t *) malloc(sizeof(app_ble_evt_context_t));
    app_ble_evt_context->conn_handle = p_evt->conn_handle;
    app_ble_evt_context->error_code = error_code;

    // run app event handler
    app_ble_evt_handler_t app_evt_handler = (role == BLE_GAP_ROLE_CENTRAL)? app_central_evt_handler : app_peripheral_evt_handler;
    app_evt_handler(EVT_BLE_SEC_ERROR, (void*)app_ble_evt_context);

    free(app_ble_evt_context);
}


static void _on_pm_bondsdeleted_evt(pm_evt_t const *p_evt)
{
    app_central_evt_handler(EVT_BLE_BONDING_REMOVED, NULL);
}

static void _on_pm_securityconfigreq_evt(pm_evt_t const *p_evt)
{
    pm_conn_sec_config_t conn_sec_config = {.allow_repairing = APP_ALLOW_REPAIRING};
    pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
}



static void on_pm_evt(pm_evt_t const *p_evt)
{
    // standard function for maintaining room in flash based on Peer Manager events.
    pm_handler_flash_clean(p_evt);

    NRF_LOG_INFO("(BLE-PM): on evt %s (%d)", pm_evt_id_to_str(p_evt->evt_id), p_evt->evt_id);

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
            NRF_LOG_INFO("(BLE-PM): ALREADY PAIRED");
            // _on_pm_bondedpeerconnected_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_START:
            NRF_LOG_INFO("(BLE-PM): SECURITY STARTED");
        break;


        case PM_EVT_CONN_SEC_SUCCEEDED:
            NRF_LOG_INFO("(BLE-PM): BONDED");
            _on_pm_connsecsucceded_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_FAILED:
            NRF_LOG_INFO("(BLE-PM): SECURITY FAILED");
            _on_pm_securityfailed_evt(p_evt);
        break;

        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            NRF_LOG_INFO("(BLE-PM): PEERS DELETED.");
            _on_pm_bondsdeleted_evt(p_evt);
        break;

        case PM_EVT_CONN_SEC_CONFIG_REQ:
            NRF_LOG_INFO("(BLE-PM): SECURITY CONFIG REQ..");
            _on_pm_securityconfigreq_evt(p_evt);
        break;

        // @NOT HANDLED
        case PM_EVT_PEER_DATA_UPDATE_FAILED:
            NRF_LOG_ERROR("(BLE): PEER DATA UPDATE (code: %d)",  p_evt->params.peer_data_update_failed.error);
        break;

        // @NOT HANDLED
        case PM_EVT_PEERS_DELETE_FAILED:
            NRF_LOG_ERROR("(BLE): PEERS DELETE (err. code: %d)", p_evt->params.peers_delete_failed_evt.error);
        break;

        // @NOT HANDLED
        case PM_EVT_ERROR_UNEXPECTED:
            NRF_LOG_ERROR("(BLE-PM): UNEXPECTED ERROR (err. code: %d)", p_evt->params.error_unexpected.error);
        break;

        default: break;
    }
}

static void peer_manager_init()
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    // Security parameters to be used for all security procedures.
    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);
    err_code = pm_register(on_pm_evt);
    APP_ERROR_CHECK(err_code);
}
/* #endregion  */

static void ble_stack_init()
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, on_ble_evt, NULL);
}


/* #region  PUBLIC */
void ble_disconnect(conn_handle_t conn_handle)
{
    ble_conn_state_status_t conn_status = ble_conn_state_status(conn_handle);
    if (conn_status == BLE_CONN_STATUS_CONNECTED)
    {
        ret_code_t err_code = sd_ble_gap_disconnect(conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        if (err_code != NRF_SUCCESS)
            NRF_LOG_ERROR("(BLE): CMD - disconnect (err. code: %d)", err_code);
    }
    else
        NRF_LOG_WARNING("(BLE): Connection handle %d has already been disconnected!", conn_handle);
}

static void _disconnect(conn_handle_t conn_handle, void *p_context)
{
    ble_disconnect(conn_handle);
}

void ble_disconnect_all()
{
    uint32_t conn_count = ble_conn_state_for_each_connected(_disconnect, NULL);
    NRF_LOG_INFO("(BLE-S): Disconnected %d links.", conn_count);
}

void ble_start_scan()
{
    scan_start();
}

void ble_stop_scan()
{
    scan_stop();
}

void ble_start_advertising_without_whitelist()
{
    advertising_start(false, false);
}

void ble_stop_advertising()
{
    advertising_stop();
}


void ble_delete_bonds()
{
    ret_code_t err_code = pm_peers_delete();
    if (err_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(BLE): CMD - delete bonds (err. code: %d)", err_code);
    APP_ERROR_CHECK(err_code);
}

uint32_t ble_get_peers_count()
{
    return pm_peer_count();
}

void ble_init(app_ble_init_t *init)
{
    app_central_evt_handler = init->app_central_evt_handler;
    app_peripheral_evt_handler = init->app_peripheral_evt_handler;

    ble_stack_init();
    gap_params_init(); 
    gatt_init();

    peer_manager_init();

    db_discovery_init();

    scan_init();

    // init BLE services
    app_ble_services_init_t s_init = {.p_gatt_queue = &m_gatt_queue};
    app_ble_services_init(&s_init);

    //init advertising
    ble_uuid_t   adv_uuids[ADVER_MAX_NUM_UUIDS];                                
    app_ble_services_get_adv_data(&(adv_uuids[0]));

    advertising_init(adv_uuids, ADVER_NUM_UUIDS);

    ble_gap_addr_t ble_addr;
    sd_ble_gap_addr_get(&ble_addr);
    NRF_LOG_WARNING("Device address: 0x%6x", ble_addr.addr)

}

/* #endregion  PUBLIC */