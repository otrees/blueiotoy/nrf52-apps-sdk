#include "app_ble_services.h"

#include "ble_dfu.h"
#include "ble_conn_state.h"
#include "ble_advdata.h"
#include "ble_dis.h"

#include "nrf_log.h"

#include "app_config.h"
#include "app_ble_config.h"

#include "app_ble.h"

#include "ble_monitor_service.h"
#include "ble_led_control_service_c.h"


// gatt queue reference
static nrf_ble_gq_t *p_gatt_queue;

// LED Control Service client structure
BLE_LCS_C_DEF(m_lcs_c); 
BLE_MS_DEF(m_ms); 

// MS timers
APP_TIMER_DEF(m_monitor_service_char_timer_id);
APP_TIMER_DEF(m_monitor_service_char_timer_on_init_id);

/* #region DFU */
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void on_ble_dfu_evt(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            NRF_LOG_INFO("(BLE-S): Device is preparing to enter bootloader mode.");
            ble_disconnect_all();
        break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            NRF_LOG_INFO("(BLE-S): Device will enter bootloader mode.");
            APP_ERROR_CHECK(false);
        break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("(BLE-S): Request to enter bootloader mode failed asynchroneously.");
            APP_ERROR_CHECK(false);
        break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("(BLE-S): Request to send a response to client failed.");
            APP_ERROR_CHECK(false);
        break;

        default:
            NRF_LOG_ERROR("(BLE-S): Unknown event from ble_dfu_buttonless.");
    }
}

/* #endregion DFU */


static void service_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

//TODO: will be moved to battery.h
static int32_t measure_temperature()
{
    int32_t temperature;
    ret_code_t err_code = sd_temp_get(&temperature);
    // temperature is read in 0.25 degrees of celsia
    APP_ERROR_CHECK(err_code);
    temperature /= 4;
    return temperature;
}


void app_ble_services_get_p_dimled()
{
    ret_code_t err_code = ble_lcs_c_dimled_read(&m_lcs_c);
    if (err_code != NRF_SUCCESS)
        NRF_LOG_ERROR("(BLE-S): ERROR - getdimled (err. code: %d)", err_code);
}

void app_ble_services_set_p_dimled(uint8_t lightmode)
{
    ret_code_t err_code = ble_lcs_c_dimled_write(&m_lcs_c, lightmode);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("(BLE-S): ERROR - setdimled (err. code: %d)", err_code);
    }
    else
        NRF_LOG_INFO("(BLE-S): ble_service_setdimled() to %d", lightmode);
}

void app_ble_services_set_p_debugled(uint8_t color[3])
{
    ret_code_t err_code = ble_lcs_c_debugled_write(&m_lcs_c, color);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_ERROR("(BLE-S): ERROR - setdebugled (err. code: %d)", err_code);
    }
    else
        NRF_LOG_INFO("(BLE-S): ble_service_setdebugled() to [%d %d %d]", color[0], color[1], color[2]);
}


void on_ble_db_disc_evt(ble_db_discovery_evt_t const *p_evt)
{
    ble_lcs_c_on_db_disc_evt(&m_lcs_c, p_evt);
}


static void on_timer_ms_evt(void *p_context)
{   
    int32_t temperature;

    NRF_LOG_RAW_INFO("\n--- Maintenance service callback ---\n");
    temperature = measure_temperature();
    NRF_LOG_DEBUG("Temperature read: %d", temperature);

    // do not throw notifications
    //!!! MOCKUP 
    monitor_service_battery_level_char_update(&m_ms, 100, 3300);
    monitor_service_battery_charge_char_update(&m_ms, 0);
    monitor_service_temp_char_update(&m_ms, temperature);
}

static void on_ble_lcs_c_evt(ble_lcs_c_t *p_lcs_c, ble_lcs_c_evt_t *p_lcs_c_evt)
{
    ret_code_t err_code;

    switch (p_lcs_c_evt->evt_type)
    {
        case BLE_LCS_C_EVT_DISCOVERY_COMPLETE:
        {
            err_code = ble_lcs_c_handles_assign(p_lcs_c, p_lcs_c_evt->conn_handle, &p_lcs_c_evt->params.lcs_handles_peer_db);
            APP_ERROR_CHECK(err_code);

            NRF_LOG_DEBUG("LED Control Service Discovered. Reading light mode.");

            // NRF_LOG_DEBUG("Enabling Dim LED light mode notification.");
            // err_code = ble_bas_c_bl_notif_enable(p_lcs_c, true);
            // APP_ERROR_CHECK(err_code);
        } break;

        case BLE_LCS_C_EVT_DIM_LED_NOTIFICATION:
            NRF_LOG_INFO("LED Control Service, light mode received %d", p_lcs_c_evt->params.light_mode);
            break;

        case BLE_LCS_C_EVT_DIM_LED_READ:
            NRF_LOG_INFO("LED Control Service, light mode read %d", p_lcs_c_evt->params.light_mode);
            break;

        default: break;
    }
}


void app_ble_services_get_adv_data(ble_uuid_t *uuid_to_advertise)
{
    // advertise only monitor service UUIS
    monitor_service_get_adv_data(&m_ms, uuid_to_advertise);
}


static void dis_init()
{
    ret_code_t err_code;

    ble_dis_init_t dis_init;
    memset(&dis_init, 0, sizeof(ble_dis_init_t));

    ble_srv_ascii_to_utf8(&(dis_init.manufact_name_str), (char *)MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&(dis_init.model_num_str), (char *) MODEL_NUMBER);

    // set read permission
    dis_init.dis_char_rd_sec = SEC_OPEN;

    err_code = ble_dis_init(&dis_init);
    NRF_LOG_INFO("DIS initialized (ret code: %d)", err_code);
    APP_ERROR_CHECK(err_code);
}

static void timers_create()
{
    app_timer_create(&m_monitor_service_char_timer_id, APP_TIMER_MODE_REPEATED, on_timer_ms_evt);
    app_timer_create(&m_monitor_service_char_timer_on_init_id, APP_TIMER_MODE_SINGLE_SHOT, on_timer_ms_evt);
}


/**@brief Function for initializing services that will be used by the application */

void app_ble_services_init(app_ble_services_init_t *init)
{
    p_gatt_queue = init->p_gatt_queue;

    // initialize LCS CENTRAL
    ble_lcs_init_c_t lcs_init_c;
    lcs_init_c.evt_handler = on_ble_lcs_c_evt;
    lcs_init_c.error_handler = service_error_handler;
    lcs_init_c.p_gatt_queue  = p_gatt_queue;

    ble_lcs_c_init(&m_lcs_c, &lcs_init_c);

    // initialize MS
    //TODO: rename to the same format
    monitor_service_init(&m_ms);

    // initialize DIS
    dis_init();


    //initialize DFUS
    ble_dfu_buttonless_init_t dfus_init = {0};
    dfus_init.evt_handler = on_ble_dfu_evt;
    ret_code_t err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    // run MS timers
    timers_create();
    app_timer_start(m_monitor_service_char_timer_on_init_id, BLE_MONITOR_SERVICE_ON_INIT_DELAY, NULL);
    app_timer_start(m_monitor_service_char_timer_id, BLE_MONITOR_SERVICE_INTERVAL, NULL);

}

