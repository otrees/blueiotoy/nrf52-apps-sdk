#ifndef BUSINESSLOGIC_H__
#define BUSINESSLOGIC_H__

#include "app_button.h"
#include "app_ble.h"

typedef enum
{
  EVT_STATE_TIMER_ELAPSED,
	NUM_EVTS_STATE_TIMER
} app_statetimer_evt_t;

void app_on_btn_central_evt(app_btn_evt_t event, void *context);
void app_on_btn_peripheral_evt(app_btn_evt_t event, void *context);

void app_on_state_timer_central_evt(app_statetimer_evt_t event);
void app_on_state_timer_peripheral_evt(app_statetimer_evt_t event);

void app_on_ble_central_evt(app_ble_evt_t event, void *context);
void app_on_ble_peripheral_evt(app_ble_evt_t event, void *context);

void businesslogic_init();

#endif /* BUSINESSLOGIC_H__ */
