#include <stdint.h>

#include "boards.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"
// log
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

// DFU
#include "ble_dfu.h"

#include "app_timer.h"
#include "app_error.h"


// board
#include "board_ledmatrix.h"
#include "board_buttons.h"

#include "businesslogic.h"

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


// This call can be used for debug purposes during application development.
// @note CAUTION: Activating this code will write the stack to flash on an error.
//                This function should NOT be used in a final product.
//                It is intended STRICTLY for development/debugging purposes.
//                The flash write will happen EVEN if the radio is active, thus interrupting
//                any communication.
//                Use with care. Uncomment the line below to use.
// On assert, the system can only recover with a reset.
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name)
{
  NRF_LOG_ERROR("APP ERROR HANDLER: %s:%d err.code =%d\n", p_file_name, line_num, error_code);
  NRF_LOG_FLUSH();
  nrf_delay_ms(3000);
  NVIC_SystemReset();
}

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t *p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


static void log_init()
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INTERNAL_FINAL_FLUSH(); //NOTE: this needs to be here because of bootloader
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle()
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static void power_management_init()
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


static void timers_init()
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

static void board_init()
{
    LED_matrix_init();
    buttons_init();
}


int main(void)
{
    log_init();
    NRF_LOG_INFO("\r\n===============================");
    NRF_LOG_INFO("SunFibre Button starting...");

    //TODO: this does not need to be here
    // initialize the async SVCI interface to bootloader before any interrupts are enabled.
    ret_code_t err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);
    
    // timers init
    timers_init();
    // bsp init
    board_init();
    // power management init
    power_management_init();

    NRF_LOG_INFO("===============================");

    // business logic init
    businesslogic_init();

    for (;;)
    { 
        idle_state_handle();
    }
}