#ifndef APP_BLE_H__
#define APP_BLE_H__

#include <stdbool.h>

#include "nrf_sdh_ble.h"
#include "app_timer.h"


typedef uint16_t conn_handle_t;

typedef enum
{
    EVT_BLE_CONNECTED,
    EVT_BLE_CONNECTED_AND_BONDED,
    EVT_BLE_DISCONNECTED,
    EVT_BLE_BONDING_SUCCESSFUL,
    EVT_BLE_SEC_ERROR,
    EVT_BLE_BONDING_REMOVED,
    NUM_EVTS_BLE
} app_ble_evt_t;

typedef struct 
{
    conn_handle_t   conn_handle;
    uint16_t        error_code;
} app_ble_evt_context_t;

typedef void (*app_ble_evt_handler_t)(app_ble_evt_t app_event, app_ble_evt_context_t *context);

typedef struct 
{
    app_ble_evt_handler_t   app_central_evt_handler;
    app_ble_evt_handler_t   app_peripheral_evt_handler;
} app_ble_init_t;



// BLE commands
uint32_t ble_get_peers_count(void);

void ble_disconnect(conn_handle_t conn_handle);
void ble_disconnect_all(void);

void ble_delete_bonds(void);

void ble_start_scan(void);
void ble_stop_scan(void);

void ble_start_advertising_without_whitelist(void);
void ble_stop_advertising(void);

void ble_init(app_ble_init_t *init);



#endif /* APP_BLE_H__ */