#ifndef APP_BLE_SERVICES_H__
#define APP_BLE_SERVICES_H__

#include "nrf_ble_gq.h"
#include "ble_db_discovery.h"


#define BLE_LCS_C_OBSERVER_PRIO				2

#define BLE_SERVICES_NUM_DISCOVERED		1

typedef struct
{
	nrf_ble_gq_t* p_gatt_queue;
} app_ble_services_init_t;


void app_ble_services_get_p_dimled(); 

void app_ble_services_set_p_dimled(uint8_t lightmode);

void app_ble_services_set_p_debugled(uint8_t color[3]);

void on_ble_db_disc_evt(ble_db_discovery_evt_t const *p_evt);

void app_ble_services_get_adv_data(ble_uuid_t *uuid_to_advertise);

void app_ble_services_init(app_ble_services_init_t *init);

#endif /* APP_BLE_SERVICES_H__ */