#ifndef APP_LOGIC_H__
#define APP_LOGIC_H__

typedef enum
{
    CENTRAL_IDLE,
    CENTRAL_SCANNING,
    CENTRAL_CONNECTED,
    CENTRAL_ERROR,
    NUM_APP_CENTRAL_STATES
} app_central_state_t;

typedef enum
{
    PERIPHERAL_IDLE,
    PERIPHERAL_ADVERTISING,
    PERIPHERAL_CONNECTED,
    PERIPHERAL_ERROR,
    NUM_APP_PERIPHERAL_STATES
} app_peripheral_state_t;

typedef enum
{
    BONDING_MISS_KEY,     // AZURE 0x?? (4102) ... error occures when client connects and starts security with unknown key
    BONDING_NO_SEC_REQ,   // PURPLE---         ... error occures when client connects but does not send a request to start bonding
    CONNECTION_REPAIR,    // WHITE 0x85 (133)  ... error occures when already bonded client tries to bond again (server has different encryption key)
    UNIDENTIFIED_ERROR,
    NUM_APP_ERRORS
} app_error_t;


// app utils
const char *app_error_to_str(app_error_t error);
const char *app_central_state_to_str(app_central_state_t state);
const char *app_peripheral_state_to_str(app_peripheral_state_t state);


#endif /* APP_LOGIC_H__ */